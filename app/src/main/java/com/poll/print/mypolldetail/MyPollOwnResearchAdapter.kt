package com.poll.print.mypolldetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import com.poll.print.Utils.SessionManager

class MyPollOwnResearchAdapter(
    private val context: Context?,
    var myPollOwnRearchModel: List<MyPollOwnRearchModel>?,
) : RecyclerView.Adapter<MyPollOwnResearchAdapter.ViewHolder>() {

    lateinit var mSessionManager: SessionManager
    var user_sort_id = ""

    val regex = Regex("^p\\d+")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_my_poll_own_research, parent, false)

        mSessionManager = SessionManager(context)
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mpm = myPollOwnRearchModel!!.get(position)

        holder.tv_title.text = mpm.sort_id
        holder.tv_user_ans.text = mpm.userAnswer

        val unique_usid =
            regex.find(mpm.sort_id.toString())?.value

        if (unique_usid == user_sort_id) {
            holder.tv_title.background = context!!.resources.getDrawable(R.drawable.bg_create_poll)
            holder.tv_title.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
        }
    }

    override fun getItemCount(): Int {
        return myPollOwnRearchModel!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tv_title: TextView = itemView.findViewById(R.id.tv_title)
        val tv_user_ans: TextView = itemView.findViewById(R.id.tv_user_ans)
    }
}