package com.poll.print.mypolldetail

import android.content.Intent
import android.os.Bundle
import android.util.ArrayMap
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.charts.Pie
import com.bumptech.glide.Glide
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils.setGone
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.createpoll.CreateRepollActivity
import com.poll.print.databinding.ActivityMyPollDetailsBinding
import com.poll.print.myactivitydetails.BarLegendsAdapter
import com.poll.print.myactivitydetails.PollResponse
import com.poll.print.myactivitydetails.PollResultAdapter
import com.poll.print.myactivitydetails.PollResultUpdatedAdapter
import com.poll.print.pollsubmit.PollSubmitActivity
import com.poll.print.prouser.ProUserActivity
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class MyPollDetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityMyPollDetailsBinding
    lateinit var mSessionManager: SessionManager

    private var myPollOwnRearchModel = mutableListOf<MyPollOwnRearchModel>()
    private var myUserPollOptionsModel = mutableListOf<MyUserPollOptionsModel>()
    private lateinit var myPollOwnResearchAdapter: MyPollOwnResearchAdapter
    private lateinit var barLegendsAdapter: BarLegendsAdapter
    private lateinit var pollResultDetailsAdapter: PollResultAdapter
    private lateinit var pollResultDetailsUpdateAdapter: PollResultUpdatedAdapter

    var user_sort_id = ""

    lateinit var optionsColorList: ArrayMap<String, Int>
    val optionsIDList = ArrayList<String>()

    var pollId = ""

    var pollType = ""
    var pollTypeDisplay = ""
    var pollDisplayTypeNum = ""
    var isFromHome = false

    var isPollExpired = false

    val dataPieChart: MutableList<DataEntry> = mutableListOf()
    var pie: Pie? = null
    val options = ArrayList<String>()
    val percentage = ArrayList<String>()

    lateinit var pieDataInformation: ArrayList<PieEntry>
    lateinit var pieDataSet: PieDataSet

    lateinit var barDataInformation: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barData: BarData
    lateinit var barDataSetList: ArrayList<BarDataSet>

    var allowVotersToChangeAnswer = false

    val answerCountList = mutableListOf<List<String>>()

    lateinit var material_color_list: IntArray

    var userPremiumStatus: Int = 0
    var isUserPremium: Boolean = false

    var isPieChart: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMyPollDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(this)
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        userPremiumStatus = mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS)
        isUserPremium = mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)

        material_color_list = intArrayOf(
            ColorTemplate.rgb("#2ecc71"),
            ColorTemplate.rgb("#f1c40f"),
            ColorTemplate.rgb("#e74c3c"),
            ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#e9724d"),
            ColorTemplate.rgb("#d6d727"),
            ColorTemplate.rgb("#92cad1"),
            ColorTemplate.rgb("#79ccb3"),
            ColorTemplate.rgb("#868686"),
            ColorTemplate.rgb("#391954"),
            ColorTemplate.rgb("#631e50"),
            ColorTemplate.rgb("#a73c5a"),
            ColorTemplate.rgb("#b0d7e1"),
            ColorTemplate.rgb("#a7e237"),
            ColorTemplate.rgb("#37bd79")
        )

        getIntentData()
        binding.headerSingle.tvTitle.text = "My Poll"
        setOnclickListener()

        getPollDetails()

        if (isFromHome)
            binding.tvRepoll.setGone()
    }

    private fun getPollDetails() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        jsonObject.addProperty("pollId", pollId)

        var apiLink = ""

        if (!isFromHome)
            apiLink = Constants.UrlPath.MY_POLL_DETAILS
        else
            apiLink = Constants.UrlPath.GET_POLL_DETAILS

        ApiServiceProvider.getInstance(this@MyPollDetailsActivity).sendPostData(
            apiLink, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                val pollResponseType = object : TypeToken<PollResponse>() {}.type
                val pollResponse: PollResponse =
                    Gson().fromJson(responseObj.toString(), pollResponseType)

                if (status) {
                    val pollData = responseObj.getJSONObject("pollData")
                    val pollOptions = pollData.getJSONArray("pollOptions")
                    val researchData = pollData.getJSONArray("researchData")

                    val userName = pollData.getString("userName")
                    val imageURL = pollData.getString("imageURL")
                    val pollTitle = pollData.getString("pollTitle")
                    val pollSubTitle = pollData.getString("pollSubTitle")
                    val totalAnswers = pollData.getInt("totalAnswers")

                    if (pollData.has("allowVotersToChangeAnswer"))
                        allowVotersToChangeAnswer = pollData.getBoolean("allowVotersToChangeAnswer")

                    if (allowVotersToChangeAnswer)
                        binding.btnChangePoll.visibility = View.VISIBLE

                    if(userName != "null")
                        binding.tvUserName.text = userName
                    else
                        binding.tvUserName.text = ""

                    binding.tvUserQuestion.text = pollTitle
                    binding.tvTotalAns.text = totalAnswers.toString() + " Pollprint"
                    binding.tvUserQuestionDescription.text = pollSubTitle

                    Glide.with(this@MyPollDetailsActivity).load(imageURL)
                        .placeholder(R.drawable.ic_no_profile).into(binding.imgUser)

                    options.clear()
                    optionsIDList.clear()
                    percentage.clear()

                    for (i in 0 until pollOptions.length()) {
                        options.add(pollOptions.getJSONObject(i).getString("optionTitle"))
                        optionsIDList.add(pollOptions.getJSONObject(i).getString("optionId"))

                        if (pollType != "Preference choice")
                            percentage.add(pollOptions.getJSONObject(i).getString("answerCount"))
                        else {

                            try {
                                val option = pollOptions.getJSONObject(i)
                                val answerCount = option.getJSONObject("answerCount")

                                val valuesList = mutableListOf<String>()

                                answerCount.keys().forEach {
                                    valuesList.add(answerCount.getString(it))
                                }
                                answerCountList.add(valuesList)

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }


                    //Preference choice = 3 , Yes/No = 0 , Percent share = 4 , multiplechoice = 2
                    if (pollType.equals("Preference choice", true)) {
                        val userPollOptions = pollData.getJSONArray("userPollOption")

                        if (userPollOptions.length() != 0) {
                            myUserPollOptionsModel = Gson().fromJson<Any>(
                                userPollOptions.toString(),
                                object : TypeToken<MutableList<MyUserPollOptionsModel>?>() {}.type
                            ) as MutableList<MyUserPollOptionsModel>
                            Log.e("abc", "UserPollOption : ${myUserPollOptionsModel.toString()}")
                        }

                    }


                    if (totalAnswers > 0) {
                        if (pollType == "multiplechoice" || pollType == "Multiple choice" || pollType == "Percent share" ||
                            pollType == "Percentage sharing on option"
                        )
                            showMultipleSelectionChart()
                        else if (pollType == "Preference choice")
                            showPreferenceChoiceChart()
                        else
                            showPieChart()
                    } else {
                        binding.pieChart.visibility = View.GONE
                        binding.barChart.visibility = View.GONE
                        binding.rvBarLegend.visibility = View.GONE
                        binding.rvPollResult.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                    }


                    if (totalAnswers > 0)
                        setPollResultDetails(pollResponse)
                    else
                        binding.clResultList.visibility = View.GONE


                    if (researchData.length() == 0) {
                        binding.tvNoDataResearch.visibility = View.VISIBLE
                    } else {
                        myPollOwnRearchModel = Gson().fromJson<Any>(
                            researchData.toString(),
                            object : TypeToken<MutableList<MyPollOwnRearchModel>?>() {}.type
                        ) as MutableList<MyPollOwnRearchModel>
                        setOwnRearchData()
                    }

                } else {
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finish()
            }
        }
    }

    private fun getIntentData() {
        pollId = intent.getStringExtra("pollId").toString()
        isFromHome = intent.getBooleanExtra("isFromHome", false)
        isPollExpired = intent.getBooleanExtra("isPollExpired", false)
        pollType = intent.getStringExtra("PollType").toString()
        pollTypeDisplay = intent.getStringExtra("PollDisplayType").toString()
        pollDisplayTypeNum = intent.getStringExtra("PollDisplayTypeNum").toString()
    }

    private fun setPollResultDetails(pollResponse: PollResponse) {

        if (!pollType.equals(
                "Percent share",
                true
            ) && !pollType.equals("Percentage sharing on option", true)
        ) {
            var answerIdList = ArrayList<String>()
            val regex = Regex("^p\\d+")

            for (i in 0 until pollResponse.pollData.pollOptions.size) {
                for (j in 0 until pollResponse.pollData.pollOptions[i].answerIds.size) {

                    val unique_usid =
                        regex.find(pollResponse.pollData.pollOptions[i].answerIds[j])?.value

                    if (unique_usid == user_sort_id) {
                        answerIdList.add(0, pollResponse.pollData.pollOptions[i].answerIds[j])
                    } else {
                        answerIdList.add(pollResponse.pollData.pollOptions[i].answerIds[j])
                    }
                }
            }

            val distinctAnswerIdList = answerIdList.distinct()

            pollResultDetailsUpdateAdapter =
                PollResultUpdatedAdapter(
                    this@MyPollDetailsActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse,
                    distinctAnswerIdList,
                    optionsColorList,
                    myUserPollOptionsModel
                )
            binding.rvPollResult.adapter = pollResultDetailsUpdateAdapter
        } else {
            pollResultDetailsAdapter =
                PollResultAdapter(
                    this@MyPollDetailsActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse
                )

            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.rvPollResult.layoutManager = layoutManager
            binding.rvPollResult.adapter = pollResultDetailsAdapter
        }
    }

    private fun showMultipleSelectionChart() {
        //MPChart
        barDataInformation = ArrayList()

        for (index in percentage.indices) {
            try {
                barDataInformation.add(
                    BarEntry(
                        (index + 1).toFloat(),
                        percentage[index].replace("%", "").replace("\"", "").toFloat()
                    )
                )
                barDataSet = BarDataSet(barDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // Custom value formatter to format the value
        barDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "0%" else ("" + value.toInt() + "%")
            }
        }

        barDataSet.setColors(material_color_list, 250)
        barDataSet.valueTextColor = resources.getColor(R.color.black)
        barDataSet.valueTextSize = 7f

        barData = BarData(barDataSet)

        binding.barChart.setFitBars(true)
        binding.barChart.legend.isEnabled = false
        binding.barChart.data = barData
        binding.barChart.description.text = ""
        binding.barChart.animateY(700)

        binding.barChart.invalidate()

        if (options.size != 0)
            setBarLegends(options, percentage, material_color_list)

    }

    private fun showPreferenceChoiceChart() {

        barDataSetList = ArrayList()
        barData = BarData()

        var numm = 1f

        try {
            for (index in answerCountList.indices) {

                barDataInformation = ArrayList()

                for (i in 0 until answerCountList[index].size) {

                    barDataInformation.add(
                        BarEntry(
                            numm,
                            answerCountList[index][i].replace("%", "").replace("\"", "")
                                .toFloat()
                        )
                    )
                    numm++
                }

                barDataSet = BarDataSet(barDataInformation, options[index])
                barDataSet.color = material_color_list[index]
                barDataSetList.add(barDataSet)

                numm += 2 //Required for x-coordinator for bar graph

            }

            // Custom value formatter to format the value
            barDataSet.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return if (value == 0f) "0%" else ("" + value.toInt() + "%")
                }
            }

            for (i in 0 until barDataSetList.size)
                barData.addDataSet(barDataSetList[i])

            binding.barChart.data = barData
            binding.barChart.legend.isEnabled = false

            barDataSet.valueTextColor = resources.getColor(R.color.black)
            barDataSet.valueTextSize = 7f

            val groupSpace = 0.8f
            val barSpace = 0.03f
            val barWidth = 1f

            barData.barWidth = barWidth

            binding.barChart.description.isEnabled = false
            binding.barChart.xAxis.setCenterAxisLabels(true)
            binding.barChart.setFitBars(true)
            binding.barChart.groupBars(0f, groupSpace, barSpace)
            binding.barChart.animateY(700)

            binding.barChart.invalidate()

            if (options.size != 0)
                setBarLegends(options, percentage, material_color_list)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showPieChart() {

        isPieChart = true

        binding.pieChart.visibility = View.VISIBLE
        binding.barChart.visibility = View.GONE

        //MPChart
        pieDataInformation = ArrayList()
        for (index in percentage.indices) {
            try {
                pieDataInformation.add(
                    PieEntry(
                        percentage[index].replace("%", "").replace("\"", "").toFloat(),
                        options[index]
                    )
                )
                pieDataSet = PieDataSet(pieDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        pieDataSet.colors = material_color_list.asList()
        pieDataSet.valueTextSize = 8f

        val data = PieData(pieDataSet)

        binding.pieChart.data = data
        binding.pieChart.description.isEnabled = false
        binding.pieChart.isDrawHoleEnabled = false
        binding.pieChart.setDrawEntryLabels(false)
        binding.pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        binding.pieChart.legend.isWordWrapEnabled = true
        binding.pieChart.legend.isEnabled = false

        // Center text inside slices
        pieDataSet.xValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE
        pieDataSet.yValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE

        // Custom value formatter to format the value
        pieDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "" else ("" + value.toInt() + "%")
            }
        }

        binding.pieChart.invalidate()

        if (options.size != 0) {
            val params = binding.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
            params.topToBottom = binding.pieChart.id
            setBarLegends(options, percentage, material_color_list)
        }

    }

    private fun setBarLegends(
        options: ArrayList<String>,
        percentage: ArrayList<String>,
        materialColors: IntArray
    ) {

        optionsColorList = ArrayMap<String, Int>()

        for (i in 0 until options.size) {
            optionsColorList.put(optionsIDList[i], material_color_list[i])
        }

        barLegendsAdapter =
            BarLegendsAdapter(this@MyPollDetailsActivity, options, percentage, materialColors)
        binding.rvBarLegend.layoutManager = GridLayoutManager(this, 2)
        binding.rvBarLegend.adapter = barLegendsAdapter
    }

    private fun setOwnRearchData() {
        myPollOwnResearchAdapter =
            MyPollOwnResearchAdapter(this@MyPollDetailsActivity, myPollOwnRearchModel)
        binding.rvDoOwnResearch.adapter = myPollOwnResearchAdapter
    }

    private fun addYourOwnResearch() {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.addProperty("userAnswer", binding.edtDoOwnResearch.text.toString())
        ApiServiceProvider.getInstance(this@MyPollDetailsActivity).sendPostData(
            Constants.UrlPath.ADD_USER_OWN_RESEARCH, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@MyPollDetailsActivity,
                        "" + message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    binding.edtDoOwnResearch.setText("")
                    binding.tvNoData.visibility = View.GONE
                    getPollDetails()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setOnclickListener() {
        binding.apply {
            headerSingle.imgBack.setOnClickListener {
                finish()
            }
            imgSend.setOnClickListener {
                if (edtDoOwnResearch.text.toString().trim() != "")
                    addYourOwnResearch()
            }
            btnProUser.setOnClickListener {
                val userCountry = mSessionManager.getData(SessionManager.USER_COUNTRY)

                val i = Intent(this@MyPollDetailsActivity, ProUserActivity::class.java)
                i.putExtra("userCountry", userCountry)
                startActivity(i)
            }
            tvRepoll.setOnClickListener {
                val i = Intent(this@MyPollDetailsActivity, CreateRepollActivity::class.java)
                i.putExtra("pollId", pollId)
                i.putExtra("PollType", pollType)
                i.putExtra("PollDisplayType", pollTypeDisplay)
                startActivity(i)
            }
            tvUserQuestion.setOnClickListener {
                if (tvUserQuestion.tag == "1") {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_expand, 0
                    )
                    tvUserQuestionDescription.visibility = View.VISIBLE
                    tvUserQuestion.tag = "0"
                } else {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_collapse, 0
                    )
                    tvUserQuestionDescription.visibility = View.GONE
                    tvUserQuestion.tag = "1"
                }
            }

            btnChangePoll.setOnClickListener {
                val intent = Intent(applicationContext, PollSubmitActivity::class.java)
                intent.putExtra("PollId", pollId)
                intent.putExtra("isPollAnswered", true)
                intent.putExtra("allowVotersToChangeAnswer", allowVotersToChangeAnswer)
                intent.putExtra("isPollExpired", isPollExpired)
                intent.putExtra("PollDisplayType", pollDisplayTypeNum.toInt())
                startActivity(intent)
            }

            tvViewResult.setOnClickListener {
                if (tvViewResult.tag == "1") {
                    tvViewResult.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_expand, 0
                    )
                    tvViewResult.tag = "0"
                    rvBarLegend.visibility = View.VISIBLE

                    if (!isPieChart)
                        barChart.visibility = View.VISIBLE
                    else
                        pieChart.visibility = View.VISIBLE

                    clResultList.visibility = View.VISIBLE
                } else {
                    tvViewResult.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_collapse, 0
                    )
                    tvViewResult.tag = "1"
                    rvBarLegend.visibility = View.GONE

                    if (!isPieChart)
                        barChart.visibility = View.GONE
                    else
                        pieChart.visibility = View.GONE

                    clResultList.visibility = View.GONE
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (isUserPremium) {
            if (userPremiumStatus != Constants.PRO_USER_STATUS.ACCEPTED) {
                if (userPremiumStatus == Constants.PRO_USER_STATUS.SUBMITTED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                    binding.clDoOwnResearch.visibility = View.GONE
                } else if (userPremiumStatus == Constants.PRO_USER_STATUS.NOTAPLLIED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                    binding.clDoOwnResearch.visibility = View.GONE
                } else if (userPremiumStatus == Constants.PRO_USER_STATUS.REJECTED) {
                    binding.btnProUser.text = "Become a Pro User"
                    binding.btnProUser.isEnabled = true
                    binding.clDoOwnResearch.visibility = View.GONE
                }
            } else {
                binding.btnProUser.text = "Pro User"
                binding.btnProUser.isEnabled = false
                binding.clDoOwnResearch.visibility = View.VISIBLE
            }
        } else {
            binding.btnProUser.text = "Become a Pro User"
            binding.btnProUser.isEnabled = true
            binding.clDoOwnResearch.visibility = View.GONE
        }

    }

}