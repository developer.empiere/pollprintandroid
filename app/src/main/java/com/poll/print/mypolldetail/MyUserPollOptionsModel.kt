package com.poll.print.mypolldetail

data class MyUserPollOptionsModel(
    var sortId: String? = null,
    val optionIds: Array<String>? = null
)