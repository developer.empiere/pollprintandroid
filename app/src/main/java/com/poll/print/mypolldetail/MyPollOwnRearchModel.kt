package com.poll.print.mypolldetail

data class MyPollOwnRearchModel(
    var _id: String? = null,
    val userAnswer: String? = null,
    val userId: String? = null,
    var sort_id: String? = null
)