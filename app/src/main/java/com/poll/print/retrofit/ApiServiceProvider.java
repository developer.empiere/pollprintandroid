package com.poll.print.retrofit;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.poll.print.Utils.AppUtils;
import com.poll.print.Utils.Constants;
import com.poll.print.Utils.SessionManager;
import com.poll.print.dialog.AlertDialog;
import com.poll.print.dialog.ConnectionError;
import com.poll.print.dialog.LoadingDialog;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiServiceProvider extends RetrofitBase {
    private final ApiServices apiServices;
    private final LoadingDialog mDialog;
    private AlertDialog mAlertDialog;
    private final SessionManager mSessionManager;
    private Context mContext;
    private String mApiUrl;

    private ApiServiceProvider(Context context) {
        super(context, Constants.UrlPath.API_URL, true);
        mContext = context;
        mDialog = new LoadingDialog(context);
        apiServices = retrofit.create(ApiServices.class);
        mSessionManager = new SessionManager(context);
    }

    private ApiServiceProvider(Context context, String baseUrl) {
        super(context, baseUrl, true);
        mDialog = new LoadingDialog(context);
        apiServices = retrofit.create(ApiServices.class);
        mSessionManager = new SessionManager(context);
    }

    public static ApiServiceProvider getInstance(Context context) {
        return new ApiServiceProvider(context);
    }

    public static ApiServiceProvider getInstance(Context context, String baseUrl) {
        return new ApiServiceProvider(context, baseUrl);
    }

    private void manageResponse(Response response, RetrofitListener retrofitListener) {
        switch (response.code()) {
            case Constants.ResponseCode.CODE_200:
            case Constants.ResponseCode.CODE_204:
                validateResponse(response, retrofitListener);
                break;

            case Constants.ResponseCode.CODE_500:
            case Constants.ResponseCode.CODE_400:
            case Constants.ResponseCode.CODE_401:
            case Constants.ResponseCode.CODE_403:
            case Constants.ResponseCode.CODE_404:
            case Constants.ResponseCode.CODE_422:
            case Constants.ResponseCode.CODE_428:
            case Constants.ResponseCode.CODE_429:
                try {
                    if (checkIsGrantedAPI()) {
                        validateResponse(response, retrofitListener);
                    } else {
                        JSONObject resObj = new JSONObject(response.errorBody().string());
                        if (resObj.has("message")) {
                            if (response.code() == Constants.ResponseCode.CODE_401) {
                                mAlertDialog = new AlertDialog(context);
                                mAlertDialog.setMessage(resObj.getString("message"));
                                mAlertDialog.setCancelable(false);
                                mAlertDialog.setPositiveButton("Ok", view -> {
                                    mAlertDialog.dismiss();
                                });
                                if (!mAlertDialog.isShowing()) mAlertDialog.show();
                            } else {
                                AppUtils.INSTANCE.showAlertDialog(context, "Title", resObj.getString("message"), "Ok");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private boolean checkIsGrantedAPI() {
        return false;
    }

    private void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();
    }

    public void sendPostData(String url, JsonObject requestData, Boolean isProgressShown, final RetrofitListener retrofitListener) {
        if (isProgressShown) mDialog.show();

        mApiUrl = url;
        Call<JsonElement> call = apiServices.sendPostData(url, requestData);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(mContext, t).setListener(() -> sendPostData(url, requestData, isProgressShown, retrofitListener));
            }
        });
    }

    public void sendSpinnerData(String url, Boolean isProgressShown, final RetrofitListener retrofitListener) {
        if (isProgressShown)
            mDialog.show();
        mApiUrl = url;

        Call<JsonElement> call = apiServices.sendPostDataWithoutParams(url);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> sendSpinnerData(url, isProgressShown, retrofitListener));
            }
        });
    }

    public void uploadImage(String url, RequestBody imageType, RequestBody imageStoreId, MultipartBody.Part imagee,
                            Boolean isProgressShown,
                            final RetrofitListener retrofitListener) {
        if (isProgressShown)
            mDialog.show();
        mApiUrl = url;

        Call<JsonElement> call = apiServices.sendImages(mApiUrl, imageType, imageStoreId, imagee);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> uploadImage(url, imageType, imageStoreId, imagee,
                        isProgressShown, retrofitListener));
            }
        });
    }
}