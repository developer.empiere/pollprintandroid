package com.poll.print.retrofit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiServices {
    @POST()
    Call<JsonElement> sendPostData(@Url String url, @Body JsonObject requestData);

    @POST()
    Call<JsonElement> sendPostDataWithoutParams(@Url String url);

    @Multipart
    @POST()
    Call<JsonElement> sendImages(@Url String url, @Part("imageType") RequestBody imageType, @Part("imageStoreId") RequestBody imageStoreId,
                                 @Part MultipartBody.Part userImage);
}