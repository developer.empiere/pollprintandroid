package com.poll.print.retrofit;

import retrofit2.Response;

public interface RetrofitListener {
    void onResponseSuccess(Response response);
}