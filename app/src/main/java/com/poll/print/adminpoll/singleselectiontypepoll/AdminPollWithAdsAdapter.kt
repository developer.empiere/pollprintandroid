package com.poll.print.adminpoll.singleselectiontypepoll

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R
import com.poll.print.Utils.AdsManager
import com.poll.print.myactivitydetails.MyActivityDetailsActivity
import com.poll.print.mypolldetail.MyPollDetailsActivity
import com.poll.print.pollsubmit.PollSubmitActivity
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.concurrent.TimeUnit

private const val ITEM_TYPE_POLL = 0
private const val ITEM_TYPE_AD = 1

class AdminPollWithAdsAdapter(
    private val context: Context?,
    private var adminPollModel: List<Any>, // Changed to List<Any> to support mixed types
    private var userType: String,
    private var selCategoryImage: String = ""
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var pollOptionAdapter: PollOptionAdapter

    private lateinit var sdf: SimpleDateFormat
    private lateinit var currentDateString: String
    private lateinit var currentDate: Date

    lateinit var rightNow: Calendar
    var currentMonthTotalDays: Int = 0

    override fun getItemViewType(position: Int): Int {
        return if (adminPollModel[position] is AdminPollModel) {
            ITEM_TYPE_POLL
        } else {
            ITEM_TYPE_AD
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        sdf = SimpleDateFormat("dd/MM/yyyy")
        currentDateString = sdf.format(Date())
        currentDate = sdf.parse(currentDateString)
        rightNow = Calendar.getInstance()
        currentMonthTotalDays = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH)

        return if (viewType == ITEM_TYPE_POLL) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_user_or_admin_poll_fixed, parent, false)
            PollViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_ad_view, parent, false)
            AdViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PollViewHolder) {
            val model = adminPollModel[position] as AdminPollModel
            bindPoll(holder, model)
        } else if (holder is AdViewHolder) {
            bindAd(holder)
        }
    }

    override fun getItemCount(): Int {
        return adminPollModel.size
    }

    private fun bindPoll(holder: PollViewHolder, model: AdminPollModel) {

        if (model.userName == null || model.userName.isEmpty()) {
            //holder.tv_user_name.text = "Admin"
            holder.tv_user_name.text = "poll-mAIster"
        } else {
            holder.tv_user_name.text = model.userName
        }

        holder.tv_user_question.text = model.pollTitle

        if (userType == "admin") {
            if (selCategoryImage == "")
            //Glide.with(context!!).load(model.imageURL)
                Glide.with(context!!).load(model.categoryImageUrl)
                    .placeholder(R.drawable.ic_no_profile)
                    .into(holder.img_user)
            else
                Glide.with(context!!).load(selCategoryImage)
                    .placeholder(R.drawable.ic_no_profile)
                    .into(holder.img_user)
        } else {
            Glide.with(context!!).load(model.imageURL)
                .placeholder(R.drawable.ic_no_profile)
                .into(holder.img_user)
        }

        pollOptionAdapter = PollOptionAdapter(
            context,
            model.pollOptions,
            model.pollId,
            model.pollTypeDisplay
        )
        holder.rv_poll_type.adapter = pollOptionAdapter


        if (!model.isAnswered) {
            holder.rv_poll_type.alpha = .05f
            holder.ivFingerprint.visibility = View.VISIBLE
        } else {
            holder.rv_poll_type.alpha = 1f
            holder.ivFingerprint.visibility = View.GONE
        }

        holder.clPollDetails.setOnClickListener {

            var isPollExpired = false

            if (model.endDate != null) {

                val pollEndDateString = model.endDate

                val pollEndDate: Date = sdf.parse(pollEndDateString)

                if (pollEndDate.before(currentDate)) {
                    isPollExpired = true
                }

            }

            if (!model.isAnswered) {
                val intent = Intent(context, PollSubmitActivity::class.java)
                intent.putExtra("PollId", model.pollId)
                intent.putExtra("isPollAnswered", model.isAnswered)
                intent.putExtra("allowVotersToChangeAnswer", model.allowVotersToChangeAnswer)
                intent.putExtra("isPollExpired", isPollExpired)
                intent.putExtra("PollDisplayType", model.pollTypeDisplay)
                context.startActivity(intent)
            } else {
                if (userType == "admin" || model.isAdminCreatedPoll) {
                    val i = Intent(context, MyPollDetailsActivity::class.java)
                    i.putExtra("pollId", model.pollId)
                    i.putExtra("PollType", model.pollType)
                    i.putExtra("isPollExpired", isPollExpired)
                    i.putExtra("PollDisplayType", "")
                    i.putExtra("PollDisplayTypeNum", model.pollTypeDisplay.toString())
                    i.putExtra("isFromHome", true)
                    context.startActivity(i)
                } else if (userType == "user" || !model.isAdminCreatedPoll) {
                    var i = Intent(context, MyActivityDetailsActivity::class.java)
                    i.putExtra("pollId", model.pollId)
                    i.putExtra("PollType", model.pollTypeDisplay)
                    i.putExtra("PollDisplayType", model.pollType)
                    i.putExtra("isFromHome", true)
                    context.startActivity(i)
                }
            }

        }

        holder.blurimage.setOnClickListener {

            var isPollExpired = false

            if (model.endDate != null) {

                val pollEndDateString = model.endDate

                val pollEndDate: Date = sdf.parse(pollEndDateString)

                if (pollEndDate.before(currentDate)) {
                    isPollExpired = true
                }

            }

            if (!model.isAnswered) {
                val intent = Intent(context, PollSubmitActivity::class.java)
                intent.putExtra("PollId", model.pollId)
                intent.putExtra("isPollAnswered", model.isAnswered)
                intent.putExtra("allowVotersToChangeAnswer", model.allowVotersToChangeAnswer)
                intent.putExtra("isPollExpired", isPollExpired)
                intent.putExtra("PollDisplayType", model.pollTypeDisplay)
                context.startActivity(intent)
            } else {
                if (userType == "admin" || model.isAdminCreatedPoll) {
                    val i = Intent(context, MyPollDetailsActivity::class.java)
                    i.putExtra("pollId", model.pollId)
                    i.putExtra("PollType", model.pollType)
                    i.putExtra("PollDisplayType", "")
                    i.putExtra("PollDisplayTypeNum", model.pollTypeDisplay.toString())
                    i.putExtra("isFromHome", true)
                    context.startActivity(i)
                } else if (userType == "user" || !model.isAdminCreatedPoll) {
                    var i = Intent(context, MyActivityDetailsActivity::class.java)
                    i.putExtra("pollId", model.pollId)
                    i.putExtra("PollType", model.pollTypeDisplay)
                    i.putExtra("PollDisplayType", model.pollType)
                    i.putExtra("isFromHome", true)
                    context.startActivity(i)
                }
            }

        }

        if (model.endDate != null) {

            holder.tv_time.visibility = View.VISIBLE

            val pollEndDateString = model.endDate

            val pollEndDate: Date = sdf.parse(pollEndDateString)

            val diffDate: Long = pollEndDate.time - currentDate.time

            if (pollEndDate != currentDate) {
                if (pollEndDate.after(currentDate)) {

                    var time_in_seconds =
                        TimeUnit.MILLISECONDS.convert(diffDate, TimeUnit.MILLISECONDS)

                    //Countdown Part Starts
                    val currentHourIn24Format: Int = rightNow.get(Calendar.HOUR_OF_DAY)
                    val currentMinFormat: Int = rightNow.get(Calendar.MINUTE)

                    var diffHour: Long = 24L - currentHourIn24Format
                    var diffMin: Long = 60L - currentMinFormat

                    var seconds: String = ((time_in_seconds / 1000) % 60).toString()
                    var minute: String = ((time_in_seconds / 1000) / 60).toString()
                    var hours: String = (minute.toLong() / 60).toString()
                    var days: String = (hours.toLong() / currentMonthTotalDays).toString()

                    //Uncomment below code to get additional time formats
                    if (days != "0")
                        holder.tv_time.text = "$days D $diffHour hr left"
                    else
                        if (diffHour != 1L)
                            holder.tv_time.text = "$diffHour hr $diffMin min left"
                        else
                            if (diffMin != 1L)
                                holder.tv_time.text = "$diffMin min $seconds sec left"
                            else
                                holder.tv_time.text = "$seconds sec left"

                    //Uncomment below code to get only single time format
                    /*if (days != "0")
                        holder.tv_time.text = "$days D left"
                    else
                        if (diffHour != 1L)
                            holder.tv_time.text = "$diffHour hr left"
                        else
                            if (diffMin != 1L)
                                holder.tv_time.text = "$diffMin min left"
                            else
                                holder.tv_time.text = "$seconds sec left"*/

                    //Countdown Part Ends

                } else {
                    holder.tv_time.text = "Expired"
                }
            } else {
                var time_in_seconds =
                    TimeUnit.MILLISECONDS.convert(diffDate, TimeUnit.MILLISECONDS)

                val currentHourIn24Format: Int = rightNow.get(Calendar.HOUR_OF_DAY)
                val currentMinFormat: Int = rightNow.get(Calendar.MINUTE)

                var diffHour: Long = 24L - currentHourIn24Format
                var diffMin: Long = 60L - currentMinFormat

                var seconds: String = ((time_in_seconds / 1000) % 60).toString()

                if (diffHour != 1L)
                    holder.tv_time.text = "$diffHour hr $diffMin min left"
                else
                    if (diffMin != 1L)
                        holder.tv_time.text = "$diffMin min $seconds sec left"
                    else
                        holder.tv_time.text = "$seconds sec left"
            }

        } else {
            holder.tv_time.visibility = View.INVISIBLE
        }
    }

    private fun bindAd(holder: AdViewHolder) {
        // Create a new AdView
        /*val adsView = AdView(context!!)
        adsView.adUnitId = Constants.GoogleAdsData.BANNER_AD_ID
        adsView.setAdSize(adSize)
        adView = adsView

        // Add the AdView to the container
        holder.ad_view_container.removeAllViews()
        holder.ad_view_container.addView(adView)

        // Load the ad
        val adRequest = AdRequest.Builder().build()
        adView!!.loadAd(adRequest)*/
        val adsManager = AdsManager(context!!, holder.ad_view_container)
        adsManager.loadBannerAd()
    }

    class PollViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img_user: RoundedImageView = itemView.findViewById(R.id.img_user)
        val ivFingerprint: ImageView = itemView.findViewById(R.id.ivFingerprint)
        val tv_user_name: TextView = itemView.findViewById(R.id.tv_user_name)
        val tv_time: TextView = itemView.findViewById(R.id.tv_time)
        val tv_user_question: TextView = itemView.findViewById(R.id.tv_user_question)
        val rv_poll_type: RecyclerView = itemView.findViewById(R.id.rv_poll_type)
        val blurimage: ConstraintLayout = itemView.findViewById(R.id.blurimage)
        val clPollDetails: ConstraintLayout = itemView.findViewById(R.id.clPollDetails)
    }

    class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ad_view_container: FrameLayout = itemView.findViewById(R.id.ad_view_container)
    }

    /*val adSize: AdSize
        get() {
            val displayMetrics = context!!.resources.displayMetrics
            val adWidthPixels =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    val windowMetrics: WindowMetrics =
                        (context as Activity).windowManager.currentWindowMetrics
                    windowMetrics.bounds.width()
                } else {
                    displayMetrics.widthPixels
                }
            val density = displayMetrics.density
            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                context,
                adWidth
            )
        }*/
}
