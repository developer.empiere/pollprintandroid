package com.poll.print.adminpoll

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.DashBoard.PollCategoryModel
import com.poll.print.R
import com.poll.print.Utils.AdsManager
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollAdapter
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollModel
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollWithAdsAdapter
import com.poll.print.databinding.FragmentAdminPollBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class AdminPollFragment : Fragment() {

    var _binding: FragmentAdminPollBinding? = null
    val binding get() = _binding!!
    lateinit var mSessionManager: SessionManager
    private var adminPollModel = ArrayList<AdminPollModel>()
    private var adminPollModelList = ArrayList<AdminPollModel>()
    private lateinit var adminPollAdapter: AdminPollAdapter
    private lateinit var adminPollWithAdsAdapter: AdminPollWithAdsAdapter

    //private var adView: AdView? = null
    private lateinit var adsManager: AdsManager

    private var pollCategoryModel = mutableListOf<PollCategoryModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAdminPollBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("RtlHardcoded")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSessionManager = SessionManager(context)
        println("UserId:-" + mSessionManager.getData(SessionManager.USER_ID))

        binding.container.setOnRefreshListener {
            // on below line we are setting is refreshing to false.
            binding.container.isRefreshing = false

            getAdminPollData()
        }
        getAdminPollData()
        //initializeMobileAdsSdk()
        MobileAds.initialize(requireContext())

        /*adsManager = AdsManager(requireContext(), binding.adViewContainer)

        CoroutineScope(Dispatchers.Main).launch {
            MobileAds.initialize(requireContext())
            adsManager.loadBannerAd()
        }*/

        // Initialize the Mobile Ads SDK
        /*adsManager.initializeMobileAdsSdk {
            // Load a banner ad after initialization
            adsManager.loadBannerAd()
        }*/
    }

    /*private fun initializeMobileAdsSdk() {
        CoroutineScope(Dispatchers.IO).launch {
            // Initialize the Google Mobile Ads SDK on a background thread.
            MobileAds.initialize(requireContext()) {}

            requireActivity().runOnUiThread {
                // Load an ad on the main thread.
                loadBanner()
            }
        }
    }

    private fun loadBanner() {
        // [START create_ad_view]
        // Create a new ad view.
        val adView = AdView(requireContext())
        adView.adUnitId = Constants.GoogleAdsData.BANNER_AD_ID
        adView.setAdSize(adSize)
        this.adView = adView

        // Replace ad container with new ad view.
        binding.adViewContainer.removeAllViews()
        binding.adViewContainer.addView(adView)
        // [END create_ad_view]

        // [START load_ad]
        // Start loading the ad in the background.
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        // [END load_ad]
    }

    // Get the ad size with screen width.
    private val adSize: AdSize
        get() {
            val displayMetrics = resources.displayMetrics
            val adWidthPixels =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    val windowMetrics: WindowMetrics =
                        requireActivity().windowManager.currentWindowMetrics
                    windowMetrics.bounds.width()
                } else {
                    displayMetrics.widthPixels
                }
            val density = displayMetrics.density
            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                requireContext(),
                adWidth
            )
        }*/

    fun getAdminPollData() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_ALL_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    adminPollModelList.clear()

                    val pollPrints = responseObj.getJSONArray("pollPrints")

                    val questLevel = 2
                    val overallPollPrintCounter = 1000000

                    val totalPollprintCount = responseObj.getString("totalPollprintCount")
                    //Set total admin pollprint data
                    binding.progressBar.max = overallPollPrintCounter
                    binding.progressBar.progress = totalPollprintCount.toInt()
                    binding.tvProgress.text = totalPollprintCount

                    binding.tvQuestLevel.text = "Path To Unlock Level - $questLevel"
                    binding.tvTotal.text = overallPollPrintCounter.toString()

                    adminPollModel = Gson().fromJson<Any>(
                        pollPrints.toString(), object : TypeToken<ArrayList<AdminPollModel>?>() {}.type
                    ) as ArrayList<AdminPollModel>

                    for (i in 0 until adminPollModel.size) {
                        if (adminPollModel[i].isAdminCreatedPoll) {
                            adminPollModelList.add(adminPollModel[i])
                        }
                    }

                    val adminPollListWithAds = mutableListOf<Any>().apply {
                        adminPollModelList.forEachIndexed { index, item ->
                            add(item)
                            if ((index + 1) % 3 == 0) {
                                add("AdViewHolder")
                            }
                        }
                    }
                    //if (adminPollModelList.size > 0)
                    if (adminPollListWithAds.size > 0) {
                        (requireActivity() as DashBoardActivity).getPollCategory()
                        setAdminPollData(adminPollListWithAds)
                    } else {
                        binding.rvPollAdmin.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getCategoryData(selCategoryId: String, categoryImage: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        jsonObject.addProperty("categoryId", selCategoryId)
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_ALL_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {

                    adminPollModelList.clear()

                    val pollPrints = responseObj.getJSONArray("pollPrints")
                    adminPollModel = Gson().fromJson<Any>(
                        pollPrints.toString(), object : TypeToken<ArrayList<AdminPollModel>?>() {}.type
                    ) as ArrayList<AdminPollModel>

                    for (i in 0 until adminPollModel.size) {
                        if (adminPollModel[i].isAdminCreatedPoll) {
                            adminPollModelList.add(adminPollModel[i])
                        }
                    }

                    val adminPollListWithAds = mutableListOf<Any>().apply {
                        adminPollModelList.forEachIndexed { index, item ->
                            add(item)
                            if ((index + 1) % 3 == 0) {
                                add("AdViewHolder")
                            }
                        }
                    }

                    //if (adminPollModelList.size > 0)
                    if (adminPollListWithAds.size > 0) setAdminCategoryPollData(categoryImage, adminPollListWithAds)
                    else {
                        binding.rvPollAdmin.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                    }


                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*private fun setAdminPollData() {
        binding.rvPollAdmin.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollAdapter = AdminPollAdapter(context, adminPollModelList, "admin")
        Log.e("aaa", "adminPollModelList:: $adminPollModelList")
        binding.rvPollAdmin.adapter = adminPollAdapter
    }*/

    private fun setAdminPollData(adminPollListWithAds: MutableList<Any>) {
        binding.rvPollAdmin.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollWithAdsAdapter = AdminPollWithAdsAdapter(context, adminPollListWithAds, "admin")
        Log.e("aaa", "adminPollModelList:: $adminPollListWithAds")
        binding.rvPollAdmin.adapter = adminPollWithAdsAdapter

        binding.rvPollAdmin.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    // Scrolling down, disable refresh
                    binding.container.isEnabled = false
                } else if (dy < 0) {
                    // Scrolling up, enable refresh only if at the top
                    val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
                    binding.container.isEnabled = layoutManager?.findFirstCompletelyVisibleItemPosition() == 0
                }
            }
        })
    }

    /*private fun setAdminCategoryPollData(categoryImage: String) {
        binding.rvPollAdmin.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollAdapter = AdminPollAdapter(context, adminPollModelList, "admin", categoryImage)
        Log.e("aaa", "adminPollModelList:: $adminPollModelList")
        binding.rvPollAdmin.adapter = adminPollAdapter
    }*/

    private fun setAdminCategoryPollData(
        categoryImage: String, adminPollListWithAds: MutableList<Any>
    ) {
        binding.rvPollAdmin.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollWithAdsAdapter = AdminPollWithAdsAdapter(context, adminPollListWithAds, "admin", categoryImage)
        Log.e("aaa", "adminPollModelList:: $adminPollListWithAds")
        binding.rvPollAdmin.adapter = adminPollWithAdsAdapter

        binding.rvPollAdmin.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    // Scrolling down, disable refresh
                    binding.container.isEnabled = false
                } else if (dy < 0) {
                    // Scrolling up, enable refresh only if at the top
                    val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
                    binding.container.isEnabled = layoutManager?.findFirstCompletelyVisibleItemPosition() == 0
                }
            }
        })
    }

    fun getPollCategoryImage(selCategoryId: String) {
        val requestData = JsonObject()
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_POLL_CATEGORY_LIST, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                var categoryImage = ""

                if (status) {
                    val categories = responseObj.getJSONArray("categories")
                    pollCategoryModel = Gson().fromJson<Any>(
                        categories.toString(), object : TypeToken<MutableList<PollCategoryModel>?>() {}.type
                    ) as MutableList<PollCategoryModel>

                    for (i in 0 until pollCategoryModel.size) {
                        if (pollCategoryModel[i]._id == selCategoryId) categoryImage = pollCategoryModel[i].categoryImageURL.toString()
                    }

                    getCategoryData(selCategoryId, categoryImage)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}