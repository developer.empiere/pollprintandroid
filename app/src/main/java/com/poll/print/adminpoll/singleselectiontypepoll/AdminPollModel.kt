package com.poll.print.adminpoll.singleselectiontypepoll

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

data class AdminPollModel(

    @SerializedName("pollId")
    val pollId: String,
    @SerializedName("userName")
    val userName: String,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("isAnswered")
    val isAnswered: Boolean,
    @SerializedName("isAdminCreatedPoll")
    val isAdminCreatedPoll: Boolean,
    @SerializedName("pollTypeDisplay")
    val pollTypeDisplay: Int,
    @SerializedName("_id")
    val id: String,
    @SerializedName("userId")
    val userId: String,
    @SerializedName("categoryId")
    val categoryId: String,
    @SerializedName("categoryImageUrl")
    val categoryImageUrl: String,
    @SerializedName("pollTitle")
    val pollTitle: String,
    @SerializedName("pollImages")
    val pollImages: List<String>,
    @SerializedName("pollSubTitle")
    val pollSubTitle: String,
    @SerializedName("startDate")
    val startDate: String,
    @SerializedName("endDate")
    val endDate: String,
    @SerializedName("pollType")
    val pollType: String,
    @SerializedName("pollOptions")
    val pollOptions: List<PollOption>,
    @SerializedName("allowVotersToChangeAnswer")
    val allowVotersToChangeAnswer: Boolean,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("updatedAt")
    val updatedAt: String,
    @SerializedName("__v")
    val version: Int,
    @SerializedName("totalPollprintCount")
    val totalPollprintCount: Int,
    @SerializedName("isEnabled")
    val isEnabled: Boolean,
    @SerializedName("researchData")
    val researchData: List<Any>
)

data class PollOption(
    @SerializedName("option")
    val option: String,
    @SerializedName("optionUrl")
    val optionUrl: List<String>,
    @SerializedName("_id")
    val id: String,
    @SerializedName("answerCount")
    val answerCount: JsonElement
)

data class PollOptions(
    @SerializedName("optionTitle")
    val option: String,
    @SerializedName("optionId")
    val id: String,
    @SerializedName("answerCount")
    val answerCount: JsonElement
)