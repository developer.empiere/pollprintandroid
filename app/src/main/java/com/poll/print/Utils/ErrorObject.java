package com.poll.print.Utils;

import java.io.Serializable;

public class ErrorObject implements Serializable {
    private String message;
    private int status;
    private int code;
    private String developerMessage;
    private String field1;
    private String field2;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}