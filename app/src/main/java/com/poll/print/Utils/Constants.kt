package com.poll.print.Utils

object Constants {

    interface RequestPermission {
        companion object {
            const val REQUEST_GALLERY = 1000
            const val REQUEST_CAMERA = 1001
        }
    }

    interface PRO_USER_STATUS {
        companion object {
            const val SUBMITTED = 1
            const val REJECTED = 2
            const val ACCEPTED = 3
            const val NOTAPLLIED = 4

        }
    }

    interface TimeOut {
        companion object {
            const val IMAGE_UPLOAD_CONNECTION_TIMEOUT = 120
            const val IMAGE_UPLOAD_SOCKET_TIMEOUT = 120
            const val SOCKET_TIME_OUT = 60
            const val CONNECTION_TIME_OUT = 60
        }
    }

    interface GoogleAdsData {
        companion object {
            const val APPLICATION_AD_ID = "ca-app-pub-6224624441366955~9558396055"
            const val BANNER_AD_ID = "ca-app-pub-3940256099942544/9214589741"  //TestID
            //const val BANNER_AD_ID = "ca-app-pub-6224624441366955/6500987870"  //LiveID
        }
    }

    interface UrlPath {
        companion object {

            //const val API_URL = "http://194.163.176.34:5500/api/v1/"
            const val API_URL = "https://api.pollprint.org/api/v1/"

            const val APP_TERMS_AND_CONDITION_LINK = "https://shomaa.org/terms-and-conditions/"
            const val APP_PRIVACY_POLICY_LINK = "https://shomaa.org/privacy-policy/"
            const val APP_ABOUT_US_LINK = "https://shomaa.org/"

            const val LOGIN = "users/loginUser"
            const val LOGOUT = "users/logout"
            const val REGISTER = "users/registerUser"
            const val NUMBER_EXISTS = "users/checkNumberExists"
            const val UPDATE_USER_PROFILE = "users/updateUser"
            const val UPDATE_PASSWORD = "users/updatePassword"
            const val CHANGE_PASSWORD = "users/changePassword"
            const val GET_COUNTRY = "country/getCountriesList"
            const val GET_STATE = "state/getStateList"
            const val GET_CITY = "city/getCityList"
            const val GET_POLL_CATEGORY_LIST = "category/getPollCategoryList"
            const val CREATE_POLL = "poll/createPoll"
            const val SUBMIT_POLL_ANS = "poll/submitPollAnswer"
            const val GET_ALL_POLL = "poll/getAllPolls"
            const val GET_USER_PROFILE = "users/getUserDetails"
            const val UPLOAD_IMAGE = "users/uploadProfilePic"
            const val GET_MY_POLL = "poll/getMyPolls"
            const val MY_POLL_DETAILS = "poll/getMyPollDetails"
            const val GET_MY_ACTIVITY = "poll/getMyActivity"
            const val GET_MY_ACTIVITY_DETAILS = "poll/getMyActivityDetails"
            const val ADD_USER_OWN_RESEARCH = "poll/addUserOwnResearch"
            const val GET_POLL_DETAILS = "poll/getPollDetails"
            const val PRO_USER = "users/createPremiumUser"
            const val RELIGIONLIST = "religion/getReligionList"
            const val PROFESSION = "profession/getProfession"
            const val GET_PRO_USER_LANGUAGE = "language/getLanguages"
        }
    }

    interface ErrorClass {
        companion object {
            const val CODE = "code"
            const val STATUS = "status"
            const val MESSAGE = "message"
            const val DEVELOPER_MESSAGE = "developerMessage"
        }
    }

    interface ResponseCode {
        companion object {
            const val CODE_200 = 200
            const val CODE_204 = 204
            const val CODE_500 = 500
            const val CODE_400 = 400
            const val CODE_401 = 401
            const val CODE_403 = 403
            const val CODE_404 = 404
            const val CODE_422 = 422
            const val CODE_428 = 428
            const val CODE_429 = 429
        }
    }
}