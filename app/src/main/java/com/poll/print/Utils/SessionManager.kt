package com.poll.print.Utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.Login.LoginActivity

class SessionManager(private val _context: Context?) {

    private val pref: SharedPreferences
    private val editor: SharedPreferences.Editor
    private val PRIVATE_MODE = 0

    init {
        pref = _context!!.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun setData(key: String?, value: String?) {
        editor.putString(key, value)
        editor.commit()
    }

    fun setData(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun setData(key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    fun setUserLogin(isUserLogin: Boolean) {
        editor.putBoolean(IS_USER_LOGIN, isUserLogin)
        editor.commit()
    }

    fun checkLogin() {
        isLoggedIn = pref.getBoolean(IS_USER_LOGIN, false)
        if (isLoggedIn) {
            val i = Intent(_context, DashBoardActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            _context!!.startActivity(i)
        } else {
            val i = Intent(_context, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            _context!!.startActivity(i)
        }
    }

    var isLoggedIn: Boolean = false

    fun getData(key: String?): String? {
        return pref.getString(key, "")
    }

    fun getData(key: String?, defaultValue: String?): String? {
        return pref.getString(key, defaultValue)
    }

    fun getAuthToken(key: String?): String? {
        return pref.getString(key, "")
    }

    fun getIntData(key: String?): Int {
        return pref.getInt(key, 11)
    }

    fun getBooleanData(key: String?): Boolean {
        return pref.getBoolean(key, true)
    }

    companion object {
        private const val PREFER_NAME = "4hire"
        private const val IS_USER_LOGIN = "is_user_loggedIn"
        const val AUTH_TOKEN = "auth_token"
        const val FCM_TOKEN = "fcm_token"
        const val PRO_USER_STATUS = "auth_token"
        const val USER_ID = "user_id"
        const val USER_EMAIL = "userEmail"
        const val IS_USER_PREMIUM = "isPremiumUser"
        const val USER_PREMIUM_STATUS = "premiumUserStatus"
        const val SORT_ID = "sort_id"
        const val PHONE_NUMBER = "phone_number"
        const val USER_NAME = "userName"
        const val USER_PROFILE = "userProfileImage"
        const val USER_PROFILE_IMAGE = "userProfileImage"
        const val USER_BIRTH_DATE = "birthDate"
        const val USER_MOBILE = "mobile"
        const val USER_COUNTRY = "country"
        const val USER_COUNTRY_ID = "country_id"
    }
}


