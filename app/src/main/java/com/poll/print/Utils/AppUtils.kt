package com.poll.print.Utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import com.poll.print.dialog.AlertDialog

object AppUtils {
    fun View.setVisible() {
        this.visibility = View.VISIBLE
    }

    fun View.setInVisible() {
        this.visibility = View.INVISIBLE
    }

    fun View.setGone() {
        this.visibility = View.GONE
    }

    fun isEditTextEmpty(editText: EditText): Boolean {
        return editText.text.toString().trim { it <= ' ' } == ""
    }

    fun showAlertDialog(
        context: Context?,
        title: String?,
        message: String?,
        positiveButton: String?
    ) {
        val alertDialog = AlertDialog(context)
        alertDialog.setCancelable(false)
        alertDialog.title = title
        alertDialog.message = message
        alertDialog.setPositiveButton(
            positiveButton,
            View.OnClickListener { alertDialog.dismiss() })
        alertDialog.show()
    }

    fun setStatusBar(window: Window, activity: Activity) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    private fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
        val window = activity.window
        val winParams = window.attributes
        if (on) winParams.flags = winParams.flags or bits else winParams.flags =
            winParams.flags and bits.inv()
        window.attributes = winParams
    }

    fun isValidEmail(editText: EditText): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(getEditTextValue(editText)!!).matches()
    }

    fun getEditTextValue(editText: EditText): String {
        return editText.text.toString()
    }
}