package com.poll.print.Utils

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.ViewGroup
import android.view.WindowMetrics
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AdsManager(private val context: Context, private val adContainer: ViewGroup) {

    private var adView: AdView? = null

    // Initialize Mobile Ads SDK
    /*fun initializeMobileAdsSdk(onInitialized: (() -> Unit)? = null) {
        CoroutineScope(Dispatchers.IO).launch {
            MobileAds.initialize(context) {
                onInitialized?.invoke() // Callback after initialization
            }
        }
    }*/

    // Load a banner ad
    fun loadBannerAd() {
        CoroutineScope(Dispatchers.Main).launch {
            // Create a new AdView
            val adsView = AdView(context)
            adsView.adUnitId = Constants.GoogleAdsData.BANNER_AD_ID
            adsView.setAdSize(adSize)
            adView = adsView

            // Add the AdView to the container
            adContainer.removeAllViews()
            adContainer.addView(adView)

            // Load the ad
            val adRequest = AdRequest.Builder().build()
            adView!!.loadAd(adRequest)
        }
    }

    private val adSize: AdSize
        get() {
            val displayMetrics = context.resources.displayMetrics
            val adWidthPixels =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    val windowMetrics: WindowMetrics =
                        (context as Activity).windowManager.currentWindowMetrics
                    windowMetrics.bounds.width()
                } else {
                    displayMetrics.widthPixels
                }
            val density = displayMetrics.density
            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                context,
                adWidth
            )
        }
}