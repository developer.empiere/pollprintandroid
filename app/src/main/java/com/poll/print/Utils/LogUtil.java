package com.poll.print.Utils;

import android.util.Log;

class LogUtil {
    public static boolean ISDEBUG = true;

    /**
     * Prints given tag and exception as error log, only if in debug mode with message.
     *
     * @param tag - Tag to be set for logs
     * @param msg - Log message
     * @param ex  - Exception to be logged
     */
    public static void error(String tag, String msg, Throwable ex) {
        if (LogUtil.ISDEBUG) {
            Log.e(tag, msg, ex);
        }
    }
}
