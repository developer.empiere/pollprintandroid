package com.poll.print.Utils;

import java.io.Serializable;

public class Logger implements Serializable {
    private String tag;

    public Logger(String tag) {
        this.tag = tag;
    }

    /**
     * Prints the stack trace for the given throwable instance for debug build.
     *
     * @param ex
     */
    public void error(Throwable ex) {
        LogUtil.error(this.tag, "", ex);
    }
}
