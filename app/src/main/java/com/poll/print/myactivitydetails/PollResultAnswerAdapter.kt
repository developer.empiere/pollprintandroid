package com.poll.print.myactivitydetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import com.poll.print.Utils.SessionManager

class PollResultAnswerAdapter(
    private val context: Context?,
    var answerIds: List<String>,
    var pollValue: Int,
    var answerIdsWithPercentage: List<AnswerValues>?
) : RecyclerView.Adapter<PollResultAnswerAdapter.ViewHolder>() {

    lateinit var mSessionManager: SessionManager
    var user_sort_id = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.poll_result_answer_ids_items, parent, false)

        mSessionManager = SessionManager(context)
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (answerIdsWithPercentage == null) {
            holder.tvAnswerIds.text = answerIds[position]

            if (answerIds[position].contains(user_sort_id)) {
                holder.tvAnswerIds.background =
                    context!!.resources.getDrawable(R.drawable.bg_create_poll)
                holder.tvAnswerIds.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
            }
        } else {
            holder.tvAnswerIds.text =
                "${answerIdsWithPercentage!![position].sortId} - ${answerIdsWithPercentage!![position].percentage}%"

            if (answerIdsWithPercentage!![position].sortId.contains(user_sort_id)) {
                holder.tvAnswerIds.background =
                    context!!.resources.getDrawable(R.drawable.bg_create_poll)
                holder.tvAnswerIds.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
            }
        }
    }

    override fun getItemCount(): Int {
        return answerIds.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvAnswerIds: TextView = itemView.findViewById(R.id.tvAnswerIds)
    }

}