package com.poll.print.myactivitydetails

import com.google.gson.JsonElement

data class ResearchData(
    val userId: String,
    val userAnswer: String,
    val isEnabled: Boolean,
    val researchDate: String,
    val _id: String
)

data class PollOption(
    val optionId: String,
    val optionTitle: String,
    val optionImages: List<String>,
    val answerCount: JsonElement,
    val answerIds: List<String>,
    val answerIdsWithPer: List<AnswerValues>,
    val isOptionAnswer: Boolean,
    val userAnswerPercentage: Any?
)

data class AnswerValues(
    val sortId: String,
    val percentage: Int
)

data class PollDetails(
    val pollId: String,
    val userName: String,
    val sort_id: String,
    val imageURL: Any?,
    val totalAnswers: Int,
    val pollValue: Int,
    val pollTitle: String,
    val pollSubTitle: String,
    val timeStamp: String,
    val pollType: String,
    val pollImages: List<String>,
    val pollOptionsCount: Int,
    val allowVotersToChangeAnswer: Boolean,
    val researchData: List<ResearchData>,
    val pollOptions: List<PollOption>
)

data class PollResponse(
    val status: Boolean,
    val message: String,
    val pollDetails: PollDetails,
    val pollData: PollDetails
)