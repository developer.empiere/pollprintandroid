package com.poll.print.myactivitydetails

import android.content.Intent
import android.os.Bundle
import android.util.ArrayMap
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.AppUtils.setGone
import com.poll.print.Utils.AppUtils.setInVisible
import com.poll.print.Utils.AppUtils.setVisible
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityMyActivityDetailsBinding
import com.poll.print.mypolldetail.MyUserPollOptionsModel
import com.poll.print.pollsubmit.PercentageData
import com.poll.print.pollsubmit.PollSubmitModel
import com.poll.print.pollsubmit.RePollSubmitAdapter
import com.poll.print.pollsubmiteddetail.SubmitedPollDetailActivity
import com.poll.print.prouser.ProUserActivity
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject
import java.util.Collections


class MyActivityDetailsActivity : AppCompatActivity(), RePollSubmitAdapter.ProgressCounterUpdater {

    lateinit var binding: ActivityMyActivityDetailsBinding

    lateinit var mSessionManager: SessionManager
    var userId: String = ""
    var userProfile: String = ""

    var pollId = ""
    var pollType = 0
    var pollOptionId: String = ""
    var likelyRating: String = ""
    var pollTypeDisplay = ""
    var allowVotersToChangeAnswer = false

    var isPollExpired = false

    var isFromHome = false

    var multiArrayList: ArrayList<String> = arrayListOf()
    val multiOptionsArray = JsonArray()
    val preferanceOptionsArray = JsonArray()

    private var pollSubmitModel = ArrayList<PollSubmitModel>()

    private var myUserPollOptionsModel = mutableListOf<MyUserPollOptionsModel>()

    var isPieChart = false

    var user_sort_id = ""

    private var myActivityOwnResearchModel = mutableListOf<MyActivityOwnResearchModel>()
    private lateinit var myActivityOwnResearchAdapter: MyActivityOwnResearchAdapter
    private lateinit var barLegendsAdapter: BarLegendsAdapter
    private lateinit var pollResultDetailsAdapter: PollResultAdapter

    private lateinit var pollSubmitAdapter: RePollSubmitAdapter

    var currentProgress = 0
    var sharingValue: String = ""

    val options = ArrayList<String>()
    val optionsIDList = ArrayList<String>()
    val percentage = ArrayList<String>()

    lateinit var optionsColorList: ArrayMap<String, Int>

    lateinit var pieDataInformation: ArrayList<PieEntry>
    lateinit var pieDataSet: PieDataSet

    lateinit var barDataInformation: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barDataSetList: ArrayList<BarDataSet>

    lateinit var barData: BarData

    var userName: String = ""

    lateinit var material_color_list: IntArray

    var userPremiumStatus: Int = 0
    var isUserPremium: Boolean = false

    private var isRepoll = false

    private lateinit var pollResultDetailsUpdateAdapter: PollResultUpdatedAdapter

    val answerCountList = mutableListOf<List<String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMyActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        likelyRating = binding.tvLikely.text.toString()

        mSessionManager = SessionManager(this@MyActivityDetailsActivity)
        userId = mSessionManager.getData(SessionManager.USER_ID).toString()
        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()

        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        userPremiumStatus = mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS)
        isUserPremium = mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)

        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()


        if (userName != null && userName != "null")
            binding.tvUname.text = userName

        material_color_list = intArrayOf(
            ColorTemplate.rgb("#2ecc71"),
            ColorTemplate.rgb("#f1c40f"),
            ColorTemplate.rgb("#e74c3c"),
            ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#e9724d"),
            ColorTemplate.rgb("#d6d727"),
            ColorTemplate.rgb("#92cad1"),
            ColorTemplate.rgb("#79ccb3"),
            ColorTemplate.rgb("#868686"),
            ColorTemplate.rgb("#391954"),
            ColorTemplate.rgb("#631e50"),
            ColorTemplate.rgb("#a73c5a"),
            ColorTemplate.rgb("#b0d7e1"),
            ColorTemplate.rgb("#a7e237"),
            ColorTemplate.rgb("#37bd79")
        )

        Glide.with(this@MyActivityDetailsActivity)
            .load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        getIntentData()

        setOnclickListener()
        getActivityDetails()
    }

    private fun getActivityDetails() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        jsonObject.addProperty("pollId", pollId)

        var apiLink = ""

        if (!isFromHome)
            apiLink = Constants.UrlPath.GET_MY_ACTIVITY_DETAILS
        else
            apiLink = Constants.UrlPath.GET_POLL_DETAILS

        ApiServiceProvider.getInstance(this@MyActivityDetailsActivity).sendPostData(
            apiLink, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                val pollResponseType = object : TypeToken<PollResponse>() {}.type
                val pollResponse: PollResponse =
                    Gson().fromJson(responseObj.toString(), pollResponseType)

                if (status) {
                    val pollDetails = responseObj.getJSONObject("pollData")
                    var pollOptions = pollDetails.getJSONArray("pollOptions")
                    val researchData = pollDetails.getJSONArray("researchData")
                    val userName = pollDetails.getString("userName")
                    val imageURL = pollDetails.getString("imageURL")
                    val pollTitle = pollDetails.getString("pollTitle")
                    val pollSubTitle = pollDetails.getString("pollSubTitle")
                    val totalAnswers = pollDetails.getInt("totalAnswers")
                    allowVotersToChangeAnswer = pollDetails.getBoolean("allowVotersToChangeAnswer")

                    if (pollDetails.has("pollValue")) {
                        sharingValue = pollDetails.getString("pollValue")
                    }

                    pollSubmitModel = Gson().fromJson<Any>(
                        pollOptions.toString(),
                        object : TypeToken<ArrayList<PollSubmitModel>?>() {}.type
                    ) as ArrayList<PollSubmitModel>

                    if (!allowVotersToChangeAnswer)
                        binding.btnChangePoll.visibility = View.GONE

                    if(userName != "null")
                        binding.tvUserName.text = userName
                    else
                        binding.tvUserName.text = ""

                    binding.tvUserQuestion.text = pollTitle
                    binding.tvPollAns.text = totalAnswers.toString() + " Pollprint"
                    binding.tvUserQuestionDescription.text = pollSubTitle

                    Glide.with(this@MyActivityDetailsActivity).load(imageURL)
                        .placeholder(R.drawable.ic_no_profile).into(binding.imgUser)

                    options.clear()
                    optionsIDList.clear()
                    percentage.clear()

                    for (i in 0 until pollOptions.length()) {
                        options.add(pollOptions.getJSONObject(i).getString("optionTitle"))
                        optionsIDList.add(pollOptions.getJSONObject(i).getString("optionId"))

                        if (pollType != 3)
                            percentage.add(pollOptions.getJSONObject(i).getString("answerCount"))
                        else {
                            try {
                                val option = pollOptions.getJSONObject(i)
                                val answerCount = option.getJSONObject("answerCount")

                                val valuesList = mutableListOf<String>()
                                answerCount.keys().forEach {
                                    valuesList.add(answerCount.getString(it))
                                }
                                answerCountList.add(valuesList)

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                    if (pollType == 3) {
                        val userPollOptions = pollDetails.getJSONArray("userPollOption")

                        if (userPollOptions.length() != 0) {
                            myUserPollOptionsModel = Gson().fromJson<Any>(
                                userPollOptions.toString(),
                                object : TypeToken<MutableList<MyUserPollOptionsModel>?>() {}.type
                            ) as MutableList<MyUserPollOptionsModel>
                            Log.e("abc", "UserPollOption : ${myUserPollOptionsModel.toString()}")
                        }

                    }

                    //Preference choice = 3 , Yes/No = 0 , Percent share = 4 , multiplechoice = 2
                    if (totalAnswers > 0) {
                        if (pollType == 2 || pollType == 4)
                            showMultipleSelectionChart()
                        /*else if (pollType == 4)
                        showPercentageSharingChart()*/
                        else if (pollType == 3)
                            showPreferenceChoiceChart()
                        else
                            showPieChart()
                    } else {
                        binding.pieChart.visibility = View.GONE
                        binding.barChart.visibility = View.GONE
                        binding.rvBarLegend.visibility = View.GONE
                        binding.rvPollResult.visibility = View.GONE
                        binding.tvNoDataChart.visibility = View.VISIBLE
                    }

                    if (totalAnswers > 0)
                        setPollResultDetails(pollResponse)
                    else
                        binding.clResultList.visibility = View.GONE

                    if (researchData.length() == 0) {
                        binding.tvNoData.visibility = View.VISIBLE
                    } else {
                        myActivityOwnResearchModel = Gson().fromJson<Any>(
                            researchData.toString(),
                            object : TypeToken<MutableList<MyActivityOwnResearchModel>?>() {}.type
                        ) as MutableList<MyActivityOwnResearchModel>
                        setOwnRearchData()
                    }

                    if (!isUserPremium || (userPremiumStatus != Constants.PRO_USER_STATUS.ACCEPTED)) {
                        binding.clDoOwnResearch.visibility = View.GONE
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showMultipleSelectionChart() {

        //MPChart
        barDataInformation = ArrayList()

        for (index in percentage.indices) {
            try {
                barDataInformation.add(
                    BarEntry(
                        (index + 1).toFloat(),
                        percentage[index].replace("%", "").replace("\"", "").toFloat()
                    )
                )
                barDataSet = BarDataSet(barDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // Custom value formatter to format the value
        barDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "0%" else ("" + value.toInt() + "%")
            }
        }

        barDataSet.setColors(material_color_list, 250)
        barDataSet.valueTextColor = resources.getColor(R.color.black)
        barDataSet.valueTextSize = 7f

        barData = BarData(barDataSet)

        binding.barChart.setFitBars(true)
        binding.barChart.legend.isEnabled = false
        binding.barChart.data = barData
        binding.barChart.description.text = ""
        binding.barChart.animateY(700)

        binding.barChart.invalidate()

        if (options.size != 0)
            setBarLegends(options, percentage, material_color_list)

    }

    private fun showPreferenceChoiceChart() {

        barDataSetList = ArrayList()
        barData = BarData()

        var numm = 1f

        try {
            for (index in answerCountList.indices) {

                barDataInformation = ArrayList()

                for (i in 0 until answerCountList[index].size) {

                    barDataInformation.add(
                        BarEntry(
                            numm,
                            answerCountList[index][i].replace("%", "").replace("\"", "")
                                .toFloat()
                        )
                    )
                    numm++
                }

                barDataSet = BarDataSet(barDataInformation, options[index])
                barDataSet.color = material_color_list[index]
                barDataSetList.add(barDataSet)

                numm += 2 //Required for x-coordinator for bar graph

            }

            // Custom value formatter to format the value
            barDataSet.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return if (value == 0f) "0%" else ("" + value.toInt() + "%")
                }
            }

            for (i in 0 until barDataSetList.size)
                barData.addDataSet(barDataSetList[i])

            binding.barChart.data = barData
            binding.barChart.legend.isEnabled = false

            barDataSet.valueTextColor = resources.getColor(R.color.black)
            barDataSet.valueTextSize = 7f

            val groupSpace = 0.8f
            val barSpace = 0.03f
            val barWidth = 1f

            barData.barWidth = barWidth

            binding.barChart.description.isEnabled = false
            binding.barChart.xAxis.setCenterAxisLabels(true)
            binding.barChart.setFitBars(true)
            binding.barChart.groupBars(0f, groupSpace, barSpace)
            binding.barChart.animateY(700)

            binding.barChart.invalidate()

            if (options.size != 0)
                setBarLegends(options, percentage, material_color_list)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showPieChart() {

        isPieChart = true

        binding.pieChart.visibility = View.VISIBLE
        binding.barChart.visibility = View.GONE

        //MPChart
        pieDataInformation = ArrayList()
        for (index in percentage.indices) {
            try {
                pieDataInformation.add(
                    PieEntry(
                        percentage[index].replace("%", "").replace("\"", "").toFloat(),
                        options[index]
                    )
                )
                pieDataSet = PieDataSet(pieDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        pieDataSet.colors = material_color_list.asList()
        pieDataSet.valueTextSize = 8f

        val data = PieData(pieDataSet)

        binding.pieChart.data = data
        binding.pieChart.description.isEnabled = false
        binding.pieChart.isDrawHoleEnabled = false
        binding.pieChart.setDrawEntryLabels(false)
        binding.pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        binding.pieChart.legend.isWordWrapEnabled = true
        binding.pieChart.legend.isEnabled = false

        // Center text inside slices
        pieDataSet.xValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE
        pieDataSet.yValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE

        // Custom value formatter to format the value
        pieDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "" else ("" + value.toInt() + "%")
            }
        }

        binding.pieChart.invalidate()

        if (options.size != 0) {
            val params = binding.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
            params.topToBottom = binding.pieChart.id
            setBarLegends(options, percentage, material_color_list)
        }

    }

    private fun getIntentData() {
        pollId = intent.getStringExtra("pollId").toString()
        isFromHome = intent.getBooleanExtra("isFromHome", false)
        pollType = intent.getIntExtra("PollType", 0)
        isPollExpired = intent.getBooleanExtra("isPollExpired", false)
        pollTypeDisplay = intent.getStringExtra("PollDisplayType").toString()
    }

    private fun setOwnRearchData() {
        myActivityOwnResearchAdapter =
            MyActivityOwnResearchAdapter(
                this@MyActivityDetailsActivity,
                myActivityOwnResearchModel
            )
        binding.rvDoOwnResearch.adapter = myActivityOwnResearchAdapter
    }

    private fun setPollResultDetails(pollResponse: PollResponse) {

        if (pollType != 4) {
            var answerIdList = ArrayList<String>()
            val regex = Regex("^p\\d+")

            for (i in 0 until pollResponse.pollData.pollOptions.size) {
                for (j in 0 until pollResponse.pollData.pollOptions[i].answerIds.size) {

                    val unique_usid =
                        regex.find(pollResponse.pollData.pollOptions[i].answerIds[j])?.value

                    if (unique_usid == user_sort_id) {
                        answerIdList.add(0, pollResponse.pollData.pollOptions[i].answerIds[j])
                    } else {
                        answerIdList.add(pollResponse.pollData.pollOptions[i].answerIds[j])
                    }
                }
            }

            val distinctAnswerIdList = answerIdList.distinct()

            pollResultDetailsUpdateAdapter =
                PollResultUpdatedAdapter(
                    this@MyActivityDetailsActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse,
                    distinctAnswerIdList,
                    optionsColorList,
                    myUserPollOptionsModel
                )
            binding.rvPollResult.adapter = pollResultDetailsUpdateAdapter
        } else {
            pollResultDetailsAdapter =
                PollResultAdapter(
                    this@MyActivityDetailsActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse
                )

            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.rvPollResult.layoutManager = layoutManager
            binding.rvPollResult.adapter = pollResultDetailsAdapter
        }

    }

    private fun setBarLegends(
        options: ArrayList<String>,
        percentage: ArrayList<String>,
        materialColors: IntArray
    ) {

        optionsColorList = ArrayMap<String, Int>()

        for (i in 0 until options.size) {
            optionsColorList.put(optionsIDList[i], material_color_list[i])
        }

        barLegendsAdapter =
            BarLegendsAdapter(
                this@MyActivityDetailsActivity,
                options,
                percentage,
                materialColors
            )
        binding.rvBarLegend.layoutManager = GridLayoutManager(this, 2)
        binding.rvBarLegend.adapter = barLegendsAdapter
    }

    private fun setOnclickListener() {
        binding.apply {
            imgClose.setOnClickListener {
                finish()
            }
            tvUserQuestion.setOnClickListener {
                if (tvUserQuestion.tag == "1") {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_expand, 0
                    )
                    tvUserQuestionDescription.visibility = View.VISIBLE

                    if (!isPieChart)
                        barChart.visibility = View.VISIBLE
                    else
                        pieChart.visibility = View.VISIBLE

                    rvBarLegend.visibility = View.VISIBLE
                    clResultList.visibility = View.VISIBLE
                    clGraph.visibility = View.VISIBLE

                    tvUserQuestion.tag = "0"
                } else {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_collapse, 0
                    )
                    tvUserQuestionDescription.visibility = View.GONE

                    if (!isPieChart)
                        barChart.visibility = View.GONE
                    else
                        pieChart.visibility = View.GONE

                    rvBarLegend.visibility = View.GONE
                    clResultList.visibility = View.GONE
                    clGraph.visibility = View.GONE

                    tvUserQuestion.tag = "1"
                }
            }

            btnChangePoll.setOnClickListener {
                if (allowVotersToChangeAnswer) {

                    if (!isPollExpired) {
                        isRepoll = true

                        btnSubmit.setVisible()
                        btnChangePoll.setInVisible()
                        rvPollMyAns.setVisible()
                        clLikely.setVisible()

                        if (pollTypeDisplay == "Percentage sharing on option") {
                            if (allowVotersToChangeAnswer) {
                                binding.tvMaximumCount.visibility = View.VISIBLE
                                binding.tvMaximumCount.text = "$currentProgress/$sharingValue"
                            }
                        }

                        setPollOptionData()
                    } else {
                        Toast.makeText(
                            this@MyActivityDetailsActivity,
                            "Poll is Expired",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } else {
                    Toast.makeText(
                        this@MyActivityDetailsActivity,
                        "allowVotersToChangeAnswer is false",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            btnSubmit.setOnClickListener {

                isRepoll = false

                if (pollTypeDisplay == "Yes/No" || pollTypeDisplay == "Single selection from multiple choice") {
                    if (pollOptionId == "") {
                        Toast.makeText(
                            this@MyActivityDetailsActivity,
                            "Please choose at least one answer",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        submitYourPoll()
                    }
                } else if (pollTypeDisplay == "Multiple choice") {
                    if (multiArrayList.isEmpty()) {
                        Toast.makeText(
                            this@MyActivityDetailsActivity,
                            "Please choose at least one answer",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        for (i in 0 until multiArrayList.size) {
                            multiOptionsArray.add(multiArrayList[i])
                        }
                        submitPollForMultipleChoice(multiOptionsArray)
                    }
                } else if (pollTypeDisplay == "Preference choice") {
                    for (i in 0 until pollSubmitModel.size) {
                        preferanceOptionsArray.add(pollSubmitModel[i].optionId)
                    }
                    submitPollForMultipleChoice(preferanceOptionsArray)
                } else if (pollTypeDisplay == "Percentage sharing on option") {
                    submitPollPercentageSharing()
                }
            }

            imgSend.setOnClickListener {
                if (edtDoOwnResearch.text.toString().trim() != "")
                    addYourOwnResearch()
            }
            clOne.setOnClickListener {
                imgBg1.setVisible()
                likelyRating = binding.tvLikely.text.toString()
                imgBg2.setGone()
                imgBg3.setGone()
            }

            clTwo.setOnClickListener {
                imgBg1.setGone()
                imgBg2.setVisible()
                likelyRating = binding.tvSomewhatLikely.text.toString()
                imgBg3.setGone()
            }
            clThree.setOnClickListener {
                imgBg1.setGone()
                imgBg2.setGone()
                imgBg3.setVisible()
                likelyRating = binding.tvUnlikely.text.toString()
            }
            btnProUser.setOnClickListener {

                val userCountry = mSessionManager.getData(SessionManager.USER_COUNTRY)

                val i = Intent(this@MyActivityDetailsActivity, ProUserActivity::class.java)
                i.putExtra("userCountry", userCountry)
                startActivity(i)
            }
        }
    }

    var simpleCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END,
        0
    ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            val fromPosition = viewHolder.adapterPosition
            val toPosition = target.adapterPosition
            Collections.swap(pollSubmitModel, fromPosition, toPosition)
            recyclerView.adapter!!.notifyItemMoved(fromPosition, toPosition)
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}
    }

    private fun setPollOptionData() {
        pollSubmitAdapter = RePollSubmitAdapter(
            this@MyActivityDetailsActivity,
            pollSubmitModel,
            pollId,
            pollType,
            sharingValue,
            this
        )
        binding.rvPollMyAns.adapter = pollSubmitAdapter

        if (pollTypeDisplay == "Preference choice") {
            val itemTouchHelper = ItemTouchHelper(simpleCallback)
            itemTouchHelper.attachToRecyclerView(binding.rvPollMyAns)
        }

        pollSubmitAdapter.setOnAnyItemClickListener { optId ->
            pollOptionId = optId
        }

        pollSubmitAdapter.setOnMultiItemClickListener { isCheck, optId ->
            if (isCheck) {
                multiArrayList.add(optId)
            } else {
                for (i in 0 until multiArrayList.size) {
                    if (multiArrayList[i] == optId) {
                        multiArrayList.removeAt(i)
                        break
                    }
                }
            }
        }
    }

    private fun submitYourPoll() {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.addProperty("optionId", pollOptionId)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@MyActivityDetailsActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@MyActivityDetailsActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(
                        this@MyActivityDetailsActivity,
                        SubmitedPollDetailActivity::class.java
                    )
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollTypeDisplay)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitPollForMultipleChoice(multiOptionsArrayParamter: JsonArray) {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.add("optionIds", multiOptionsArrayParamter)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@MyActivityDetailsActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@MyActivityDetailsActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(
                        this@MyActivityDetailsActivity,
                        SubmitedPollDetailActivity::class.java
                    )
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollTypeDisplay)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitPollPercentageSharing() {

        var overallPercentageCount = 0

        var percentageDataArrayList = ArrayList<PercentageData>()

        for (i in 0 until pollSubmitModel.size) {

            var percentageCalculation =
                (pollSubmitModel[i].seekBarProgress * 100) / sharingValue.toInt()

            percentageDataArrayList.add(
                PercentageData(
                    pollSubmitModel[i].optionId.toString(),
                    percentageCalculation
                )
            )

            overallPercentageCount += percentageCalculation
        }

        if (overallPercentageCount == 0) {
            Toast.makeText(this@MyActivityDetailsActivity, "Please give voting", Toast.LENGTH_SHORT)
                .show()
            return
        }

        val jsonElement: JsonElement = Gson().toJsonTree(percentageDataArrayList)

        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )

        requestData.addProperty("pollId", pollId)
        requestData.add("percentageData", jsonElement)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@MyActivityDetailsActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@MyActivityDetailsActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(
                        this@MyActivityDetailsActivity,
                        SubmitedPollDetailActivity::class.java
                    )
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollTypeDisplay)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun addYourOwnResearch() {
        val requestData = JsonObject()
        requestData.addProperty("userId", userId)
        requestData.addProperty("pollId", pollId)
        requestData.addProperty("userAnswer", binding.edtDoOwnResearch.text.toString())
        ApiServiceProvider.getInstance(this@MyActivityDetailsActivity).sendPostData(
            Constants.UrlPath.ADD_USER_OWN_RESEARCH, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@MyActivityDetailsActivity,
                        "" + message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    binding.edtDoOwnResearch.setText("")
                    binding.tvNoData.visibility = View.GONE
                    getActivityDetails()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun updateText(newText: String) {
        binding.tvMaximumCount.text = "$newText/$sharingValue"
    }

    override fun onResume() {
        super.onResume()

        if (mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)) {
            if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) != Constants.PRO_USER_STATUS.ACCEPTED) {
                if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.SUBMITTED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                    mSessionManager.setData(
                        SessionManager.USER_PREMIUM_STATUS,
                        Constants.PRO_USER_STATUS.SUBMITTED
                    )
                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, true)
                } else if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.NOTAPLLIED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                    mSessionManager.setData(
                        SessionManager.USER_PREMIUM_STATUS,
                        Constants.PRO_USER_STATUS.SUBMITTED
                    )
                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, true)
                } else if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.REJECTED) {
                    binding.btnProUser.text = "Become a Pro User"
                    binding.btnProUser.isEnabled = true
                    binding.clDoOwnResearch.visibility = View.GONE
                    mSessionManager.setData(
                        SessionManager.USER_PREMIUM_STATUS,
                        Constants.PRO_USER_STATUS.REJECTED
                    )
                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, true)
                }
            } else {
                binding.btnProUser.text = "Pro User"
                binding.btnProUser.isEnabled = false
                binding.clDoOwnResearch.visibility = View.VISIBLE
                mSessionManager.setData(
                    SessionManager.USER_PREMIUM_STATUS,
                    Constants.PRO_USER_STATUS.ACCEPTED
                )
                mSessionManager.setData(SessionManager.IS_USER_PREMIUM, true)
            }
        } else {
            binding.btnProUser.text = "Become a Pro User"
            binding.btnProUser.isEnabled = true
            binding.clDoOwnResearch.visibility = View.GONE
            mSessionManager.setData(
                SessionManager.USER_PREMIUM_STATUS,
                Constants.PRO_USER_STATUS.REJECTED
            )
            mSessionManager.setData(SessionManager.IS_USER_PREMIUM, false)
        }

    }

}