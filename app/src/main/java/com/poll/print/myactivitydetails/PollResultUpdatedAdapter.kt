package com.poll.print.myactivitydetails

import android.content.Context
import android.util.ArrayMap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import com.poll.print.Utils.SessionManager
import com.poll.print.mypolldetail.MyUserPollOptionsModel

class PollResultUpdatedAdapter(
    private val context: Context?,
    var optionName: ArrayList<String>,
    var optionPercentage: ArrayList<String>,
    var materialColors: IntArray,
    var pollResponse: PollResponse,
    var distinctAnswerIdList: List<String>,
    var optionsColorList: ArrayMap<String, Int>,
    var myUserPollOptionsModel: MutableList<MyUserPollOptionsModel>
) : RecyclerView.Adapter<PollResultUpdatedAdapter.ViewHolder>() {

    lateinit var mSessionManager: SessionManager
    var user_sort_id = ""

    val regex = Regex("^p\\d+")

    lateinit var userPollColorOptionIdList: ArrayList<String>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.poll_result_updated_items, parent, false)

        mSessionManager = SessionManager(context)
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvAnswerID.text = distinctAnswerIdList[position]

        val unique_usid = regex.find(distinctAnswerIdList[position])?.value

        if (unique_usid == user_sort_id) {
            holder.tvAnswerID.background =
                context!!.resources.getDrawable(R.drawable.bg_create_poll)
            holder.tvAnswerID.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
        }

        if (pollResponse.pollData != null) {

            val colorList = ArrayList<Int>()

            if (!pollResponse.pollData.pollType.equals("Preference choice", true)) {
                for (i in 0 until pollResponse.pollData.pollOptions.size) {
                    for (j in 0 until pollResponse.pollData.pollOptions[i].answerIds.size) {
                        if (distinctAnswerIdList[position] == pollResponse.pollData.pollOptions[i].answerIds[j]) {
                            colorList.add(materialColors[i])
                        }
                    }
                }
            } else {

                if (pollResponse.pollData.pollType.equals("Preference choice", true)) {

                    userPollColorOptionIdList = ArrayList<String>()

                    for (i in 0 until myUserPollOptionsModel.size) {
                        for (j in 0 until myUserPollOptionsModel[position].optionIds!!.size) {
                            userPollColorOptionIdList!!.add(myUserPollOptionsModel[position].optionIds!![j])
                        }
                        break
                    }
                }

                for (k in 0 until userPollColorOptionIdList!!.size) {
                    for (l in 0 until optionsColorList.size) {
                        if (userPollColorOptionIdList!![k] == optionsColorList.keyAt(l)) {
                            colorList.add(optionsColorList.valueAt(l))
                        }
                    }
                }

            }

            holder.rvPollResultAnswers.adapter = PollResultAnswerUpdatedAdapter(
                context,
                colorList
            )

        } else {

            val colorList = ArrayList<Int>()

            if (!pollResponse.pollDetails.pollType.equals("Preference choice", true)) {
                for (i in 0 until pollResponse.pollDetails.pollOptions.size) {
                    for (j in 0 until pollResponse.pollDetails.pollOptions[i].answerIds.size) {
                        if (distinctAnswerIdList[position] == pollResponse.pollDetails.pollOptions[i].answerIds[j]) {
                            colorList.add(materialColors[i])
                        }
                    }
                }
            } else {

                if (pollResponse.pollDetails.pollType.equals("Preference choice", true)) {

                    userPollColorOptionIdList = ArrayList<String>()

                    for (i in 0 until myUserPollOptionsModel.size) {
                        for (j in 0 until myUserPollOptionsModel[position].optionIds!!.size) {
                            userPollColorOptionIdList!!.add(myUserPollOptionsModel[position].optionIds!![j])
                        }
                        break
                    }
                }

                for (k in 0 until userPollColorOptionIdList!!.size) {
                    for (l in 0 until optionsColorList.size) {
                        if (userPollColorOptionIdList!![k] == optionsColorList.keyAt(l)) {
                            colorList.add(optionsColorList.valueAt(l))
                        }
                    }
                }

            }

            holder.rvPollResultAnswers.adapter = PollResultAnswerUpdatedAdapter(
                context,
                colorList
            )

        }

    }

    override fun getItemCount(): Int {
        return distinctAnswerIdList!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvAnswerID: TextView = itemView.findViewById(R.id.tvAnswerID)
        val rvPollResultAnswers: RecyclerView = itemView.findViewById(R.id.rvPollResultAnswers)
    }

}