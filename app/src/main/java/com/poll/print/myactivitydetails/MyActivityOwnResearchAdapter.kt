package com.poll.print.myactivitydetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import com.poll.print.Utils.SessionManager

class MyActivityOwnResearchAdapter(
    private val context: Context?,
    var myActivityOwnResearchModel: List<MyActivityOwnResearchModel>?,
) : RecyclerView.Adapter<MyActivityOwnResearchAdapter.ViewHolder>() {

    lateinit var mSessionManager: SessionManager
    var user_sort_id = ""

    val regex = Regex("^p\\d+")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_my_activity_own_research, parent, false)

        mSessionManager = SessionManager(context)
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val maorm = myActivityOwnResearchModel!!.get(position)
        holder.tv_p_id.text = maorm.sort_id
        holder.tv_user_ans.text = maorm.userAnswer

        val unique_usid =
            regex.find(maorm.sort_id.toString())?.value

        if (unique_usid == user_sort_id) {
            holder.tv_p_id.background = context!!.resources.getDrawable(R.drawable.bg_create_poll)
            holder.tv_p_id.setTextColor(context.resources.getColor(R.color.colorPrimaryDark))
        }
    }

    override fun getItemCount(): Int {
        return myActivityOwnResearchModel!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tv_p_id: TextView = itemView.findViewById(R.id.tv_p_id)
        val tv_user_ans: TextView = itemView.findViewById(R.id.tv_user_ans)
    }
}