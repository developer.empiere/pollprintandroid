package com.poll.print.myactivitydetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import de.hdodenhof.circleimageview.CircleImageView

class PollResultAnswerUpdatedAdapter(
    private val context: Context?,
    private val colorList: ArrayList<Int>
) : RecyclerView.Adapter<PollResultAnswerUpdatedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.poll_result_answer_ids_updated_items, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.ivAnswerCategory.setColorFilter(colorList[position])
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivAnswerCategory: CircleImageView = itemView.findViewById(R.id.ivAnswerCategory)
    }

}