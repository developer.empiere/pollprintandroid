package com.poll.print.myactivitydetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R

class PollResultAdapter(
    private val context: Context?,
    var optionName: ArrayList<String>,
    var optionPercentage: ArrayList<String>,
    var materialColors: IntArray,
    var pollResponse: PollResponse
) : RecyclerView.Adapter<PollResultAdapter.ViewHolder>() {

    lateinit var answerID: List<String>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.poll_result_items, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (pollResponse.pollDetails != null) {
            answerID =
                pollResponse.pollDetails.pollOptions[position].answerIds
        } else {
            answerID =
                pollResponse.pollData.pollOptions[position].answerIds
        }

        try {
            holder.ivAnswerCategory.setColorFilter(materialColors[position])
        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder.tvAnswerCategory.text = optionName[position]

        if (pollResponse.pollDetails != null) {
            if (pollResponse.pollDetails.pollType == "Percentage sharing on option") {
                holder.rvPollResultAnswers.adapter = PollResultAnswerAdapter(
                    context,
                    answerID,
                    pollResponse.pollDetails.pollValue,
                    pollResponse.pollDetails.pollOptions[position].answerIdsWithPer
                )
            } else {
                holder.rvPollResultAnswers.adapter = PollResultAnswerAdapter(
                    context,
                    answerID,
                    pollResponse.pollDetails.pollValue,
                    null
                )
            }
        } else {
            if (pollResponse.pollData.pollType == "Percentage sharing on option") {
                holder.rvPollResultAnswers.adapter = PollResultAnswerAdapter(
                    context,
                    answerID,
                    pollResponse.pollData.pollValue,
                    pollResponse.pollData.pollOptions[position].answerIdsWithPer
                )
            } else {
                holder.rvPollResultAnswers.adapter = PollResultAnswerAdapter(
                    context,
                    answerID,
                    pollResponse.pollData.pollValue,
                    null
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return optionName!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivAnswerCategory: ImageView = itemView.findViewById(R.id.ivAnswerCategory)
        val tvAnswerCategory: TextView = itemView.findViewById(R.id.tvAnswerCategory)
        val rvPollResultAnswers: RecyclerView = itemView.findViewById(R.id.rvPollResultAnswers)
    }

}