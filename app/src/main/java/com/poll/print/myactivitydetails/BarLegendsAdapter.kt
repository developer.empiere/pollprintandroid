package com.poll.print.myactivitydetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R

class BarLegendsAdapter(
    private val context: Context?,
    var optionName: ArrayList<String>,
    var optionPercentage: ArrayList<String>,
    var materialColors: IntArray
) : RecyclerView.Adapter<BarLegendsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_bar_legend_items, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.ivLegend.setColorFilter(materialColors[position])
        } catch (e: Exception) {
            e.printStackTrace()
        }

        holder.tvLegend.text = optionName[position]
    }

    override fun getItemCount(): Int {
        return optionName!!.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val ivLegend: ImageView = itemView.findViewById(R.id.ivLegend)
        val tvLegend: TextView = itemView.findViewById(R.id.tvLegend)
    }

}