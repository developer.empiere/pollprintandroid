package com.poll.print.myactivitydetails

data class MyActivityOwnResearchModel(
    var userId: String? = null,
    var userAnswer: String? = null,
    var _id: String? = null,
    var sort_id: String? = null
)