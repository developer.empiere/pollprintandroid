package com.poll.print.searchpoll

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollAdapter
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollModel
import com.poll.print.databinding.ActivitySearchPollBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject
import java.util.Locale

class SearchPollActivity : AppCompatActivity() {

    lateinit var binding: ActivitySearchPollBinding

    lateinit var mSessionManager: SessionManager
    var userProfile: String = ""

    private var adminPollModel = ArrayList<AdminPollModel>()
    private lateinit var adminPollAdapter: AdminPollAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySearchPollBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(this@SearchPollActivity)

        getAdminPollData()
        setOnclickListener()

        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()

        Glide.with(this@SearchPollActivity)
            .load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)
    }

    private fun setOnclickListener() {
        binding.apply {
            imgClose.setOnClickListener {
                finish()
            }
            edtSearchPoll.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    newText: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    if (adminPollModel.size > 0 && adminPollModel != null && !adminPollModel.isEmpty()) {
                        if (newText.trim { it <= ' ' }.isNotEmpty()) {
                            filter(newText.toString())
                        } else {
                            getAdminPollData()
                        }
                    }
                }
            })
        }
    }

    private fun filter(text: String) {
        if (text.isNotEmpty()) {
            val temp: MutableList<AdminPollModel> = ArrayList()
            for (d in adminPollModel) {
                if (d.pollTitle.lowercase(Locale.getDefault())
                        .contains(text.lowercase(Locale.getDefault()))
                ) {
                    temp.add(d)
                }
            }
            adminPollAdapter.updateList(temp as ArrayList<AdminPollModel>)
        }
    }

    private fun getAdminPollData() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(this@SearchPollActivity).sendPostData(
            Constants.UrlPath.GET_ALL_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    val pollPrints = responseObj.getJSONArray("pollPrints")
                    if (pollPrints.length() == 0) {
                        Toast.makeText(this@SearchPollActivity, "No Data Found", Toast.LENGTH_SHORT)
                            .show()
                    }

                    adminPollModel = Gson().fromJson<Any>(
                        pollPrints.toString(),
                        object : TypeToken<ArrayList<AdminPollModel>?>() {}.type
                    ) as ArrayList<AdminPollModel>

                    setAdminPollData()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setAdminPollData() {
        adminPollAdapter = AdminPollAdapter(
            this@SearchPollActivity,
            adminPollModel,
            ""
        )
        binding.rvSearchPoll.adapter = adminPollAdapter
    }
}