package com.poll.print.Registration

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.Login.LoginActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.country.CountryAdapter
import com.poll.print.country.CountryModel
import com.poll.print.databinding.ActivityRegistrationBinding
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import com.poll.print.state.StateAdapter
import com.poll.print.state.StateModel
import org.json.JSONObject
import java.util.Calendar
import java.util.GregorianCalendar

class RegistrationActivity : AppCompatActivity() {

    lateinit var binding: ActivityRegistrationBinding
    lateinit var mSessionManager: SessionManager

    private var countryModel: MutableList<CountryModel>? = mutableListOf()
    private var stateModel: MutableList<StateModel>? = mutableListOf()

    var countryId: String = ""
    var countryname: String = ""

    var fcmToken: String = ""

    var stateName: String = ""

    lateinit var selectedBirthDate: String
    lateinit var selectedBirthMonth: String
    lateinit var selectedBirthYear: String

    private var isUserEighteenPlus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        setOnClickListener()
        registerFCMToken()

        getCountryList()
        val sm = StateModel()
        sm.stateName = "Select State"
        stateModel!!.add(0, sm)
        setStateAdapter(stateModel!!)

    }

    private fun registerFCMToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener<String?> { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            // fetching the token
            fcmToken = task.result
            Log.e("aaa", "FCM Token :: $fcmToken")
        })
    }

    private fun setOnClickListener() {
        binding.apply {
            btnRegistration.setOnClickListener {
                if (checkValidation()) {
                    doRegister()
                }
            }
            tvLoginNow.setOnClickListener {
                val i = Intent(this@RegistrationActivity, LoginActivity::class.java)
                startActivity(i)
                finishAffinity()
            }
            edtBirthdate.setOnClickListener {
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(
                    this@RegistrationActivity, { view, year, monthOfYear, dayOfMonth ->
                        edtBirthdate.setText(
                            (dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                        )
                        selectedBirthDate = dayOfMonth.toString()
                        selectedBirthMonth = (monthOfYear + 1).toString()
                        selectedBirthYear = year.toString()
                        checkAge(selectedBirthYear, selectedBirthMonth, selectedBirthDate)
                    }, year, month, day
                )
                datePickerDialog.datePicker.maxDate = c.timeInMillis
                datePickerDialog.show()
            }
            binding.spCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {
                    countryId = (binding.spCountry.selectedItem as CountryModel)._id
                    countryname = (binding.spCountry.selectedItem as CountryModel).countryName
                    if (!countryId.equals("")) {
                        getStateListData(countryId)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
            binding.spState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {
                    stateName = (binding.spState.selectedItem as StateModel).stateName
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }
    }

    private fun getCountryList() {
        val jsonObject = JsonObject()
        ApiServiceProvider.getInstance(this@RegistrationActivity).sendPostData(Constants.UrlPath.GET_COUNTRY, jsonObject, true, RetrofitListener { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    val countries = responseObj.getJSONArray("countries")
                    countryModel = Gson().fromJson<Any>(
                        countries.toString(), object : TypeToken<MutableList<CountryModel>?>() {}.type
                    ) as MutableList<CountryModel>

                    val cm = CountryModel()
                    cm.countryName = "Select Country"
                    countryModel!!.add(0, cm)
                    setCountryAdapter(countryModel!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun getStateListData(storeCountryId: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("countryId", storeCountryId)
        ApiServiceProvider.getInstance(this@RegistrationActivity).sendPostData(Constants.UrlPath.GET_STATE, jsonObject, true, RetrofitListener { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    val states = responseObj.getJSONArray("states")
                    stateModel = Gson().fromJson<Any>(
                        states.toString(), object : TypeToken<MutableList<StateModel>?>() {}.type
                    ) as MutableList<StateModel>
                    setStateAdapter(stateModel!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun setCountryAdapter(countryModel: MutableList<CountryModel>) {
        binding.spCountry.adapter = this@RegistrationActivity.let { CountryAdapter(it, countryModel) }
        this@RegistrationActivity.let { CountryAdapter(it, countryModel) }
    }

    private fun setStateAdapter(stateModel: MutableList<StateModel>) {
        stateModel.sortBy { it.stateName }

        binding.spState.adapter = this@RegistrationActivity.let { StateAdapter(it, stateModel) }
    }

    private fun checkAge(year: String, month: String, day: String) {
        var date = day
        var mon = month
        var yr = year

        if (date.length < 2) date = "0$date"

        if (mon.length < 2) mon = "0$mon"

        val userAge: Calendar = GregorianCalendar(yr.toInt(), mon.toInt(), date.toInt())
        val minAdultAge: Calendar = GregorianCalendar()
        minAdultAge.add(Calendar.YEAR, -18)

        if (minAdultAge.before(userAge)) {
            isUserEighteenPlus = false
        } else {
            isUserEighteenPlus = true
        }
    }

    private fun doRegister() {
        val countryIdArray = JsonArray()
        countryIdArray.add(countryId)
        val requestData = JsonObject()
        requestData.addProperty("mobile", binding.edtMobile.text.toString())
        requestData.addProperty("birthDate", binding.edtBirthdate.text.toString())
        requestData.addProperty("country", countryname)
//        requestData.addProperty("countryId", countryId)
        requestData.add("countryId", countryIdArray)
        requestData.addProperty("state", stateName)
        requestData.addProperty("password", binding.edtPassword.text.toString())
        requestData.addProperty("device_token", fcmToken)

        ApiServiceProvider.getInstance(this).sendPostData(Constants.UrlPath.REGISTER, requestData, true, RetrofitListener { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")

                if (status) {
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    doLogin()
                } else {
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun doLogin() {
        val requestData = JsonObject()
        requestData.addProperty("mobile", binding.edtMobile.text.toString())
        requestData.addProperty("password", binding.edtPassword.text.toString())
        requestData.addProperty("device_token", fcmToken)

        ApiServiceProvider.getInstance(this).sendPostData(Constants.UrlPath.LOGIN, requestData, true, RetrofitListener { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                val token = responseObj.getString("token")
                val userData = responseObj.getJSONObject("userData")
                val userId = userData.getString("userId")
                val userName = userData.getString("userName")
                val userEmail = userData.getString("userEmail")
                val userProfile = userData.getString("userProfileImage")
                val sort_id = userData.getString("sort_id")
                val premiumUserStatus = userData.getString("premiumUserStatus")
                val deviceToken = userData.getString("device_token")
                val isPremiumUser = userData.getBoolean("isPremiumUser")

                if (status) {
                    mSessionManager.setData(SessionManager.USER_ID, userId)
                    mSessionManager.setData(SessionManager.USER_NAME, userName)
                    mSessionManager.setData(
                        SessionManager.USER_MOBILE, binding.edtMobile.text.toString()
                    )
                    mSessionManager.setData(SessionManager.USER_EMAIL, userEmail)
                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, isPremiumUser)
                    mSessionManager.setData(SessionManager.SORT_ID, sort_id)
                    mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                    mSessionManager.setData(SessionManager.USER_PROFILE_IMAGE, userProfile)
                    mSessionManager.setData(SessionManager.FCM_TOKEN, deviceToken)
                    mSessionManager.setUserLogin(true)

                    if (premiumUserStatus.equals("Submitted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS, Constants.PRO_USER_STATUS.SUBMITTED
                        )

                    } else if (premiumUserStatus.equals("Rejected")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS, Constants.PRO_USER_STATUS.REJECTED
                        )

                    } else if (premiumUserStatus.equals("Accepted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS, Constants.PRO_USER_STATUS.ACCEPTED
                        )

                    } else if (premiumUserStatus.equals("NotApplied")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS, Constants.PRO_USER_STATUS.NOTAPLLIED
                        )

                    }
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    val i = Intent(this@RegistrationActivity, DashBoardActivity::class.java)
                    startActivity(i)
                    finishAffinity()
                } else {
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private fun checkValidation(): Boolean {
        if (binding.edtMobile.length() < 10) {
            binding.edtMobile.requestFocus()
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_invalid_mobile), getString(R.string.txt_ok)
            )
            return false
        } else if (!binding.chckTermsAndCondition.isChecked) {
            binding.edtMobile.requestFocus()
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_select_terms_and_condition), getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtPassword.text.toString().trim() == "") {
            binding.edtPassword.requestFocus()
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_enter_password), getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtConfirmPassword.text.toString().trim() == "") {
            binding.edtConfirmPassword.requestFocus()
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_enter_confirm_password), getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtPassword.text.toString().trim() != binding.edtConfirmPassword.text.toString().trim()) {
            binding.edtConfirmPassword.requestFocus()
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_match_password), getString(R.string.txt_ok)
            )
            return false
        } else if (!isUserEighteenPlus) {
            AppUtils.showAlertDialog(
                this, getString(R.string.txt_alert), getString(R.string.alert_age_eighteen_plus_validation), getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }
}