package com.poll.print.ChangePassword

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.poll.print.ForgotPassword.ForgotPasswordActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityChangePasswordBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityChangePasswordBinding
    var isShowText = true

    lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(this@ChangePasswordActivity)

        setOnClickListener()
    }

    private fun setOnClickListener() {
        binding.apply {
            tvForgotPassword.setOnClickListener {
                val i = Intent(this@ChangePasswordActivity, ForgotPasswordActivity::class.java)
                startActivity(i)
            }
            btnSave.setOnClickListener {
                if (checkvalidation()) {
                    changeUserPassword()
                }
            }
            imgPasswordOn.setOnClickListener {
                isShowText
                edtNewPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
                binding.imgPasswordOn.visibility = View.GONE
                binding.imgPasswordOff.visibility = View.VISIBLE
            }
            imgPasswordOff.setOnClickListener(View.OnClickListener {
                isShowText = false
                edtNewPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
                binding.imgPasswordOff.visibility = View.GONE
                binding.imgPasswordOn.visibility = View.VISIBLE
            })
            imgConfirmNewPasswordOn.setOnClickListener {
                isShowText
                edtConfirmNewPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
                binding.edtConfirmNewPassword.setSelection(binding.edtConfirmNewPassword.length())
                binding.imgConfirmNewPasswordOn.visibility = View.GONE
                binding.imgConfirmNewPasswordOff.visibility = View.VISIBLE
            }
            imgConfirmNewPasswordOff.setOnClickListener(View.OnClickListener {
                isShowText = false
                edtConfirmNewPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                binding.edtConfirmNewPassword.setSelection(binding.edtConfirmNewPassword.length())
                binding.imgConfirmNewPasswordOff.visibility = View.GONE
                binding.imgConfirmNewPasswordOn.visibility = View.VISIBLE
            })
            imgBack.setOnClickListener {
                finish()
            }
        }
    }

    private fun checkvalidation(): Boolean {
        if (binding.edtNewPassword.text.toString().isEmpty()) {
            binding.edtNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_new_password),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtConfirmNewPassword.text.toString().isEmpty()) {
            binding.edtConfirmNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_confirm_password),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtNewPassword.text.toString() != binding.edtConfirmNewPassword.text.toString()) {
            binding.edtConfirmNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_match_password),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }

    private fun changeUserPassword() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        jsonObject.addProperty("password", binding.edtCurrentPassword.text.toString())
        jsonObject.addProperty("newPassword", binding.edtNewPassword.text.toString())

        ApiServiceProvider.getInstance(this@ChangePasswordActivity).sendPostData(
            Constants.UrlPath.CHANGE_PASSWORD, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(this@ChangePasswordActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}