package com.poll.print.Verification

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.auth
import com.poll.print.UpdatePassword.UpdatePasswordActivity
import com.poll.print.databinding.ActivityVerificationBinding
import com.poll.print.dialog.LoadingDialog


class VerificationActivity : AppCompatActivity() {

    lateinit var binding: ActivityVerificationBinding

    lateinit var auth: FirebaseAuth
    var storedVerificationId: String = ""

    private lateinit var mDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        mDialog = LoadingDialog(this)

        getIntentData()
        setOnClickListener()
    }

    private fun getIntentData() {
        storedVerificationId = intent.getStringExtra("storedVerificationId").toString()
    }

    private fun setOnClickListener() {
        binding.apply {
            btnVerify.setOnClickListener {
                val otp = binding.otpView.text.toString()
                if (otp.isNotEmpty()) {
                    val credential: PhoneAuthCredential =
                        PhoneAuthProvider.getCredential(storedVerificationId, otp)
                    signInWithPhoneAuthCredential(credential)
                } else {
                    Toast.makeText(this@VerificationActivity, "Enter OTP", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mDialog.show()
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    mDialog.dismiss()
                    val intent = Intent(this, UpdatePasswordActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    mDialog.dismiss()
                    // Sign in failed, display a message and update the UI
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Toast.makeText(this, "Invalid OTP", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }
}