package com.poll.print.myactivity

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.recyclerview.widget.RecyclerView
import com.poll.print.R
import com.poll.print.pollsubmit.PollSubmitActivity

class MyActivityLinechartAdapter(
    private val context: Context?,
    var pollSingleModel: ArrayList<PollOptions>,
    val pollId: String,
    val pollDisplayType: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            0 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_type_single, parent, false)
                return TypeSingleHolder(view)
            }

            1 -> {
                view =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_poll_type_single, parent, false)
                return TypeYesNoHolder(view)
            }

            2 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_type_multi, parent, false)
                return TypeMultyHolder(view)
            }

            3 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_type_preference, parent, false)
                return TypePreferenceHolder(view)
            }

            4 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_type_sharing, parent, false)
                return TypeSharingHolder(view)
            }

            else -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_type_single, parent, false)
                return TypeSingleHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = pollSingleModel[position]

        when (holder) {
            is TypeSingleHolder -> {
                holder.tv_single.text = model.optionTitle

                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            is TypeYesNoHolder -> {
                holder.tv_single.text = model.optionTitle

                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            is TypeMultyHolder -> {
                holder.chk_1.text = model.optionTitle

                holder.chk_1.setOnCheckedChangeListener { compoundButton, b ->
                    holder.chk_1.isChecked = !b
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
                holder.itemView.setOnClickListener {
                    holder.chk_1.isChecked = false
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            is TypePreferenceHolder -> {
                holder.tv_preference.text = model.optionTitle

                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            is TypeSharingHolder -> {
                holder.tv_name.text = model.optionTitle

                holder.seekbar.isEnabled = false

                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            is TypeRadioHolder -> {
                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }

            else -> {
                holder.itemView.setOnClickListener {
                    val intent = Intent(context, PollSubmitActivity::class.java)
                    intent.putExtra("PollId", pollId)
                    intent.putExtra("PollDisplayType", pollDisplayType)
                    context!!.startActivity(intent)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return pollSingleModel.size
    }

    companion object {
        const val TYPE_YES_NO = 0
        const val TYPE_SINGLE = 1
        const val TYPE_MULTY = 2
        const val TYPE_PREFERENCE = 3
        const val TYPE_PERCENTAGE = 4
    }

    override fun getItemViewType(position: Int): Int {
        when (pollDisplayType) {
            TYPE_YES_NO -> {
                return 0
            }

            TYPE_SINGLE -> {
                return 1
            }

            TYPE_MULTY -> {
                return 2
            }

            TYPE_PREFERENCE -> {
                return 3
            }

            TYPE_PERCENTAGE -> {
                return 4
            }
        }

        return 0
    }

    class TypeSingleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_single: TextView

        init {
            tv_single = itemView.findViewById(R.id.tv_single)
        }
    }

    class TypeYesNoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_single: TextView

        init {
            tv_single = itemView.findViewById(R.id.tv_single)
        }
    }

    class TypeMultyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var chk_1: CheckBox

        init {
            chk_1 = itemView.findViewById(R.id.chk_1)
        }
    }

    class TypePreferenceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_preference: TextView
        var img_arrow_down: ImageView
        var img_arrow_up: ImageView

        init {
            img_arrow_up = itemView.findViewById(R.id.img_arrow_up)
            img_arrow_down = itemView.findViewById(R.id.img_arrow_down)
            tv_preference = itemView.findViewById(R.id.tv_preference)
        }
    }

    class TypeSharingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var seekbar: AppCompatSeekBar
        var tv_name: TextView
        var tv_count: TextView

        init {
            seekbar = itemView.findViewById(R.id.seekbar)
            tv_name = itemView.findViewById(R.id.tv_name)
            tv_count = itemView.findViewById(R.id.tv_count)
        }
    }

    class TypeRadioHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rdo_yes_no: RadioButton
        var radioGroup_poll_type: RadioGroup

        init {
            rdo_yes_no = itemView.findViewById(R.id.rdo_yes_no)
            radioGroup_poll_type = itemView.findViewById(R.id.radioGroup_poll_type)
        }
    }
}