package com.poll.print.myactivity

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

data class MyActivityModel(
    @SerializedName("pollId")
    val pollId: String,
    @SerializedName("userName")
    val userName: String,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("totalAnswers")
    val totalAnswers: Int,
    @SerializedName("pollTypeDisplay")
    val pollTypeDisplay: Int,
    @SerializedName("pollTitle")
    val pollTitle: String,
    @SerializedName("pollSubTitle")
    val pollSubTitle: String,
    @SerializedName("pollEndDate")
    val pollEndDate: String,
    @SerializedName("timeStamp")
    val timeStamp: String,
    @SerializedName("pollType")
    val pollType: String,
    @SerializedName("pollOptionsCount")
    val pollOptionsCount: Int,
    @SerializedName("pollOptions")
    val pollOptions: ArrayList<PollOptions>
)

data class PollOptions(
    @SerializedName("optionId")
    val optionId: String,
    @SerializedName("optionTitle")
    val optionTitle: String,
    @SerializedName("answerCount")
    val answerCount: JsonElement
)