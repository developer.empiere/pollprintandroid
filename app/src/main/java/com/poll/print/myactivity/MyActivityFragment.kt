package com.poll.print.myactivity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.Utils.AppUtils.setVisible
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.FragmentMyActivityBinding
import com.poll.print.mypoll.MyPollModel
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class MyActivityFragment : Fragment() {

    private var _binding: FragmentMyActivityBinding? = null
    private val binding get() = _binding!!
    lateinit var mSessionManager: SessionManager
    var userId: String = ""
    private var myActivityModel = mutableListOf<MyActivityModel>()
    private lateinit var myActivityAdapter: MyActivityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyActivityBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("RtlHardcoded")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSessionManager = SessionManager(context)

        userId = mSessionManager.getData(SessionManager.USER_ID).toString()

        getActivityData()
        setOnClickListener()
    }

    private fun getActivityData() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", userId)
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_MY_ACTIVITY, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    val pollPrints = responseObj.getJSONArray("pollPrints")
                    if (pollPrints.length() == 0) {
                        binding.tvNoData.setVisible()
                    } else {
                        myActivityModel = Gson().fromJson<Any>(
                            pollPrints.toString(),
                            object : TypeToken<MutableList<MyActivityModel>?>() {}.type
                        ) as MutableList<MyActivityModel>

                        setActivityData()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setOnClickListener() {
        binding.apply {}
    }

    private fun setActivityData() {
        myActivityAdapter = MyActivityAdapter(context, myActivityModel)
        binding.rvMyActivity.adapter = myActivityAdapter
    }
}