package com.poll.print.myactivity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R
import com.poll.print.myactivitydetails.MyActivityDetailsActivity
import java.text.SimpleDateFormat
import java.util.Date

class MyActivityAdapter(
    private val context: Context?,
    var myActivityModel: List<MyActivityModel>?,
) : RecyclerView.Adapter<MyActivityAdapter.ViewHolder>() {

    private lateinit var myActivityLinechartAdapter: MyActivityLinechartAdapter

    private lateinit var sdf: SimpleDateFormat
    private lateinit var currentDateString: String
    private lateinit var currentDate: Date

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_my_activity, parent, false)

        sdf = SimpleDateFormat("dd/MM/yyyy")
        currentDateString = sdf.format(Date())
        currentDate = sdf.parse(currentDateString)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val mam = myActivityModel!![position]

        holder.tv_user_name.text = mam.userName
        holder.tv_user_question.text = mam.pollTitle
        holder.tv_user_question_description.text = mam.pollSubTitle
        holder.tv_time.text = mam.totalAnswers.toString() + " Pollprint"

        Glide.with(context!!).load(mam.imageURL)
            .placeholder(R.drawable.ic_no_profile)
            .into(holder.img_user)

        myActivityLinechartAdapter = MyActivityLinechartAdapter(
            context,
            mam.pollOptions,
            mam.pollId,
            mam.pollTypeDisplay
        )
        holder.rv_my_activity_line_chart.adapter = myActivityLinechartAdapter

        holder.tv_view_result.setOnClickListener {
            if (holder.tv_view_result.tag == "1") {
                holder.tv_view_result.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_expand, 0
                )
                holder.rv_my_activity_line_chart.visibility = View.VISIBLE
                holder.tv_view_result.tag = "0"
            } else {
                holder.tv_view_result.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_collapse, 0
                )
                holder.rv_my_activity_line_chart.visibility = View.GONE
                holder.tv_view_result.tag = "1"
            }
        }

        holder.itemView.setOnClickListener {

            var isPollExpired = false

            if (mam.pollEndDate != null) {

                val pollEndDateString = mam.pollEndDate
                val pollEndDate: Date = sdf.parse(pollEndDateString)

                if (pollEndDate.before(currentDate)) {
                    isPollExpired = true
                }

            }

            var i = Intent(context, MyActivityDetailsActivity::class.java)
            i.putExtra("pollId", mam.pollId)
            i.putExtra("PollType", mam.pollTypeDisplay)
            i.putExtra("PollDisplayType", mam.pollType)
            i.putExtra("isPollExpired", isPollExpired)
            context.startActivity(i)
        }

        holder.blurimage.setOnClickListener {

            var isPollExpired = false

            if (mam.pollEndDate != null) {

                val pollEndDateString = mam.pollEndDate
                val pollEndDate: Date = sdf.parse(pollEndDateString)

                if (pollEndDate.before(currentDate)) {
                    isPollExpired = true
                }

            }

            var i = Intent(context, MyActivityDetailsActivity::class.java)
            i.putExtra("pollId", mam.pollId)
            i.putExtra("PollType", mam.pollTypeDisplay)
            i.putExtra("PollDisplayType", mam.pollType)
            i.putExtra("isPollExpired", isPollExpired)
            context.startActivity(i)
        }

    }

    override fun getItemCount(): Int {
        return myActivityModel!!.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tv_view_result: TextView = itemView.findViewById(R.id.tv_view_result)
        val rv_my_activity_line_chart: RecyclerView =
            itemView.findViewById(R.id.rv_my_activity_line_chart)
        val blurimage: ConstraintLayout = itemView.findViewById(R.id.blurimage)
        val img_user: RoundedImageView = itemView.findViewById(R.id.img_user)
        val tv_user_name: TextView = itemView.findViewById(R.id.tv_user_name)
        val tv_time: TextView = itemView.findViewById(R.id.tv_time)
        val tv_user_question: TextView = itemView.findViewById(R.id.tv_user_question)
        val tv_user_question_description: TextView =
            itemView.findViewById(R.id.tv_user_question_description)
    }
}