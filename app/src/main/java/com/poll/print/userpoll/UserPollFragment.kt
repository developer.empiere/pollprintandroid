package com.poll.print.userpoll

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollModel
import com.poll.print.adminpoll.singleselectiontypepoll.AdminPollWithAdsAdapter
import com.poll.print.databinding.FragmentUserPollBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class UserPollFragment : Fragment() {

    private var _binding: FragmentUserPollBinding? = null
    private val binding get() = _binding!!
    lateinit var mSessionManager: SessionManager

    private var adminPollModel = ArrayList<AdminPollModel>()
    private var adminPollModelList = ArrayList<AdminPollModel>()
    //private lateinit var adminPollAdapter: AdminPollAdapter
    private lateinit var adminPollWithAdsAdapter: AdminPollWithAdsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserPollBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSessionManager = SessionManager(context)

        MobileAds.initialize(requireContext())

        binding.container.setOnRefreshListener {
            // on below line we are setting is refreshing to false.
            binding.container.isRefreshing = false

            getUserPollData()
        }
        getUserPollData()
    }

    fun getUserPollData() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_ALL_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {

                    adminPollModelList.clear()

                    val pollPrints = responseObj.getJSONArray("pollPrints")

                    adminPollModel = Gson().fromJson<Any>(
                        pollPrints.toString(),
                        object : TypeToken<ArrayList<AdminPollModel>?>() {}.type
                    ) as ArrayList<AdminPollModel>

                    for (i in 0 until adminPollModel.size) {
                        if (!adminPollModel[i].isAdminCreatedPoll) {
                            adminPollModelList.add(adminPollModel[i])
                        }
                    }

                    val adminPollListWithAds = mutableListOf<Any>().apply {
                        adminPollModelList.forEachIndexed { index, item ->
                            add(item)
                            if ((index + 1) % 3 == 0) {
                                add("AdViewHolder")
                            }
                        }
                    }

                    //if (adminPollModelList.size > 0)
                    if (adminPollListWithAds.size > 0) {
                        (requireActivity() as DashBoardActivity).getPollCategory()
                        setUserPollData(adminPollListWithAds)
                    }else {
                        binding.rvPollUser.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getUserPollCategoryData(categoryID: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        jsonObject.addProperty("categoryId", categoryID)
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_ALL_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {

                    adminPollModelList.clear()

                    val pollPrints = responseObj.getJSONArray("pollPrints")
                    if (pollPrints.length() == 0) {
                        Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show()
                    }
                    adminPollModel = Gson().fromJson<Any>(
                        pollPrints.toString(),
                        object : TypeToken<ArrayList<AdminPollModel>?>() {}.type
                    ) as ArrayList<AdminPollModel>

                    for (i in 0 until adminPollModel.size) {
                        if (!adminPollModel[i].isAdminCreatedPoll) {
                            adminPollModelList.add(adminPollModel[i])
                        }
                    }

                    val adminPollListWithAds = mutableListOf<Any>().apply {
                        adminPollModelList.forEachIndexed { index, item ->
                            add(item)
                            if ((index + 1) % 3 == 0) {
                                add("AdViewHolder")
                            }
                        }
                    }

                    //if (adminPollModelList.size > 0)
                    if (adminPollListWithAds.size > 0){
                        setUserPollData(adminPollListWithAds)
                    }else {
                        binding.rvPollUser.visibility = View.GONE
                        binding.tvNoData.visibility = View.VISIBLE
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /*private fun setUserPollData() {
        binding.rvPollUser.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollAdapter = AdminPollAdapter(context, adminPollModelList, "user")
        binding.rvPollUser.adapter = adminPollAdapter
    }*/

    private fun setUserPollData(adminPollListWithAds: MutableList<Any>) {
        binding.rvPollUser.visibility = View.VISIBLE
        binding.tvNoData.visibility = View.GONE
        adminPollWithAdsAdapter = AdminPollWithAdsAdapter(context, adminPollListWithAds, "user")
        binding.rvPollUser.adapter = adminPollWithAdsAdapter
    }
}