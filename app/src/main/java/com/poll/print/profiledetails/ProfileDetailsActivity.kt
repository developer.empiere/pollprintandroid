package com.poll.print.profiledetails

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityProfileDetailsBinding
import com.poll.print.editprofile.EditProfileActivity
import com.poll.print.myactivity.MyActivityAdapter
import com.poll.print.myactivity.MyActivityModel
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class ProfileDetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityProfileDetailsBinding

    private lateinit var profileDetailPageAdapter: ProfileDetailPageAdapter

    lateinit var mSessionManager: SessionManager
    var userProfile: String = ""
    var userName: String = ""
    var userPremiumStatus: Int = 0
    var isUserPremium: Boolean = false

    var userId: String = ""

    private var myActivityModel = mutableListOf<MyActivityModel>()
    private lateinit var myActivityAdapter: MyActivityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProfileDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()
        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()
        userPremiumStatus = mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS)
        isUserPremium = mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)
        userId = mSessionManager.getData(SessionManager.USER_ID).toString()

        setOnClickListener()

        if (!isUserPremium || (userPremiumStatus != Constants.PRO_USER_STATUS.ACCEPTED)) {
            setUserData()
        } else {
            setUpViewPager()
            setUpTabLayout()
        }

        binding.headerSingle.tvTitle.text = "Profile"

        if (userName != null && userName != "null")
            binding.tvName.text = userName
        else
            binding.tvName.text = ""

        Glide.with(this@ProfileDetailsActivity)
            .load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)
    }

    private fun setUserData() {

        binding.tvMyActUser.visibility = View.VISIBLE
        binding.rvMyAct.visibility = View.VISIBLE

        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", userId)
        ApiServiceProvider.getInstance(this@ProfileDetailsActivity).sendPostData(
            Constants.UrlPath.GET_MY_ACTIVITY, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    val pollPrints = responseObj.getJSONArray("pollPrints")
                    if (pollPrints.length() != 0) {
                        myActivityModel = Gson().fromJson<Any>(
                            pollPrints.toString(),
                            object : TypeToken<MutableList<MyActivityModel>?>() {}.type
                        ) as MutableList<MyActivityModel>

                        setActivityData()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun setActivityData() {
        myActivityAdapter = MyActivityAdapter(this@ProfileDetailsActivity, myActivityModel)
        binding.rvMyAct.adapter = myActivityAdapter
    }

    private fun setUpTabLayout() {
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                customizeTab(tab, 10, R.color.textColor_white, true)

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                customizeTab(tab, 10, R.color.textColor_white_70)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        TabLayoutMediator(
            binding.tabLayout, binding.viewPager
        ) { tab, position ->
            tab.text = when (position) {
                0 -> "My Polls"
                else -> "My Activity"
            }
            tab.customView = getCustomTabView(tab.text.toString(), 10, R.color.textColor_white)
        }.attach()

        binding.tabLayout.getTabAt(0)?.select()
    }

    private fun getCustomTabView(tabTitle: String, textSize: Int, textColor: Int): View {
        val tabCustomView: View = layoutInflater.inflate(R.layout.tab_customview, null)
        val tabTextView = tabCustomView.findViewById<TextView>(R.id.title_txt)

        tabTextView.text = tabTitle
        tabTextView.textSize = textSize.toFloat()
        tabTextView.setTextColor(ContextCompat.getColor(tabCustomView.context, textColor))
        return tabCustomView
    }

    private fun customizeTab(
        tab: TabLayout.Tab, tabSizeSp: Int, textColor: Int, isSelected: Boolean = false
    ) {
        val tabCustomView = tab.customView
        tabCustomView?.let {
            val tabTextView: TextView = it.findViewById(R.id.title_txt)
            tabTextView.textSize = tabSizeSp.toFloat()
            tabTextView.setTextColor(ContextCompat.getColor(it.context, textColor))
            if (isSelected) {
                tabTextView.setTextColor(Color.parseColor("#ECD5FF"))
                tabTextView.setBackgroundResource(R.drawable.bg_select_tab)
            } else {
                tabTextView.setTextColor(Color.parseColor("#70ECD5FF"))
                tabTextView.setBackgroundResource(R.drawable.bg_deselect_tab)
            }
        }
    }

    private fun setUpViewPager() {
        profileDetailPageAdapter = ProfileDetailPageAdapter(supportFragmentManager, lifecycle)
        binding.viewPager.adapter = profileDetailPageAdapter
    }

    private fun setOnClickListener() {
        binding.apply {
            tvEdit.setOnClickListener {
                val i = Intent(this@ProfileDetailsActivity, EditProfileActivity::class.java)
                startActivity(i)
            }
            headerSingle.imgBack.setOnClickListener {
                finish()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()
        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()

        if (userName != null && userName != "null")
            binding.tvName.text = userName
        else
            binding.tvName.text = ""

        Glide.with(this@ProfileDetailsActivity).load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)
    }

}