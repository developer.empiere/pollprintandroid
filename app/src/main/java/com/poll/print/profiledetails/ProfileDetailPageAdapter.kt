package com.poll.print.profiledetails

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.poll.print.myactivity.MyActivityFragment
import com.poll.print.mypoll.MyPollFragment

class ProfileDetailPageAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                MyPollFragment()
            }

            else -> {
                MyActivityFragment()
            }
        }
    }

    override fun getItemCount() = 2
}