package com.poll.print.premiumuser

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.poll.print.Utils.AppUtils
import com.poll.print.databinding.ActivityPremiumUserBinding

class PremiumUserActivity : AppCompatActivity() {

    lateinit var binding: ActivityPremiumUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPremiumUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        setOnclickListener()
        binding.headerSingle.tvTitle.text = "Premium User"
    }

    private fun setOnclickListener() {
        binding.apply {
            headerSingle.imgBack.setOnClickListener {
                finish()
            }
        }
    }
}