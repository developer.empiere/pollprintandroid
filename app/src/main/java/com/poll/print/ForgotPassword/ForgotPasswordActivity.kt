package com.poll.print.ForgotPassword

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.Firebase
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.auth
import com.google.gson.JsonObject
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.Verification.VerificationActivity
import com.poll.print.databinding.ActivityForgotPasswordBinding
import com.poll.print.dialog.AlertDialog
import com.poll.print.dialog.LoadingDialog
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityForgotPasswordBinding
    lateinit var mSessionManager: SessionManager

    var number: String = ""

    // create instance of firebase auth
    lateinit var auth: FirebaseAuth

    // we will use this to match the sent otp from firebase
    lateinit var storedVerificationId: String
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    private lateinit var mDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        auth = Firebase.auth

        mDialog = LoadingDialog(this)

        setOnClickListener()

        // Callback function for Phone Auth
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            // This method is called when the verification is completed
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                mDialog.dismiss()
                startActivity(Intent(applicationContext, VerificationActivity::class.java))
                finish()
            }

            // Called when verification is failed add log statement to see the exception
            override fun onVerificationFailed(e: FirebaseException) {
                mDialog.dismiss()
                Toast.makeText(
                    this@ForgotPasswordActivity,
                    "Error: ${e.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }

            // On code is sent by the firebase this method is called
            // in here we start a new activity where user can enter the OTP
            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                mDialog.dismiss()
                storedVerificationId = verificationId
                resendToken = token

                // Start a new activity using intent
                // also send the storedVerificationId using intent
                // we will use this id to send the otp back to firebase
                val intent = Intent(applicationContext, VerificationActivity::class.java)
                intent.putExtra("storedVerificationId", storedVerificationId)
                intent.putExtra("email", binding.edtPhone.text.toString())
                startActivity(intent)
            }
        }

    }

    private fun setOnClickListener() {
        binding.apply {
            btnSend.setOnClickListener {
                if (checkMobileValidation())
                    checkNumberExist()
            }
        }

    }

    private fun checkNumberExist() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("mobile", binding.edtPhone.text.toString())

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.NUMBER_EXISTS, jsonObject, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        if (status) {
                            mSessionManager.setData(
                                SessionManager.PHONE_NUMBER,
                                binding.edtPhone.text.toString()
                            )
                            firebaseLogin()
                        } else {
                            val alertDialog = AlertDialog(this@ForgotPasswordActivity)
                            alertDialog.setCancelable(false)
                            alertDialog.title = "Error"
                            alertDialog.message = message
                            alertDialog.setPositiveButton(
                                "Ok",
                                View.OnClickListener {
                                    alertDialog.dismiss()
                                })
                            alertDialog.show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun firebaseLogin() {
        number = binding.edtPhone.text.trim().toString()
        // get the phone number from edit text and append the country cde with it
        if (number.isNotEmpty()) {
            number = "+91$number"
            sendVerificationCode(number)
        } else {
            Toast.makeText(this, "Enter email address", Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendVerificationCode(number: String) {
        mDialog.show()
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(number) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this) // Activity (for callback binding)
            .setCallbacks(callbacks) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun checkMobileValidation(): Boolean {
        if (binding.edtPhone.text.toString().isEmpty()) {
            binding.edtPhone.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_mobile),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtPhone.text.toString().trim().length != 10) {
            binding.edtPhone.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_invalid_mobile),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }
}