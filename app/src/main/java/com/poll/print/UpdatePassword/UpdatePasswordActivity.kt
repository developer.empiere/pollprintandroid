package com.poll.print.UpdatePassword

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.poll.print.Login.LoginActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityUpdatePasswordBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class UpdatePasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityUpdatePasswordBinding
    lateinit var mSessionManager: SessionManager

    var isShowText = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityUpdatePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(this@UpdatePasswordActivity)

        setOnClickListener()
    }

    private fun setOnClickListener() {
        binding.apply {
            btnSave.setOnClickListener {
                if (checkValidation()) {
                    updatePassword()
                }
            }
            imgPasswordOn.setOnClickListener {
                isShowText
                edtNewPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
                binding.imgPasswordOn.visibility = View.GONE
                binding.imgPasswordOff.visibility = View.VISIBLE
            }
            imgPasswordOff.setOnClickListener(View.OnClickListener {
                isShowText = false
                edtNewPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
                binding.imgPasswordOff.visibility = View.GONE
                binding.imgPasswordOn.visibility = View.VISIBLE
            })

            imgConfirmNewPasswordOn.setOnClickListener {
                isShowText
                edtConfirmNewPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
                binding.edtConfirmNewPassword.setSelection(binding.edtConfirmNewPassword.length())
                binding.imgConfirmNewPasswordOn.visibility = View.GONE
                binding.imgConfirmNewPasswordOff.visibility = View.VISIBLE
            }
            imgConfirmNewPasswordOff.setOnClickListener(View.OnClickListener {
                isShowText = false
                edtConfirmNewPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                binding.edtConfirmNewPassword.setSelection(binding.edtConfirmNewPassword.length())
                binding.imgConfirmNewPasswordOff.visibility = View.GONE
                binding.imgConfirmNewPasswordOn.visibility = View.VISIBLE
            })
        }
    }

    private fun checkValidation(): Boolean {
        if (binding.edtNewPassword.text.toString().isEmpty()) {
            binding.edtNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_new_password),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtConfirmNewPassword.text.toString().isEmpty()) {
            binding.edtConfirmNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_confirm_password),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtNewPassword.text.toString() != binding.edtConfirmNewPassword.text.toString()) {
            binding.edtConfirmNewPassword.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_match_password),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }

    private fun updatePassword() {
        val jsonObject = JsonObject()

        var email = ""

        if (mSessionManager.getData(SessionManager.USER_EMAIL).toString() != "null" &&
            mSessionManager.getData(SessionManager.USER_EMAIL) != null &&
            mSessionManager.getData(SessionManager.USER_EMAIL) != ""
        )
            email = mSessionManager.getData(SessionManager.USER_EMAIL).toString()

        jsonObject.addProperty("email", email)
        jsonObject.addProperty(
            "mobile", mSessionManager.getData(SessionManager.PHONE_NUMBER).toString()
        )
        jsonObject.addProperty("password", binding.edtNewPassword.text.toString())

        ApiServiceProvider.getInstance(this@UpdatePasswordActivity).sendPostData(
            Constants.UrlPath.UPDATE_PASSWORD, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(this@UpdatePasswordActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()
                    val i = Intent(this@UpdatePasswordActivity, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}