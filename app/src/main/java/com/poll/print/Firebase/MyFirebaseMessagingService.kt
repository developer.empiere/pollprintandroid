package com.poll.print.Firebase

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.R
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import java.util.concurrent.atomic.AtomicInteger

class MyFirebaseMessagingService : FirebaseMessagingService() {

    lateinit var sessionManager: SessionManager

    lateinit var notificationTitle: String
    lateinit var notificationMessage: String
    lateinit var notificationImage: String
    lateinit var proUserApprovalStatus: String

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "From: ${remoteMessage.from}")

        sessionManager = SessionManager(this)

        notificationTitle = remoteMessage.notification?.title.toString()
        notificationMessage = remoteMessage.notification?.body.toString()
        notificationImage = remoteMessage.notification?.imageUrl.toString()
        proUserApprovalStatus = remoteMessage.data["status"].toString()

        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data : ${remoteMessage.data}")
        }

        if (proUserApprovalStatus.equals("approve", true)) {
            sessionManager.setData(SessionManager.IS_USER_PREMIUM, true)
            sessionManager.setData(
                SessionManager.USER_PREMIUM_STATUS,
                Constants.PRO_USER_STATUS.ACCEPTED
            )
        } else {
            sessionManager.setData(SessionManager.IS_USER_PREMIUM, false)
            sessionManager.setData(
                SessionManager.USER_PREMIUM_STATUS,
                Constants.PRO_USER_STATUS.REJECTED
            )
        }

        sendNotification(this, remoteMessage)

    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        println("Push Notification Token :: $token")
    }

    private fun sendNotification(context: Context, remoteMessage: RemoteMessage) {

        var channelId = ""

        if (remoteMessage.notification!!.channelId != null)
            channelId = remoteMessage.notification!!.channelId.toString()
        else
            channelId = "Default"

        val intent = Intent(applicationContext, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val extras = Bundle()
        extras.putString("notify_title", notificationTitle)
        extras.putString("notify_message", notificationMessage)
        extras.putString("notify_image", notificationImage)
        intent.putExtras(extras)
        intent.action = Intent.ACTION_VIEW

        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(
                applicationContext,
                NotificationID.iD,
                intent,
                PendingIntent.FLAG_IMMUTABLE
            )

        val notificationBuilder =
            NotificationCompat.Builder(context.getApplicationContext(), channelId!!)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(longArrayOf(500, 500, 500))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent)
                .setContentTitle(remoteMessage.notification?.title)
                .setContentText(remoteMessage.notification?.body)

        val notificationManager = NotificationManagerCompat.from(context)

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Default",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        if (notificationImage != "null" && notificationImage != "") {
            Glide.with(applicationContext)
                .asBitmap()
                .load(remoteMessage.notification?.imageUrl)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        //largeIcon
                        notificationBuilder.setLargeIcon(resource)
                        //Big Picture
                        notificationBuilder.setStyle(
                            NotificationCompat.BigPictureStyle().bigPicture(resource)
                        )
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {}
                })
        }

        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        notificationManager.notify(
            NotificationID.iD,
            notificationBuilder.build()
        )

    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }

    internal object NotificationID {
        private val c = AtomicInteger(100)
        val iD: Int
            get() = c.incrementAndGet()
    }

}