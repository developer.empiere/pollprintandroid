package com.poll.print.pollsubmit

data class PollSubmitModel(
    var optionId: String? = null,
    var optionTitle: String = "",
    var isSelected: Boolean = false,
    var seekBarProgress: Int = 0,
    var optionImages: ArrayList<String>
)