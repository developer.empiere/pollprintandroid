package com.poll.print.pollsubmit

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R
import com.poll.print.Utils.AppUtils.setVisible


class PollSubmitAdapter(
    private val context: Context?,
    var pollSubmitModel: ArrayList<PollSubmitModel>?,
    val pollId: String,
    val pollDisplayType: Int,
    val sharingValue: String,
    private val progressCounter: ProgressCounterUpdater,
    private val pollOptionImageClickListener: PollOptionImageClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var yesNoSelectedItemPosition: Int = -1
    private var lastSelectedPosition = -1

    private var totalProgress = 0
    private var maxProgress = 0
    private val seekBarProgresses = mutableMapOf<Int, Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            0 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_yes_no, parent, false)
                return TypeYesNoHolder(view)
            }

            1 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_radio, parent, false)
                return TypeSingleHolder(view)
            }

            2 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_multi, parent, false)
                return TypeMultyHolder(view)
            }

            3 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_preference, parent, false)
                return TypePreferenceHolder(view)
            }

            4 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_sharing, parent, false)
                return TypeSharingHolder(view)
            }

            else -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_poll_submit_yes_no, parent, false)
                return TypeYesNoHolder(view)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = pollSubmitModel!![position]

        when (holder) {
            is TypeYesNoHolder -> {
                holder.tv_single.text = model.optionTitle

                holder.tv_single.setOnClickListener {
                    yesNoSelectedItemPosition = position
                    notifyDataSetChanged()
                    onAnyItemClickListener.let {
                        it?.invoke(model.optionId.toString())
                    }
                }

                holder.img_first.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[0])
                }

                holder.img_second.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[1])
                }

                holder.img_third.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[2])
                }

                if (yesNoSelectedItemPosition == position) {
                    holder.tv_single.setTextColor(context!!.resources.getColor(R.color.dark_primary))
                    holder.tv_single.setBackgroundResource(R.drawable.ic_submit_answer_background)
                } else {
                    holder.tv_single.setTextColor(context!!.resources.getColor(R.color.white))
                    holder.tv_single.setBackgroundResource(R.drawable.ic_submit_unanswer_background)
                }

                when (model.optionImages.size) {
                    1 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                    }

                    2 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                    }

                    3 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                        if (model.optionImages[2] != "") {
                            holder.img_third.setVisible()
                            Glide.with(context)
                                .load(model.optionImages[2])
                                .into(holder.img_third)
                        }
                    }
                }
            }

            is TypeSingleHolder -> {

                holder.rdo_group.clearCheck()

                holder.rdo_title.text = model.optionTitle
                holder.rdo_title.isChecked = lastSelectedPosition == position

                holder.rdo_title.setOnClickListener {
                    val previousSelectedPosition = lastSelectedPosition
                    lastSelectedPosition = position

                    notifyItemChanged(previousSelectedPosition)
                    notifyItemChanged(lastSelectedPosition)

                    onAnyItemClickListener?.invoke(model.optionId.toString())
                }

                holder.img_first.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[0])
                }

                holder.img_second.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[1])
                }

                holder.img_third.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[2])
                }

                when (model.optionImages.size) {
                    1 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                    }

                    2 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                    }

                    3 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                        if (model.optionImages[2] != "") {
                            holder.img_third.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[2])
                                .into(holder.img_third)
                        }
                    }
                }
            }

            is TypeMultyHolder -> {

                holder.chk_1.text = model.optionTitle

                holder.chk_1.isChecked = model.isSelected

                holder.chk_1.setOnClickListener { v ->
                    val isChecked = (v as CheckBox).isChecked
                    model.isSelected = isChecked
                    notifyDataSetChanged()
                    onMultiItemClickListener.let {
                        it?.invoke(isChecked, model.optionId.toString())
                    }
                }

                holder.img_first.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[0])
                }

                holder.img_second.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[1])
                }

                holder.img_third.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[2])
                }

                when (model.optionImages.size) {
                    1 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                    }

                    2 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                    }

                    3 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                        if (model.optionImages[2] != "") {
                            holder.img_third.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[2])
                                .into(holder.img_third)
                        }
                    }
                }

            }

            is TypePreferenceHolder -> {
                holder.tv_preference.text = model.optionTitle

                holder.img_first.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[0])
                }

                holder.img_second.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[1])
                }

                holder.img_third.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[2])
                }

                when (model.optionImages.size) {
                    1 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                    }

                    2 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                    }

                    3 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                        if (model.optionImages[2] != "") {
                            holder.img_third.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[2])
                                .into(holder.img_third)
                        }
                    }
                }
            }

            is TypeSharingHolder -> {
                holder.tv_name.text = model.optionTitle
                holder.seekbar.max = sharingValue.toInt()

                maxProgress = sharingValue.toInt()

                holder.seekbar.setOnSeekBarChangeListener(object :
                    SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(
                        seek: SeekBar,
                        progress: Int,
                        fromUser: Boolean
                    ) {

                        totalProgress = seekBarProgresses.values.sum()

                        if (totalProgress <= maxProgress) {
                            seekBarProgresses[position] = progress
                            holder.tv_count.text = progress.toString()
                            progressCounter.updateText(totalProgress.toString())
                        } else {
                            seekBarProgresses[position] = progress
                        }

                    }

                    override fun onStartTrackingTouch(seek: SeekBar) {
                        if (totalProgress > maxProgress)
                            return
                    }

                    override fun onStopTrackingTouch(seek: SeekBar) {
                        totalProgress = seekBarProgresses.values.sum()

                        if (totalProgress <= maxProgress) {
                            model.seekBarProgress = seek.progress
                            holder.tv_count.text = seek.progress.toString()
                            progressCounter.updateText(totalProgress.toString())
                        } else {
                            val maxFinalCalculation = seek.progress - (totalProgress - maxProgress)
                            holder.seekbar.progress = maxFinalCalculation
                            if (!maxFinalCalculation.toString().contains("-")) {
                                model.seekBarProgress = maxFinalCalculation
                                holder.tv_count.text = maxFinalCalculation.toString()
                            } else {
                                holder.tv_count.text = "0"
                            }
                            progressCounter.updateText(sharingValue)
                        }
                    }
                })

                holder.img_first.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[0])
                }

                holder.img_second.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[1])
                }

                holder.img_third.setOnClickListener {
                    pollOptionImageClickListener.enlargeImage(model.optionImages[2])
                }

                when (model.optionImages.size) {
                    1 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                    }

                    2 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                    }

                    3 -> {
                        if (model.optionImages[0] != "") {
                            holder.img_first.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[0])
                                .into(holder.img_first)
                        }
                        if (model.optionImages[1] != "") {
                            holder.img_second.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[1])
                                .into(holder.img_second)
                        }
                        if (model.optionImages[2] != "") {
                            holder.img_third.setVisible()
                            Glide.with(context!!)
                                .load(model.optionImages[2])
                                .into(holder.img_third)
                        }
                    }
                }

            }

            else -> {
                return
            }
        }
    }

    override fun getItemCount(): Int {
        return pollSubmitModel!!.size
    }

    companion object {
        const val TYPE_YES_NO = 0
        const val TYPE_SINGLE = 1
        const val TYPE_MULTY = 2
        const val TYPE_PREFERENCE = 3
        const val TYPE_PERCENTAGE = 4
    }

    override fun getItemViewType(position: Int): Int {
        when (pollDisplayType) {
            TYPE_YES_NO -> {
                return 0
            }

            TYPE_SINGLE -> {
                return 1
            }

            TYPE_MULTY -> {
                return 2
            }

            TYPE_PREFERENCE -> {
                return 3
            }

            TYPE_PERCENTAGE -> {
                return 4
            }
        }

        return 0
    }

    class TypeYesNoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_single: TextView
        var img_first: RoundedImageView
        var img_second: RoundedImageView
        var img_third: RoundedImageView

        init {
            tv_single = itemView.findViewById(R.id.tv_single)
            img_first = itemView.findViewById(R.id.img_first)
            img_second = itemView.findViewById(R.id.img_second)
            img_third = itemView.findViewById(R.id.img_third)
        }
    }

    class TypeSingleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rdo_title: RadioButton
        var rdo_group: RadioGroup
        var img_first: RoundedImageView
        var img_second: RoundedImageView
        var img_third: RoundedImageView

        init {
            rdo_title = itemView.findViewById(R.id.rdo_title)
            rdo_group = itemView.findViewById(R.id.rdo_group)
            img_first = itemView.findViewById(R.id.img_first)
            img_second = itemView.findViewById(R.id.img_second)
            img_third = itemView.findViewById(R.id.img_third)
        }
    }

    class TypeMultyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var chk_1: CheckBox
        var img_first: RoundedImageView
        var img_second: RoundedImageView
        var img_third: RoundedImageView

        init {
            chk_1 = itemView.findViewById(R.id.chk_1)
            img_first = itemView.findViewById(R.id.img_first)
            img_second = itemView.findViewById(R.id.img_second)
            img_third = itemView.findViewById(R.id.img_third)
        }
    }

    class TypePreferenceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_preference: TextView
        var img_arrow_down: ImageView
        var img_arrow_up: ImageView
        var img_first: RoundedImageView
        var img_second: RoundedImageView
        var img_third: RoundedImageView

        init {
            img_arrow_up = itemView.findViewById(R.id.img_arrow_up)
            img_arrow_down = itemView.findViewById(R.id.img_arrow_down)
            tv_preference = itemView.findViewById(R.id.tv_preference)
            img_first = itemView.findViewById(R.id.img_first)
            img_second = itemView.findViewById(R.id.img_second)
            img_third = itemView.findViewById(R.id.img_third)
        }
    }

    class TypeSharingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var seekbar: AppCompatSeekBar
        var tv_name: TextView
        var tv_count: TextView
        var img_first: RoundedImageView
        var img_second: RoundedImageView
        var img_third: RoundedImageView

        init {
            seekbar = itemView.findViewById(R.id.seekbar)
            tv_name = itemView.findViewById(R.id.tv_name)
            tv_count = itemView.findViewById(R.id.tv_count)
            img_first = itemView.findViewById(R.id.img_first)
            img_second = itemView.findViewById(R.id.img_second)
            img_third = itemView.findViewById(R.id.img_third)
        }
    }

    private var onAnyItemClickListener: ((String) -> Unit)? = null
    fun setOnAnyItemClickListener(listener: (String) -> Unit) {
        onAnyItemClickListener = listener
    }

    private var onMultiItemClickListener: ((Boolean, String) -> Unit)? = null
    fun setOnMultiItemClickListener(listener: (Boolean, String) -> Unit) {
        onMultiItemClickListener = listener
    }

    interface ProgressCounterUpdater {
        fun updateText(newText: String)
    }

    interface PollOptionImageClickListener {
        fun enlargeImage(imageUrl: String)
    }

}