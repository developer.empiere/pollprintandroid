package com.poll.print.pollsubmit

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.AppUtils.setGone
import com.poll.print.Utils.AppUtils.setVisible
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityPollSubmitBinding
import com.poll.print.pollsubmiteddetail.SubmitedPollDetailActivity
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject
import java.util.Collections

data class PercentageData(
    val optionId: String,
    val percentage: Int
)

class PollSubmitActivity : AppCompatActivity(), PollSubmitAdapter.ProgressCounterUpdater,
    PollSubmitAdapter.PollOptionImageClickListener {

    lateinit var binding: ActivityPollSubmitBinding

    lateinit var mSessionManager: SessionManager
    var userId: String = ""
    var userProfile: String = ""

    var isPollExpired: Boolean = false
    var isPollAnswered: Boolean = false
    var allowVotersToChangeAnswer: Boolean = false

    var pollDisplayType: Int = 0
    var pollId: String = ""
    var pollOptionId: String = ""
    var pollType: String = ""
    var sharingValue: String = ""
    var likelyRating: String = ""
    var userName: String = ""
    var likely: Boolean = false
    var multiArrayList: ArrayList<String> = arrayListOf()
    val multiOptionsArray = JsonArray()
    val preferanceOptionsArray = JsonArray()
    var size = 0

    private var pollSubmitModel = ArrayList<PollSubmitModel>()
    private lateinit var pollSubmitAdapter: PollSubmitAdapter

    var currentProgress = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPollSubmitBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this@PollSubmitActivity)
        userId = mSessionManager.getData(SessionManager.USER_ID).toString()
        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()

        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()


        if (userName != null && userName != "null")
            binding.tvUname.text = userName

        Glide.with(this@PollSubmitActivity).load(userProfile).placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        likelyRating = binding.tvLikely.text.toString()

        getIntentData()
        setOnclickListener()
        getPollDetails()
    }

    private fun getPollDetails() {
        val jsonObject = JsonObject()

        jsonObject.addProperty("pollId", pollId)
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())

        ApiServiceProvider.getInstance(this@PollSubmitActivity).sendPostData(
            Constants.UrlPath.GET_POLL_DETAILS, jsonObject, false
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                if (status) {
                    val pollData = responseObj.getJSONObject("pollData")

                    val userName = pollData.getString("userName")
                    val imageURL = pollData.getString("imageURL")
                    val pollTitle = pollData.getString("pollTitle")
                    val pollSubTitle = pollData.getString("pollSubTitle")
                    if (pollData.has("pollValue")) {
                        sharingValue = pollData.getString("pollValue")
                    }
                    pollType = pollData.getString("pollType")
                    val totalAnswers = pollData.getInt("totalAnswers")

                    val pollImages = pollData.getJSONArray("pollImages")
                    val pollOptions = pollData.getJSONArray("pollOptions")

                    size = pollImages.length()

                    if (size == 1) {
                        binding.imgDesc1.setVisible()
                        binding.imgDesc2.setGone()
                        Glide.with(this@PollSubmitActivity).load(pollImages.get(0))
                            .into(binding.imgDesc1)
                    } else if (size == 2) {
                        binding.imgDesc1.setVisible()
                        binding.imgDesc2.setVisible()
                        Glide.with(this@PollSubmitActivity).load(pollImages.get(0))
                            .into(binding.imgDesc1)
                        Glide.with(this@PollSubmitActivity).load(pollImages.get(1))
                            .into(binding.imgDesc2)
                    }

                    if(userName != "null")
                        binding.tvUserName.text = userName
                    else
                        binding.tvUserName.text = ""

                    binding.tvUserQuestion.text = pollTitle
                    binding.tvTime.text = "$totalAnswers Pollprint"
                    binding.tvUserQuestionDescription.text = pollSubTitle

                    Glide.with(this@PollSubmitActivity).load(imageURL)
                        .placeholder(R.drawable.ic_no_profile).into(binding.imgUser)

                    if (pollType == "Percentage sharing on option") {
                        binding.tvMaximumCount.visibility = View.VISIBLE
                        binding.tvMaximumCount.text = "$currentProgress/$sharingValue"
                    }

                    pollSubmitModel = Gson().fromJson<Any>(
                        pollOptions.toString(),
                        object : TypeToken<ArrayList<PollSubmitModel>?>() {}.type
                    ) as ArrayList<PollSubmitModel>

                    setPollOptionData()

                } else {
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finish()
            }
        }
    }

    private fun setPollOptionData() {
        pollSubmitAdapter = PollSubmitAdapter(
            this@PollSubmitActivity,
            pollSubmitModel,
            pollId,
            pollDisplayType,
            sharingValue,
            this,
            this
        )
        binding.rvPollSubmit.adapter = pollSubmitAdapter

        if (pollType == "Preference choice") {
            val itemTouchHelper = ItemTouchHelper(simpleCallback)
            itemTouchHelper.attachToRecyclerView(binding.rvPollSubmit)
        }

        pollSubmitAdapter.setOnAnyItemClickListener { optId ->
            pollOptionId = optId
        }

        pollSubmitAdapter.setOnMultiItemClickListener { isCheck, optId ->
            if (isCheck) {
                multiArrayList.add(optId)
            } else {
                for (i in 0 until multiArrayList.size) {
                    if (multiArrayList[i] == optId) {
                        multiArrayList.removeAt(i)
                        break
                    }
                }
            }
        }
    }

    var simpleCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(
        ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END,
        0
    ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            val fromPosition = viewHolder.adapterPosition
            val toPosition = target.adapterPosition
            Collections.swap(pollSubmitModel, fromPosition, toPosition)
            recyclerView.adapter!!.notifyItemMoved(fromPosition, toPosition)
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {}
    }

    private fun getIntentData() {
        pollId = intent.getStringExtra("PollId").toString()
        pollDisplayType = intent.getIntExtra("PollDisplayType", 0)

        isPollAnswered = intent.getBooleanExtra("isPollAnswered", false)
        isPollExpired = intent.getBooleanExtra("isPollExpired", false)
        allowVotersToChangeAnswer = intent.getBooleanExtra("allowVotersToChangeAnswer", false)
    }

    private fun setOnclickListener() {
        binding.apply {
            imgClose.setOnClickListener {
                finish()
            }
            btnSubmit.setOnClickListener {

                if (!isPollExpired) {
                    if (!isPollAnswered) {
                        if (pollType == "Yes/No" || pollType == "Single selection from multiple choice") {
                            if (pollOptionId == "") {
                                Toast.makeText(
                                    this@PollSubmitActivity,
                                    "Please choose at least one answer",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                submitYourPoll()
                            }
                        } else if (pollType == "Multiple choice") {
                            if (multiArrayList.isEmpty()) {
                                Toast.makeText(
                                    this@PollSubmitActivity,
                                    "Please choose at least one answer",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                for (i in 0 until multiArrayList.size) {
                                    multiOptionsArray.add(multiArrayList[i])
                                }
                                submitPollForMultipleChoice(multiOptionsArray)
                            }
                        } else if (pollType == "Preference choice") {
                            for (i in 0 until pollSubmitModel.size) {
                                preferanceOptionsArray.add(pollSubmitModel[i].optionId)
                            }
                            submitPollForMultipleChoice(preferanceOptionsArray)
                        } else if (pollType == "Percentage sharing on option") {
                            submitPollPercentageSharing()
                        }
                    } else {

                        if (allowVotersToChangeAnswer) {
                            if (pollType == "Yes/No" || pollType == "Single selection from multiple choice") {
                                if (pollOptionId == "") {
                                    Toast.makeText(
                                        this@PollSubmitActivity,
                                        "Please choose at least one answer",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    submitYourPoll()
                                }
                            } else if (pollType == "Multiple choice") {
                                if (multiArrayList.isEmpty()) {
                                    Toast.makeText(
                                        this@PollSubmitActivity,
                                        "Please choose at least one answer",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    for (i in 0 until multiArrayList.size) {
                                        multiOptionsArray.add(multiArrayList[i])
                                    }
                                    submitPollForMultipleChoice(multiOptionsArray)
                                }
                            } else if (pollType == "Preference choice") {
                                for (i in 0 until pollSubmitModel.size) {
                                    preferanceOptionsArray.add(pollSubmitModel[i].optionId)
                                }
                                submitPollForMultipleChoice(preferanceOptionsArray)
                            } else if (pollType == "Percentage sharing on option") {
                                submitPollPercentageSharing()
                            }
                        } else {
                            Toast.makeText(
                                this@PollSubmitActivity,
                                "You already answered for this poll",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Toast.makeText(
                        this@PollSubmitActivity,
                        "Poll is Expired",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            clOne.setOnClickListener {
                imgBg1.setVisible()
                likely = true
                likelyRating = binding.tvLikely.text.toString()
                //likelyRating = "Likely"
                imgBg2.setGone()
                imgBg3.setGone()
            }

            clTwo.setOnClickListener {
                imgBg1.setGone()
                imgBg2.setVisible()
                likely = true
                likelyRating = binding.tvSomewhatLikely.text.toString()
                imgBg3.setGone()
            }
            clThree.setOnClickListener {
                imgBg1.setGone()
                imgBg2.setGone()
                imgBg3.setVisible()
                likely = true
                likelyRating = binding.tvUnlikely.text.toString()
            }

            tvUserQuestion.setOnClickListener {
                if (binding.tvUserQuestion.tag == "1") {
                    binding.tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_expand, 0
                    )
                    binding.tvUserQuestionDescription.visibility = View.VISIBLE
                    if (size == 1) {
                        binding.imgDesc1.visibility = View.VISIBLE
                    } else if (size == 2) {
                        binding.imgDesc1.visibility = View.VISIBLE
                        binding.imgDesc2.visibility = View.VISIBLE
                    }
                    binding.tvUserQuestion.tag = "0"
                } else {
                    binding.tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_collapse, 0
                    )
                    binding.tvUserQuestionDescription.visibility = View.GONE
                    binding.imgDesc1.visibility = View.GONE
                    binding.imgDesc2.visibility = View.GONE
                    binding.tvUserQuestion.tag = "1"
                }
            }

            imgDesc1.setOnClickListener {
                rlEnlargeImage.setVisible()
                Glide.with(applicationContext).load(imgDesc1.drawable).into(ivEnlargeImage)
            }

            imgDesc2.setOnClickListener {
                rlEnlargeImage.setVisible()
                Glide.with(applicationContext).load(imgDesc2.drawable).into(ivEnlargeImage)
            }

            ivCloseImage.setOnClickListener {
                rlEnlargeImage.setGone()
            }

        }
    }

    private fun submitYourPoll() {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.addProperty("optionId", pollOptionId)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@PollSubmitActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@PollSubmitActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(this@PollSubmitActivity, SubmitedPollDetailActivity::class.java)
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollType)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitPollForMultipleChoice(multiOptionsArrayParamter: JsonArray) {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.add("optionIds", multiOptionsArrayParamter)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@PollSubmitActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@PollSubmitActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(this@PollSubmitActivity, SubmitedPollDetailActivity::class.java)
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollType)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun submitPollPercentageSharing() {

        var overallPercentageCount = 0

        var percentageDataArrayList = ArrayList<PercentageData>()

        for (i in 0 until pollSubmitModel.size) {

            var percentageCalculation =
                (pollSubmitModel[i].seekBarProgress * 100) / sharingValue.toInt()

            percentageDataArrayList.add(
                PercentageData(
                    pollSubmitModel[i].optionId.toString(),
                    percentageCalculation
                )
            )

            overallPercentageCount += percentageCalculation
        }

        if (overallPercentageCount == 0) {
            Toast.makeText(this@PollSubmitActivity, "Please give voting", Toast.LENGTH_SHORT).show()
            return
        }

        val jsonElement: JsonElement = Gson().toJsonTree(percentageDataArrayList)

        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty(
            "sort_id",
            mSessionManager.getData(SessionManager.SORT_ID).toString()
        )

        requestData.addProperty("pollId", pollId)
        requestData.add("percentageData", jsonElement)
        requestData.addProperty("pollRating", likelyRating)

        ApiServiceProvider.getInstance(this@PollSubmitActivity).sendPostData(
            Constants.UrlPath.SUBMIT_POLL_ANS, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(
                        this@PollSubmitActivity, "" + message, Toast.LENGTH_SHORT
                    ).show()
                    val i = Intent(this@PollSubmitActivity, SubmitedPollDetailActivity::class.java)
                    i.putExtra("PollId", pollId)
                    i.putExtra("PollType", pollType)
                    startActivity(i)
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun updateText(newText: String) {
        binding.tvMaximumCount.text = "$newText/$sharingValue"
    }

    override fun enlargeImage(imageUrl: String) {
        binding.rlEnlargeImage.setVisible()
        Glide.with(applicationContext).load(imageUrl).into(binding.ivEnlargeImage)
    }
}