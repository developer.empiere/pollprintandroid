package com.poll.print.SplashActivity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.poll.print.BuildConfig
import com.poll.print.R
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashBinding

    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sessionManager = SessionManager(this)

        loadGIF()
        getVersionName()

        startCountDown()
    }

    private fun loadGIF() {
        Glide.with(this@SplashActivity).load(R.drawable.print).into(binding.imgGif)
    }

    private fun startCountDown() {
        object : Thread() {
            override fun run() {
                try {
                    sleep(500)//6000
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    sessionManager.checkLogin()
                }
            }
        }.start()
    }

    @SuppressLint("SetTextI18n")
    private fun getVersionName() {
        binding.tvAppVersionCode.text = "v" + BuildConfig.VERSION_NAME
    }
}