package com.poll.print.DashBoard

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.adminpoll.AdminPollFragment
import com.poll.print.createpoll.CreatePollActivity
import com.poll.print.databinding.ActivityDashBoardBinding
import com.poll.print.profilelist.ProfileListActivity
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.searchpoll.SearchPollActivity
import com.poll.print.userpoll.UserPollFragment
import org.json.JSONObject

class DashBoardActivity : AppCompatActivity(), PollCategoryAdapter.OnCategoryItemClickListener {

    lateinit var binding: ActivityDashBoardBinding
    private lateinit var activityFragmentViewPagerAdapter: ActivityFragmentViewPagerAdapter

    lateinit var mSessionManager: SessionManager
    var userProfile: String = ""
    var userProfileImage: String = ""

    private var pollCategoryModel = mutableListOf<PollCategoryModel>()
    private lateinit var pollCategoryAdapter: PollCategoryAdapter

    var isCategorySelected = false
    var isUserSelected = false

    var selectedCategoryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDashBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()
        userProfileImage = mSessionManager.getData(SessionManager.USER_PROFILE_IMAGE).toString()

        Glide.with(this@DashBoardActivity)
            .load(userProfileImage)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile).onLoadFailed(getDrawable(R.drawable.ic_no_profile))

        askNotificationPermission()

        Log.e("Firebase", "FCM :: ${mSessionManager.getData(SessionManager.FCM_TOKEN).toString()}")

        setOnClickListener()
        setUpViewPager()
        setUpTabLayout()
        getPollCategory()
        setanimation()
    }

    private fun askNotificationPermission() {
        // This is only necessary for API level >= 33 (TIRAMISU)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) !=
                PackageManager.PERMISSION_GRANTED
            ) {
                requestNotificationPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    private val requestNotificationPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (!isGranted) {
            Snackbar.make(
                binding.clMainDashboard,
                String.format(
                    String.format(
                        "Notification disabled!, you'll not get any future update notifications",
                        getString(R.string.app_name)
                    )
                ),
                5000
            ).setAction("Settings") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    val settingsIntent: Intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                    startActivity(settingsIntent)
                }
            }.setActionTextColor(resources.getColor(R.color.colorPrimaryLight)).show()
        }
    }

    fun getPollCategory() {
        val requestData = JsonObject()
        ApiServiceProvider.getInstance(this@DashBoardActivity).sendPostData(
            Constants.UrlPath.GET_POLL_CATEGORY_LIST, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    val categories = responseObj.getJSONArray("categories")
                    pollCategoryModel = Gson().fromJson<Any>(
                        categories.toString(),
                        object : TypeToken<MutableList<PollCategoryModel>?>() {}.type
                    ) as MutableList<PollCategoryModel>

                    setCategorisData()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setanimation() {}

    private fun setCategorisData() {
        pollCategoryAdapter = PollCategoryAdapter(this@DashBoardActivity, pollCategoryModel, this)
        binding.rvPollCategory.adapter = pollCategoryAdapter
    }

    private fun setUpViewPager() {
        activityFragmentViewPagerAdapter =
            ActivityFragmentViewPagerAdapter(supportFragmentManager, lifecycle)
        binding.viewPager.adapter = activityFragmentViewPagerAdapter
    }

    private fun setUpTabLayout() {
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                customizeTab(tab, 10, R.color.textColor_white, true)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                customizeTab(tab, 10, R.color.textColor_white_70)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        TabLayoutMediator(
            binding.tabLayout, binding.viewPager
        ) { tab, position ->
            tab.text = when (position) {
                0 -> "Admin's Polls"
                else -> "User's Polls"
            }
            tab.customView = getCustomTabView(tab.text.toString(), 10, R.color.textColor_white)
        }.attach()

        binding.tabLayout.getTabAt(0)?.select()
    }

    private fun getCustomTabView(tabTitle: String, textSize: Int, textColor: Int): View {
        val tabCustomView: View = layoutInflater.inflate(R.layout.tab_customview, null)
        val tabTextView = tabCustomView.findViewById<TextView>(R.id.title_txt)

        tabTextView.text = tabTitle
        tabTextView.textSize = textSize.toFloat()
        tabTextView.setTextColor(ContextCompat.getColor(tabCustomView.context, textColor))
        return tabCustomView
    }

    private fun customizeTab(
        tab: TabLayout.Tab, tabSizeSp: Int, textColor: Int, isSelected: Boolean = false
    ) {
        val tabCustomView = tab.customView
        tabCustomView?.let {
            val tabTextView: TextView = it.findViewById(R.id.title_txt)

            tabTextView.textSize = tabSizeSp.toFloat()
            tabTextView.setTextColor(ContextCompat.getColor(it.context, textColor))
            if (isSelected) {
                tabTextView.setTextColor(Color.parseColor("#ECD5FF"))
                tabTextView.setBackgroundResource(R.drawable.bg_select_tab)
            } else {
                tabTextView.setTextColor(Color.parseColor("#70ECD5FF"))
                tabTextView.setBackgroundResource(R.drawable.bg_deselect_tab)
            }
        }

        if (tab.position == 0) {
            isUserSelected = false
        } else {
            isUserSelected = true
        }

    }

    private fun setOnClickListener() {
        binding.apply {
            imgProfile.setOnClickListener {
                val i = Intent(this@DashBoardActivity, ProfileListActivity::class.java)
                startActivity(i)
            }
            tvSearch.setOnClickListener {
                val i = Intent(this@DashBoardActivity, SearchPollActivity::class.java)
                startActivity(i)
            }
            tvCreatePoll.setOnClickListener {
                val i = Intent(this@DashBoardActivity, CreatePollActivity::class.java)
                startActivity(i)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (activityFragmentViewPagerAdapter != null) {
            try {
                if (!isUserSelected) {
                    if (isCategorySelected) {
                        val ap: AdminPollFragment =
                            supportFragmentManager.fragments[0] as AdminPollFragment
                        if (ap != null) {
                            ap.getPollCategoryImage(selectedCategoryId)
                        }
                    } else {
                        val ap: AdminPollFragment =
                            supportFragmentManager.fragments[0] as AdminPollFragment
                        if (ap != null) {
                            ap.getAdminPollData()
                        }
                    }
                } else {
                    if (isCategorySelected) {
                        val up: UserPollFragment =
                            supportFragmentManager.fragments[1] as UserPollFragment
                        if (up != null) {
                            up.getUserPollCategoryData(selectedCategoryId)
                        }
                    } else {
                        val up: UserPollFragment =
                            supportFragmentManager.fragments[1] as UserPollFragment
                        if (up != null) {
                            up.getUserPollData()
                        }
                    }
                }
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }

        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE_IMAGE).toString()

        Glide.with(this@DashBoardActivity)
            .load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        getUserProfile()
    }

    private fun getUserProfile() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(this@DashBoardActivity).sendPostData(
            Constants.UrlPath.GET_USER_PROFILE, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                val data = responseObj.getJSONObject("data")
                var country_id = ""

                if (status) {
                    val premiumUserStatus = data.getString("premiumUserStatus")
                    val isPremiumUser = data.getBoolean("isPremiumUser")
                    val country = data.getString("country").toString()

                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, isPremiumUser)

                    if (data.has("countryId")) {
                        country_id = data.getString("countryId").toString()
                        mSessionManager.setData(SessionManager.USER_COUNTRY_ID, country_id)
                    }

                    mSessionManager.setData(SessionManager.USER_COUNTRY, country)

                    if (premiumUserStatus.equals("Submitted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.SUBMITTED
                        )

                    } else if (premiumUserStatus.equals("Rejected")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.REJECTED
                        )

                    } else if (premiumUserStatus.equals("Accepted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.ACCEPTED
                        )

                    } else if (premiumUserStatus.equals("NotApplied")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.NOTAPLLIED
                        )

                    }

                    if (mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM) && (mSessionManager.getIntData(
                            SessionManager.USER_PREMIUM_STATUS
                        ) == Constants.PRO_USER_STATUS.ACCEPTED)
                    ) {
                        binding.tvCreatePoll.visibility = View.VISIBLE
                    } else {
                        binding.tvCreatePoll.visibility = View.GONE
                    }

                } else {
                    Toast.makeText(this@DashBoardActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onItemClick(category_id: String) {

        selectedCategoryId = category_id

        isCategorySelected = true

        if (activityFragmentViewPagerAdapter != null) {
            try {
                if (isUserSelected) {
                    val up: UserPollFragment =
                        supportFragmentManager.fragments[1] as UserPollFragment
                    if (up != null) {
                        up.getUserPollCategoryData(selectedCategoryId)
                    }
                } else {
                    val ap: AdminPollFragment =
                        supportFragmentManager.fragments[0] as AdminPollFragment
                    if (ap != null) {
                        ap.getPollCategoryImage(selectedCategoryId)
                    }
                }
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }

    }

}