package com.poll.print.DashBoard

data class  PollCategoryModel(
    var _id: String? = null,
    var isSelect: Boolean = false,
    var categoryName: String? = null,
    var categoryImageURL: String? = null
)