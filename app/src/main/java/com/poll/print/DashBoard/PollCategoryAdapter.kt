package com.poll.print.DashBoard

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R

class PollCategoryAdapter(
    private val context: Context?,
    var pollCategoryModel: List<PollCategoryModel>?,
    private val listener: OnCategoryItemClickListener
) : RecyclerView.Adapter<PollCategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_poll_category, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pcm = pollCategoryModel!![position]

        holder.txt_name.text = pcm.categoryName
        context?.let {
            Glide.with(it)
                .load(pcm.categoryImageURL).apply(RequestOptions.circleCropTransform())
                .into(holder.img_category)

        }

        if (pcm.isSelect) {
            //holder.txt_name.setTextColor(Color.parseColor("#FFFFFF"))
            holder.txt_name.alpha = 1f
            holder.img_category.background = ContextCompat.getDrawable(
                context!!, (R.drawable.bg_poll_category_selected)
            )
        } else {
            //holder.txt_name.setTextColor(Color.parseColor("#70ECD5FF"))
            holder.txt_name.alpha = .5f
            holder.img_category.background = ContextCompat.getDrawable(
                context!!, (R.drawable.bg_poll_category)
            )
        }

        holder.itemView.setOnClickListener {
            for (i in 0 until pollCategoryModel!!.size) {
                if (pollCategoryModel!![i].isSelect) {
                    pollCategoryModel!![i].isSelect = false
                    holder.img_category.background = ContextCompat.getDrawable(
                        context, (R.drawable.bg_poll_category)
                    )
                    //holder.txt_name.setTextColor(Color.parseColor("#70ECD5FF"))
                    holder.txt_name.alpha = .5f
                    notifyDataSetChanged()
                }
            }
            pcm.isSelect = true
            holder.img_category.background = ContextCompat.getDrawable(
                context, (R.drawable.bg_poll_category_selected)
            )
            //holder.txt_name.setTextColor(Color.parseColor("#FFFFFF"))
            holder.txt_name.alpha = 1f

            listener.onItemClick(pcm._id.toString())

        }
    }

    override fun getItemCount(): Int {
        return pollCategoryModel!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val txt_name: TextView = itemView.findViewById(R.id.tv_name)
        val img_category: RoundedImageView = itemView.findViewById(R.id.img_category)
    }

    interface OnCategoryItemClickListener {
        fun onItemClick(category_id: String)
    }

}