package com.poll.print.DashBoard

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.poll.print.adminpoll.AdminPollFragment
import com.poll.print.userpoll.UserPollFragment

class ActivityFragmentViewPagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                AdminPollFragment()
            }

            else -> {
                UserPollFragment()
            }
        }
    }

    override fun getItemCount() = 2
}