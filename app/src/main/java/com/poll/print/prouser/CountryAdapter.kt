package com.poll.print.prouser

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.poll.print.R

class CountryAdapter(val context: Context, val countryModel: List<CountryModel>) :
    BaseAdapter() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val view = inflater.inflate(R.layout.custom_spinner_item, p2, false)//row_state_city_filter

        val txt_type = view.findViewById(R.id.textViewSpinnerItem) as TextView

        if (position == 0) {
            txt_type.setTextColor(Color.GRAY)
        }

        txt_type.text = countryModel[position].countryName

        return view
    }

    override fun getCount(): Int {
        return countryModel.size
    }

    override fun getItem(p0: Int): Any {
        return countryModel[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }
}
