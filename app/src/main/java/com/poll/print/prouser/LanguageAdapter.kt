package com.poll.print.prouser

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.poll.print.R

class LanguageAdapter(val context: Context, val languageModel: List<LanguageModel>) :
    BaseAdapter() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val view = inflater.inflate(R.layout.custom_spinner_item, p2, false)//row_state_city_filter

        val txt_type = view.findViewById(R.id.textViewSpinnerItem) as TextView

        txt_type.text = languageModel[position].name

        return view
    }

    override fun getCount(): Int {
        return languageModel.size
    }

    override fun getItem(p0: Int): Any {
        return languageModel[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }
}
