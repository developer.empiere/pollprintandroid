package com.poll.print.prouser

data class LanguageModel(var _id: String = "", var name: String = "", var code: String = "")