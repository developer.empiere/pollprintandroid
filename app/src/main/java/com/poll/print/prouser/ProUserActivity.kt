package com.poll.print.prouser

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.gngc.utils.Permissons
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.ImagePickerActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityProUserBinding
import com.poll.print.editprofile.AddProfilePicFragment
import com.poll.print.editprofile.PathUtil
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import com.yalantis.ucrop.UCrop
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.File

class ProUserActivity : AppCompatActivity() {
    lateinit var binding: ActivityProUserBinding

    lateinit var mSessionManager: SessionManager
    private lateinit var profileImage: File
    var mPicturePath: String = ""
    var locationImageUrl: String = ""
    val imageStoreId: String = System.currentTimeMillis().toString()
    var genderName: String = ""
    var educationLevel: String = ""
    var professionId: String = ""
    var annualIncome: Int = 0
    var languageId: String = ""
    var ethnicityName: String = ""
    var religionId: String = ""
    var politicalName: String = ""
    var citizenshipName: String = ""
    var citizenshipId: String = ""

    private var professionModel: MutableList<ProfessionModel>? = null
    private var languageModel: MutableList<LanguageModel>? = null
    private var religionModel: MutableList<ReligionModel>? = null
    private var countryModel: MutableList<CountryModel>? = null


    companion object {
        private const val CAMERA_IMAGE_REQ_CODE = 103
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        binding.headerProUser.tvTitle.text = "Pro User"
        mSessionManager = SessionManager(this)

        setOnCLickLstener()
        getProfession()
        getLanguage()
        getReligion()
        getCountriList()

        binding.seekbarAnnualIncome.setMin(0f)
        binding.seekbarAnnualIncome.setMax(250f)

        locationImageUrl = mSessionManager.getData(SessionManager.USER_PROFILE).toString()

        if (locationImageUrl.equals("")) {
            Glide.with(this@ProUserActivity)
                .load(R.drawable.ic_no_profile)
                .placeholder(R.drawable.ic_no_profile)
                .into(binding.imgProfile)
        } else {
            Glide.with(this@ProUserActivity)
                .load(locationImageUrl)
                .placeholder(R.drawable.ic_no_profile)
                .into(binding.imgProfile)
        }

        binding.edtName.setText(mSessionManager.getData(SessionManager.USER_NAME))
        binding.edtEmail.setText(mSessionManager.getData(SessionManager.USER_EMAIL))

        if (intent.getStringExtra("userCountry") == null)
            citizenshipName = mSessionManager.getData(SessionManager.USER_COUNTRY).toString()
        else
            citizenshipName = intent.getStringExtra("userCountry").toString()

        val gender = arrayOf(
            "Straight Male",
            "Straight Female",
            "LGBTQ+",
            "Prefer Not To Say"
        )
        val adapterGender = ArrayAdapter<String>(
            this,
            R.layout.custom_spinner_item,
            R.id.textViewSpinnerItem,
            gender
        )
        adapterGender.setDropDownViewResource(R.layout.custom_spinner_item)
        binding.spGender.adapter = adapterGender

        binding.spGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    genderName = gender[position]
                    println("Selecte gender:-" + genderName)
                } catch (e: Exception) {
                    println("Error accessing gender array: ${e.message}")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        val educationLevels = arrayOf(
            "No Formal Education",
            "High School",
            "Vocational/Technical Training",
            "Associate's Degree",
            "Bachelor's Degree",
            "Master's Degree",
            "Doctoral/Ph.D.",
            "Professional Degree",
            "Other"
        )
        val adapter = ArrayAdapter<String>(
            this,
            R.layout.custom_spinner_item,
            R.id.textViewSpinnerItem,
            educationLevels
        )
        adapter.setDropDownViewResource(R.layout.custom_spinner_item)
        binding.spEducation.adapter = adapter

        binding.spEducation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    educationLevel = educationLevels[position]
                } catch (e: Exception) {
                    println("Error accessing gender array: ${e.message}")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        val ethnicity = arrayOf(
            "Ethnicity",
            "Race",
            "Caste",
            "Class"
        )
        val adapterEthnicity = ArrayAdapter<String>(
            this,
            R.layout.custom_spinner_item,
            R.id.textViewSpinnerItem,
            ethnicity
        )

        adapterEthnicity.setDropDownViewResource(R.layout.custom_spinner_item)
        binding.spEthnicity.adapter = adapterEthnicity

        binding.spEthnicity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                try {
                    ethnicityName = ethnicity[position]
                } catch (e: Exception) {
                    println("Error accessing gender array: ${e.message}")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        val politicalIdentity = arrayOf(
            "Independent",
            "Left",
            "Center",
            "Right"
        )
        val adapterPoliticalIdentity = ArrayAdapter<String>(
            this,
            R.layout.custom_spinner_item,
            R.id.textViewSpinnerItem,
            politicalIdentity
        )

        adapterPoliticalIdentity.setDropDownViewResource(R.layout.custom_spinner_item)
        binding.spPoliticalIdentity.adapter = adapterPoliticalIdentity

        binding.spPoliticalIdentity.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    try {
                        politicalName = politicalIdentity[position]
                    } catch (e: Exception) {
                        println("Error accessing gender array: ${e.message}")
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }

    }

    private fun getCountriList() {
        ApiServiceProvider.getInstance(this@ProUserActivity)
            .sendSpinnerData(
                Constants.UrlPath.GET_COUNTRY,
                true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            val countries = responseObj.getJSONArray("countries")
                            countryModel = Gson().fromJson<Any>(
                                countries.toString(),
                                object : TypeToken<MutableList<CountryModel>?>() {}.type
                            ) as MutableList<CountryModel>

                            val cm = CountryModel()
                            cm.countryName = "Citizenship"
                            countryModel!!.add(0, cm)
                            setCountriList(countryModel!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setCountriList(cModel: MutableList<CountryModel>) {
        binding.spCitizenship.adapter = this@ProUserActivity?.let { CountryAdapter(it, cModel) }

        for (i in 0 until countryModel!!.size) {
            if (citizenshipName == countryModel!![i].countryName) {
                binding.spCitizenship.setSelection(i)
                citizenshipId = countryModel!![i]._id
                break
            }
        }

    }

    private fun getLanguage() {
        ApiServiceProvider.getInstance(this@ProUserActivity)
            .sendSpinnerData(
                Constants.UrlPath.GET_PRO_USER_LANGUAGE,
                true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            val languages = responseObj.getJSONArray("languages")
                            languageModel = Gson().fromJson<Any>(
                                languages.toString(),
                                object : TypeToken<MutableList<LanguageModel>?>() {}.type
                            ) as MutableList<LanguageModel>

                            setLanguageList(languageModel!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setLanguageList(lModel: MutableList<LanguageModel>) {
        binding.spLangugae.adapter = this@ProUserActivity?.let { LanguageAdapter(it, lModel) }
    }

    private fun getProfession() {
        ApiServiceProvider.getInstance(this@ProUserActivity)
            .sendSpinnerData(
                Constants.UrlPath.PROFESSION,
                true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            val profession = responseObj.getJSONArray("profession")
                            professionModel = Gson().fromJson<Any>(
                                profession.toString(),
                                object : TypeToken<MutableList<ProfessionModel>?>() {}.type
                            ) as MutableList<ProfessionModel>

                            val pm = ProfessionModel()
                            pm.name = "Profession"
                            professionModel!!.add(0, pm)
                            setProfessionList(professionModel!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setProfessionList(pModel: MutableList<ProfessionModel>) {
        binding.spPorfession.adapter = this@ProUserActivity?.let { ProfessionAdapter(it, pModel) }
    }

    private fun getReligion() {
        ApiServiceProvider.getInstance(this@ProUserActivity)
            .sendSpinnerData(
                Constants.UrlPath.RELIGIONLIST,
                true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            val religion = responseObj.getJSONArray("religion")

                            religionModel = Gson().fromJson<Any>(
                                religion.toString(),
                                object : TypeToken<MutableList<ReligionModel>?>() {}.type
                            ) as MutableList<ReligionModel>

                            val rm = ReligionModel()
                            rm.name = "Religion"
                            religionModel!!.add(0, rm)
                            setReligionList(religionModel!!)

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setReligionList(rModel: MutableList<ReligionModel>) {
        binding.spReligion.adapter = this@ProUserActivity?.let { ReligionAdapter(it, rModel) }
    }

    private fun setOnCLickLstener() {
        binding.apply {
            editAnnualIncome.setOnClickListener {
                if (editAnnualIncome.tag == "0") {
                    clAnnualIncome.visibility = View.VISIBLE
                    editAnnualIncome.tag = "1"
                } else {
                    clAnnualIncome.visibility = View.GONE
                    editAnnualIncome.tag = "0"
                }
            }
            seekbarAnnualIncome.setOnSeekChangeListener(object : OnSeekChangeListener {
                override fun onSeeking(seekParams: SeekParams) {
                    annualIncome = seekParams.progress
                    editAnnualIncome.setText("$annualIncome" + "k")

                }

                override fun onStartTrackingTouch(seekBar: IndicatorSeekBar) {}
                override fun onStopTrackingTouch(seekBar: IndicatorSeekBar) {}
            })
            headerProUser.imgBack.setOnClickListener {
                finish()
            }
            headerProUser.imgClose.setOnClickListener {
                finish()
            }
            btnSubmit.setOnClickListener {
                if (checkValidation()) {
                    submitProUser()
                }
            }
            imageProfileEdit.setOnClickListener {
                showIntentChooser()
            }

            spPorfession.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        professionId = professionModel!![position]._id
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }
            spLangugae.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        languageId = languageModel!![position]._id
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }
            spReligion.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        religionId = religionModel!![position]._id
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }
            spCitizenship.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long
                    ) {
                        citizenshipName = countryModel!![position].countryName
                        citizenshipId = countryModel!![position]._id
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {
                    }
                }
        }
    }

    private fun showIntentChooser() {
        val addProfilePicFragment = AddProfilePicFragment()
        addProfilePicFragment.setListener(listener = object : AddProfilePicFragment.Listener {
            override fun returnData(selectedType: Int?) {
                if (selectedType == Constants.RequestPermission.REQUEST_GALLERY) {
                    askForGalleryPermission()

                } else if (selectedType == Constants.RequestPermission.REQUEST_CAMERA) {
                    askForCameraPermission()
                }
            }
        })
        addProfilePicFragment.show(supportFragmentManager, "ADD_PROFILE_PIC")
    }

    private fun askForGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU && !Permissons.Check_MEDIA_IMAGES(
                this
            )
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_MEDIA_IMAGES), Permissons.WRITE_EXTERNAL_STORAGE
            )
        } else openGallery()
    }

    private fun openGallery() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, Constants.RequestPermission.REQUEST_GALLERY)
    }

    private fun checkValidation(): Boolean {
        if (AppUtils.isEditTextEmpty(binding.edtName)) {
            Toast.makeText(this@ProUserActivity, "Please Enter Name", Toast.LENGTH_SHORT).show()
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtEmail)) {
            Toast.makeText(this@ProUserActivity, "Please Enter Email", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun submitProUser() {
        val salary = "${annualIncome}000"
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID))
        jsonObject.addProperty("email", binding.edtEmail.text.toString())
        jsonObject.addProperty("name", binding.edtName.text.toString())
        jsonObject.addProperty("imageURL", locationImageUrl)
        jsonObject.addProperty("gender", genderName)
        jsonObject.addProperty("educationLevel", educationLevel)
        jsonObject.addProperty("profession", professionId)
        jsonObject.addProperty("annualIncome", salary.toInt())
        jsonObject.addProperty("languageId", languageId)
        jsonObject.addProperty("ethnicity", ethnicityName)
        jsonObject.addProperty("religionId", religionId)
        jsonObject.addProperty("politicalIdentity", politicalName)
        jsonObject.addProperty("globalIdentity", citizenshipId)

        ApiServiceProvider.getInstance(this@ProUserActivity).sendPostData(
            Constants.UrlPath.PRO_USER, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    Toast.makeText(this@ProUserActivity, "" + message, Toast.LENGTH_SHORT).show()
                    mSessionManager.setData(
                        SessionManager.USER_PREMIUM_STATUS,
                        Constants.PRO_USER_STATUS.SUBMITTED
                    )
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestPermission.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                val fileName = "front"
                val destination = Uri.fromFile(File(cacheDir, "cropped_$fileName"))
                UCrop.of(Uri.fromFile(File(mPicturePath)), destination).withAspectRatio(1F, 1F)
                    .start(this)
            }

        } else if (requestCode == Constants.RequestPermission.REQUEST_GALLERY) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                profileImage = File(uri!!.path.toString())
                binding.imgProfile.setImageURI(uri)
                uploadImages()
            }

        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == RESULT_OK) {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, UCrop.getOutput(data!!))
                Glide.with(this@ProUserActivity).load(bitmap).into(binding.imgProfile)
                profileImage = File(PathUtil.getPath(this, UCrop.getOutput(data)))

            } else if (resultCode == UCrop.RESULT_ERROR) {
            }
        }
    }

    private fun uploadImages() {
        val imageType: RequestBody =
            "OptionImage".toRequestBody("text/plain".toMediaTypeOrNull())

        val imageStoreId: RequestBody =
            imageStoreId.toString()
                .toRequestBody("text/plain".toMediaTypeOrNull())

        val uploadImage: RequestBody =
            profileImage.asRequestBody("multipart/form- *".toMediaTypeOrNull())
        val uploadImagePart: MultipartBody.Part =
            MultipartBody.Part.createFormData("userImage", profileImage.name, uploadImage)

        ApiServiceProvider.getInstance(this@ProUserActivity).uploadImage(
            Constants.UrlPath.UPLOAD_IMAGE, imageType, imageStoreId, uploadImagePart, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val success = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                locationImageUrl = responseObj.getString("location")
                mSessionManager.setData(SessionManager.USER_PROFILE, locationImageUrl)
                if (success) {
                    Toast.makeText(this@ProUserActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()

                    if (locationImageUrl.equals("")) {
                        Glide.with(this@ProUserActivity)
                            .load(R.drawable.ic_no_profile)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    } else {
                        Glide.with(this@ProUserActivity)
                            .load(locationImageUrl)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Permissons.WRITE_EXTERNAL_STORAGE -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery()
                }
            } else askForGalleryPermission()

            Permissons.CAMERA -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                }
            } else askForCameraPermission()
        }
    }

    private fun askForCameraPermission() {
        if (!Permissons.Check_CAMERA(this)) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                Permissons.CAMERA
            )
        } else openCamera()
    }

    private fun openCamera() {
        ImagePicker.with(this)
            .cameraOnly()
            // .compress(1024) // Image size will be less than 1024 KB
            .saveDir(getExternalFilesDir(null)!!)  // Path: /storage/sdcard0/Android/data/package/files
            .start(CAMERA_IMAGE_REQ_CODE)
    }
}