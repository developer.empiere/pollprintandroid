package com.poll.print.mypoll

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.charts.Pie
import com.bumptech.glide.Glide
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R
import com.poll.print.myactivitydetails.BarLegendsAdapter
import com.poll.print.mypolldetail.MyPollDetailsActivity
import org.json.JSONArray

class MyPollAdapter(
    private val context: Context?,
    var myPollModel: List<MyPollModel>?,
    var pollPrints: JSONArray
) : RecyclerView.Adapter<MyPollAdapter.ViewHolder>() {

    var dataPieChart: MutableList<DataEntry> = mutableListOf()
    val options = ArrayList<String>()
    val percentage = ArrayList<String>()

    lateinit var pieDataInformation: ArrayList<PieEntry>
    lateinit var pieDataSet: PieDataSet

    lateinit var barDataInformation: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barData: BarData
    lateinit var barDataSetList: ArrayList<BarDataSet>

    lateinit var material_color_list: IntArray

    private lateinit var barLegendsAdapter: BarLegendsAdapter

    val answerCountList = mutableListOf<List<String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_my_poll, parent, false)

        material_color_list = intArrayOf(
            ColorTemplate.rgb("#2ecc71"),
            ColorTemplate.rgb("#f1c40f"),
            ColorTemplate.rgb("#e74c3c"),
            ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#e9724d"),
            ColorTemplate.rgb("#d6d727"),
            ColorTemplate.rgb("#92cad1"),
            ColorTemplate.rgb("#79ccb3"),
            ColorTemplate.rgb("#868686"),
            ColorTemplate.rgb("#391954"),
            ColorTemplate.rgb("#631e50"),
            ColorTemplate.rgb("#a73c5a"),
            ColorTemplate.rgb("#b0d7e1"),
            ColorTemplate.rgb("#a7e237"),
            ColorTemplate.rgb("#37bd79")
        )

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val mpm = myPollModel!![position]

        dataPieChart.clear()
        options.clear()
        percentage.clear()
        answerCountList.clear()

        holder.tv_user_name.text = mpm.userName
        holder.tv_user_question.text = mpm.pollTitle
        holder.tv_user_question_description.text = mpm.pollSubTitle
        holder.tv_tv_total_poll_ans.text = mpm.totalAnswers.toString() + " Pollprint"

        Glide.with(context!!).load(mpm.imageURL).placeholder(R.drawable.ic_no_profile)
            .into(holder.img_user)

        mpm.pollOptions.forEachIndexed { index, pollOption ->
            options.add(mpm.pollOptions[index].optionTitle)

            if (mpm.pollType != "Preference choice")
                percentage.add(mpm.pollOptions[index].answerCount.toString().replace("\"", ""))
            else {

                try {
                    val answerCount = mpm.pollOptions[index].answerCount.asJsonObject

                    val valuesList = mutableListOf<String>()

                    answerCount.keySet().forEach {
                        valuesList.add(answerCount.get(it).toString())
                    }
                    answerCountList.add(valuesList)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        //Preference choice = 3 , Yes/No = 0 , Percent share = 4 , multiplechoice = 2 , Percentage sharing on option
        if(mpm.totalAnswers > 0) {
            if (mpm.pollType == "multiplechoice" || mpm.pollType == "Multiple choice" || mpm.pollType == "Percent share" ||
                mpm.pollType == "Percentage sharing on option"
            )
                showMultipleSelectionChart(holder)
            else if (mpm.pollType == "Preference choice")
                showPreferenceChoiceChart(holder)
            else
                showPieChart(holder)
        }else{
            holder.pieChart.visibility = View.GONE
            holder.barChart.visibility = View.GONE
            holder.rvBarLegend.visibility = View.GONE
            holder.tv_no_data.visibility = View.VISIBLE
        }

        holder.tv_view_result.setOnClickListener {
            if (holder.tv_view_result.tag == "1") {
                holder.tv_view_result.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_expand, 0
                )
                holder.cl_graph.visibility = View.VISIBLE
                holder.tv_view_result.tag = "0"
            } else {
                holder.tv_view_result.setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.ic_collapse, 0
                )

                holder.cl_graph.visibility = View.GONE

                holder.tv_view_result.tag = "1"
            }
        }

        holder.itemView.setOnClickListener {
            val i = Intent(context, MyPollDetailsActivity::class.java)
            i.putExtra("pollId", mpm.pollId)
            i.putExtra("PollType", mpm.pollType)
            i.putExtra("PollDisplayType", "")
            context.startActivity(i)
        }
    }

    private fun showMultipleSelectionChart(holder: ViewHolder) {

        holder.pieChart.visibility = View.GONE
        holder.barChart.visibility = View.VISIBLE
        holder.rvBarLegend.visibility = View.VISIBLE
        holder.tv_no_data.visibility = View.GONE

        //MPChart
        barDataInformation = ArrayList()

        for (index in percentage.indices) {
            try {
                barDataInformation.add(
                    BarEntry(
                        (index + 1).toFloat(),
                        percentage[index].replace("%", "").replace("\"","").toFloat()
                    )
                )
                barDataSet = BarDataSet(barDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // Custom value formatter to format the value
        barDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "0%" else ("" + value.toInt() + "%")
            }
        }

        barDataSet.setColors(material_color_list, 250)
        barDataSet.valueTextColor = context!!.resources.getColor(R.color.black)
        barDataSet.valueTextSize = 7f

        barData = BarData(barDataSet)

        holder.barChart.setFitBars(true)
        holder.barChart.legend.isEnabled = false
        holder.barChart.data = barData
        holder.barChart.description.text = ""
        holder.barChart.animateY(700)

        holder.barChart.invalidate()

        if (options.size != 0) {
            val params = holder.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
            params.topToBottom = holder.barChart.id
            setBarLegends(options, percentage, material_color_list, holder)
        }

    }

    private fun showPreferenceChoiceChart(holder: ViewHolder) {

        holder.pieChart.visibility = View.GONE
        holder.barChart.visibility = View.VISIBLE
        holder.rvBarLegend.visibility = View.VISIBLE
        holder.tv_no_data.visibility = View.GONE

        barDataSetList = ArrayList()
        barData = BarData()

        var numm = 1f

        try {
            for (index in answerCountList.indices) {

                barDataInformation = ArrayList()

                for (i in 0 until answerCountList[index].size) {

                    barDataInformation.add(
                        BarEntry(
                            numm,
                            answerCountList[index][i].replace("%", "").replace("\"", "")
                                .toFloat()
                        )
                    )
                    numm++
                }

                barDataSet = BarDataSet(barDataInformation, options[index])
                barDataSet.color = material_color_list[index]
                barDataSetList.add(barDataSet)

                numm += 2 //Required for x-coordinator for bar graph
            }

            // Custom value formatter to format the value
            barDataSet.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return if (value == 0f) "0%" else ("" + value.toInt() + "%")
                }
            }

            for (i in 0 until barDataSetList.size)
                barData.addDataSet(barDataSetList[i])

            holder.barChart.data = barData
            holder.barChart.legend.isEnabled = false

            barDataSet.valueTextColor = context!!.resources.getColor(R.color.black)
            barDataSet.valueTextSize = 7f

            val groupSpace = 0.8f
            val barSpace = 0.03f
            val barWidth = 1f

            barData.barWidth = barWidth

            holder.barChart.description.isEnabled = false
            holder.barChart.xAxis.setCenterAxisLabels(true)
            holder.barChart.setFitBars(true)
            holder.barChart.groupBars(0f, groupSpace, barSpace)
            holder.barChart.animateY(700)

            holder.barChart.invalidate()

            if (options.size != 0) {
                val params = holder.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
                params.topToBottom = holder.barChart.id
                setBarLegends(options, percentage, material_color_list, holder)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showPieChart(holder: ViewHolder) {

        holder.pieChart.visibility = View.VISIBLE
        holder.barChart.visibility = View.GONE
        holder.rvBarLegend.visibility = View.VISIBLE
        holder.tv_no_data.visibility = View.GONE

        //MPChart
        pieDataInformation = ArrayList()
        for (index in percentage.indices) {
            try {
                pieDataInformation.add(
                    PieEntry(
                        percentage[index].replace("%", "").replace("\"","").toFloat(),
                        options[index]
                    )
                )
                pieDataSet = PieDataSet(pieDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        pieDataSet.colors = material_color_list.asList()
        pieDataSet.valueTextSize = 8f

        val data = PieData(pieDataSet)

        holder.pieChart.data = data
        holder.pieChart.description.isEnabled = false
        holder.pieChart.isDrawHoleEnabled = false
        holder.pieChart.setDrawEntryLabels(false)
        holder.pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        holder.pieChart.legend.isWordWrapEnabled = true
        holder.pieChart.legend.isEnabled = false

        // Center text inside slices
        pieDataSet.xValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE
        pieDataSet.yValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE

        // Custom value formatter to format the value
        pieDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "" else ("" + value.toInt() + "%")
            }
        }

        holder.pieChart.invalidate()

        if (options.size != 0) {
            val params = holder.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
            params.topToBottom = holder.pieChart.id
            setBarLegends(options, percentage, material_color_list, holder)
        }

    }

    private fun setBarLegends(
        options: ArrayList<String>,
        percentage: ArrayList<String>,
        materialColors: IntArray,
        holder: ViewHolder
    ) {
        barLegendsAdapter =
            BarLegendsAdapter(context, options, percentage, materialColors)
        holder.rvBarLegend.layoutManager = GridLayoutManager(context, 2)
        holder.rvBarLegend.adapter = barLegendsAdapter
    }

    override fun getItemCount(): Int {
        return myPollModel!!.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val cl_graph: ConstraintLayout = itemView.findViewById(R.id.cl_graph)
        val barChart: BarChart = itemView.findViewById(R.id.barChart)
        val rvBarLegend: RecyclerView = itemView.findViewById(R.id.rvBarLegend)
        val img_user: RoundedImageView = itemView.findViewById(R.id.img_user)
        val tv_no_data: TextView = itemView.findViewById(R.id.tv_no_data)
        val tv_view_result: TextView = itemView.findViewById(R.id.tv_view_result)
        val tv_tv_total_poll_ans: TextView = itemView.findViewById(R.id.tv_tv_total_poll_ans)
        val tv_user_name: TextView = itemView.findViewById(R.id.tv_user_name)
        val tv_user_question: TextView = itemView.findViewById(R.id.tv_user_question)
        val tv_user_question_description: TextView =
            itemView.findViewById(R.id.tv_user_question_description)
        val pieChart: PieChart = itemView.findViewById(R.id.pieChart)
    }
}