package com.poll.print.mypoll

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName

data class MyPollModel(
    @SerializedName("pollId") val pollId: String,
    @SerializedName("userName") val userName: String,
    @SerializedName("imageURL") val imageURL: String,
    @SerializedName("pollTitle") val pollTitle: String,
    @SerializedName("pollSubTitle") val pollSubTitle: String,
    @SerializedName("timeStamp") val timeStamp: String,
    @SerializedName("pollType") val pollType: String,
    @SerializedName("pollOptionsCount") val pollOptionsCount: Int,
    @SerializedName("totalAnswers") val totalAnswers: Int,
    @SerializedName("pollOptions") val pollOptions: List<PollOption>
)

data class PollOption(
    @SerializedName("optionId") val optionId: String,
    @SerializedName("optionTitle") val optionTitle: String,
    @SerializedName("answerCount") val answerCount: JsonElement
)