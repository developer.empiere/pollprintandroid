package com.poll.print.mypoll

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.Utils.AppUtils.setVisible
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.FragmentMyPollBinding
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONArray
import org.json.JSONObject

class MyPollFragment : Fragment() {

    private var _binding: FragmentMyPollBinding? = null
    private val binding get() = _binding!!

    lateinit var mSessionManager: SessionManager
    var userId: String = ""

    private var myPollModel = ArrayList<MyPollModel>()
    private lateinit var myPollAdapter: MyPollAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyPollBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("RtlHardcoded")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mSessionManager = SessionManager(context)

        userId = mSessionManager.getData(SessionManager.USER_ID).toString()

        getMyPoll()
    }

    private fun getMyPoll() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", userId)

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_MY_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                if (status) {
                    val pollPrints = responseObj.getJSONArray("pollPrints")

                    if (pollPrints.length() == 0) {
                        binding.tvNoData.setVisible()
                    } else {
                        myPollModel = Gson().fromJson<Any>(
                            pollPrints.toString(),
                            object : TypeToken<ArrayList<MyPollModel>?>() {}.type
                        ) as ArrayList<MyPollModel>

                        setMyPollData(pollPrints)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setMyPollData(pollPrints: JSONArray) {
        myPollAdapter = MyPollAdapter(context, myPollModel, pollPrints)
        binding.rvMyPoll.adapter = myPollAdapter
    }
}