package com.poll.print.logout

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.gson.JsonObject
import com.poll.print.Login.LoginActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.DialogLogoutBinding
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import org.json.JSONObject

class DialogLogout : DialogFragment() {

    private var _binding: DialogLogoutBinding? = null
    private val binding get() = _binding!!
    lateinit var mSessionManager: SessionManager

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(STYLE_NO_TITLE, R.style.CustomAlertDialog)
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogLogoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireDialog().window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            AppUtils.setStatusBar(it, requireActivity())
        }

        mSessionManager = SessionManager(context)

        setUpClickListener()
    }

    private fun setUpClickListener() {
        binding.tvNo.setOnClickListener {
            dismiss()
        }
        binding.tvYes.setOnClickListener {
            checkNumberExist()
        }
    }

    private fun userLogout() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.LOGOUT, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                if (status) {
                    mSessionManager.setUserLogin(false)
                    mSessionManager.setData(SessionManager.FCM_TOKEN, "")
                    dismiss()
                    val i = Intent(context, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                    requireActivity().finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun checkNumberExist() {
        val userMobile = mSessionManager.getData(SessionManager.USER_MOBILE)

        val jsonObject = JsonObject()
        jsonObject.addProperty("mobile", userMobile)

        ApiServiceProvider.getInstance(context)
            .sendPostData(
                Constants.UrlPath.NUMBER_EXISTS, jsonObject, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            userLogout()
                        } else {
                            mSessionManager.setUserLogin(false)
                            mSessionManager.setData(SessionManager.FCM_TOKEN, "")
                            dismiss()
                            val i = Intent(context, LoginActivity::class.java)
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(i)
                            requireActivity().finish()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

}