package com.poll.print.pollsubmiteddetail

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import com.poll.print.myactivitydetails.MyActivityOwnResearchModel

data class SubmittedPollDetailsModel(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("message")
    val message: String,
    @SerializedName("pollData")
    val pollData: PollData
)

data class PollData(
    @SerializedName("userName")
    val userName: String,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("pollTitle")
    val pollTitle: String,
    @SerializedName("pollSubTitle")
    val pollSubTitle: String,
    @SerializedName("timeStamp")
    val timeStamp: String,
    @SerializedName("pollType")
    val pollType: String,
    @SerializedName("pollImages")
    val pollImages: ArrayList<String>,
    @SerializedName("pollOptionsCount")
    val pollOptionsCount: Int,
    @SerializedName("totalAnswers")
    val totalAnswers: Int,
    @SerializedName("pollOptions")
    val pollOptions: ArrayList<PollOption>,
    @SerializedName("researchData")
    val researchData: ArrayList<MyActivityOwnResearchModel>
)

data class PollOption(
    @SerializedName("optionId")
    val optionId: String,
    @SerializedName("optionTitle")
    val optionTitle: String,
    @SerializedName("optionImages")
    val optionImages: ArrayList<String>,
    @SerializedName("answerCount")
    val answerCount: JsonElement
)