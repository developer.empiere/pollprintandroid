package com.poll.print.pollsubmiteddetail

import android.os.Bundle
import android.util.ArrayMap
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivitySubmitedPollDetailBinding
import com.poll.print.myactivitydetails.BarLegendsAdapter
import com.poll.print.myactivitydetails.MyActivityOwnResearchAdapter
import com.poll.print.myactivitydetails.PollResponse
import com.poll.print.myactivitydetails.PollResultAdapter
import com.poll.print.myactivitydetails.PollResultUpdatedAdapter
import com.poll.print.mypolldetail.MyUserPollOptionsModel
import com.poll.print.retrofit.ApiServiceProvider
import org.json.JSONObject

class SubmitedPollDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivitySubmitedPollDetailBinding

    lateinit var mSessionManager: SessionManager
    var userId: String = ""
    var userProfile: String = ""
    var pollId: String = ""

    var userPremiumStatus: Int = 0
    var isUserPremium: Boolean = false

    private var submittedPollDetailsModel: SubmittedPollDetailsModel? = null
    private lateinit var myActivityOwnResearchAdapter: MyActivityOwnResearchAdapter

    private var myUserPollOptionsModel = mutableListOf<MyUserPollOptionsModel>()

    lateinit var optionsColorList: ArrayMap<String, Int>

    private lateinit var barLegendsAdapter: BarLegendsAdapter
    private lateinit var pollResultDetailsAdapter: PollResultAdapter

    private lateinit var pollResultDetailsUpdateAdapter: PollResultUpdatedAdapter

    var user_sort_id = ""

    val options = ArrayList<String>()
    val percentage = ArrayList<String>()

    var pollType = ""

    val optionsIDList = ArrayList<String>()

    lateinit var pieDataInformation: ArrayList<PieEntry>
    lateinit var pieDataSet: PieDataSet

    lateinit var barDataSetList: ArrayList<BarDataSet>
    lateinit var barDataInformation: ArrayList<BarEntry>
    lateinit var barDataSet: BarDataSet
    lateinit var barData: BarData

    val answerCountList = mutableListOf<List<String>>()

    lateinit var material_color_list: IntArray

    var isPieChart = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySubmitedPollDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this@SubmitedPollDetailActivity)
        userId = mSessionManager.getData(SessionManager.USER_ID).toString()
        user_sort_id = mSessionManager.getData(SessionManager.SORT_ID).toString()
        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()

        Glide.with(this@SubmitedPollDetailActivity)
            .load(userProfile)
            .placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        material_color_list = intArrayOf(
            ColorTemplate.rgb("#2ecc71"),
            ColorTemplate.rgb("#f1c40f"),
            ColorTemplate.rgb("#e74c3c"),
            ColorTemplate.rgb("#3498db"),
            ColorTemplate.rgb("#e9724d"),
            ColorTemplate.rgb("#d6d727"),
            ColorTemplate.rgb("#92cad1"),
            ColorTemplate.rgb("#79ccb3"),
            ColorTemplate.rgb("#868686"),
            ColorTemplate.rgb("#391954"),
            ColorTemplate.rgb("#631e50"),
            ColorTemplate.rgb("#a73c5a"),
            ColorTemplate.rgb("#b0d7e1"),
            ColorTemplate.rgb("#a7e237"),
            ColorTemplate.rgb("#37bd79")
        )

        getIntentData()
        setOnclickListener()

        userPremiumStatus = mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS)
        isUserPremium = mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)

        if (!isUserPremium || (userPremiumStatus != Constants.PRO_USER_STATUS.ACCEPTED)) {
            binding.clOwnResearch.visibility = View.GONE
        }

        getPollDetails()
    }

    private fun getIntentData() {
        pollId = intent.getStringExtra("PollId").toString()
        pollType = intent.getStringExtra("PollType").toString()
    }

    private fun getPollDetails() {
        val jsonObject = JsonObject()

        jsonObject.addProperty("pollId", pollId)
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())

        ApiServiceProvider.getInstance(this@SubmitedPollDetailActivity).sendPostData(
            Constants.UrlPath.GET_POLL_DETAILS, jsonObject, false
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                val pollResponseType = object : TypeToken<PollResponse>() {}.type
                val pollResponse: PollResponse =
                    Gson().fromJson(responseObj.toString(), pollResponseType)

                if (status) {
                    val pollDetails = responseObj.getJSONObject("pollData")
                    val pollOptions = pollDetails.getJSONArray("pollOptions")
                    val totalAnswers = pollDetails.getInt("totalAnswers")

                    submittedPollDetailsModel = Gson().fromJson<Any>(
                        responseObj.toString(),
                        object : TypeToken<SubmittedPollDetailsModel>() {}.type
                    ) as SubmittedPollDetailsModel

                    binding.tvUserName.text = submittedPollDetailsModel!!.pollData.userName
                    binding.tvUserQuestion.text =
                        submittedPollDetailsModel!!.pollData.pollTitle
                    binding.tvTime.text =
                        submittedPollDetailsModel!!.pollData.totalAnswers.toString() + " Pollprint"
                    binding.tvUserQuestionDescription.text =
                        submittedPollDetailsModel!!.pollData.pollSubTitle

                    Glide.with(this@SubmitedPollDetailActivity)
                        .load(submittedPollDetailsModel!!.pollData.imageURL)
                        .placeholder(R.drawable.ic_no_profile).into(binding.imgUser)

                    optionsIDList.clear()

                    submittedPollDetailsModel!!.pollData.pollOptions.forEachIndexed { index, pollOption ->
                        options.add(pollOption.optionTitle)
                        optionsIDList.add(pollOption.optionId)

                        if (pollType != "Preference choice")
                            percentage.add(pollOption.answerCount.toString())
                        else {
                            try {
                                val option = pollOptions.getJSONObject(index)
                                val answerCount = option.getJSONObject("answerCount")

                                val valuesList = mutableListOf<String>()
                                answerCount.keys().forEach {
                                    valuesList.add(answerCount.getString(it))
                                }
                                answerCountList.add(valuesList)

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                    }

                    if (pollType.equals("Preference choice", true)) {
                        val userPollOptions = pollDetails.getJSONArray("userPollOption")

                        if (userPollOptions.length() != 0) {
                            myUserPollOptionsModel = Gson().fromJson<Any>(
                                userPollOptions.toString(),
                                object : TypeToken<MutableList<MyUserPollOptionsModel>?>() {}.type
                            ) as MutableList<MyUserPollOptionsModel>
                            Log.e("abc", "UserPollOption : ${myUserPollOptionsModel.toString()}")
                        }

                    }


                    //Preference choice = 3 , Yes/No = 0 , Percent share = 4 , multiplechoice = 2
                    if (pollType == "multiplechoice" || pollType == "Multiple choice" || pollType == "Percent share" ||
                        pollType == "Percentage sharing on option"
                    )
                        showMultipleSelectionChart()
                    else if (pollType == "Preference choice")
                        showPreferenceChoiceChart()
                    else
                        showPieChart()

                    if (totalAnswers > 0)
                        setPollResultDetails(pollResponse)
                    else
                        binding.clResultList.visibility = View.GONE

                    setOwnRearchData()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showMultipleSelectionChart() {
        //MPChart
        barDataInformation = ArrayList()

        for (index in percentage.indices) {
            try {
                barDataInformation.add(
                    BarEntry(
                        (index + 1).toFloat(),
                        percentage[index].replace("%", "").replace("\"", "").toFloat()
                    )
                )
                barDataSet = BarDataSet(barDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        // Custom value formatter to format the value
        barDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "0%" else ("" + value.toInt() + "%")
            }
        }

        barDataSet.setColors(material_color_list, 250)
        barDataSet.valueTextColor = resources.getColor(R.color.black)
        barDataSet.valueTextSize = 7f

        barData = BarData(barDataSet)

        binding.barChart.setFitBars(true)
        binding.barChart.legend.isEnabled = false
        binding.barChart.data = barData
        binding.barChart.description.text = ""
        binding.barChart.animateY(700)

        binding.barChart.invalidate()

        if (options.size != 0)
            setBarLegends(options, percentage, material_color_list)
    }

    private fun showPreferenceChoiceChart() {

        barDataSetList = ArrayList()
        barData = BarData()

        var numm = 1f

        try {
            for (index in answerCountList.indices) {

                barDataInformation = ArrayList()

                for (i in 0 until answerCountList[index].size) {

                    barDataInformation.add(
                        BarEntry(
                            numm,
                            answerCountList[index][i].replace("%", "").replace("\"", "")
                                .toFloat()
                        )
                    )
                    numm++
                }

                barDataSet = BarDataSet(barDataInformation, options[index])
                barDataSet.color = material_color_list[index]
                barDataSetList.add(barDataSet)

                numm += 2 //Required for x-coordinator for bar graph

            }

            // Custom value formatter to format the value
            barDataSet.valueFormatter = object : ValueFormatter() {
                override fun getFormattedValue(value: Float): String {
                    return if (value == 0f) "0%" else ("" + value.toInt() + "%")
                }
            }

            for (i in 0 until barDataSetList.size)
                barData.addDataSet(barDataSetList[i])

            binding.barChart.data = barData
            binding.barChart.legend.isEnabled = false

            barDataSet.valueTextColor = resources.getColor(R.color.black)
            barDataSet.valueTextSize = 7f

            val groupSpace = 0.8f
            val barSpace = 0.03f
            val barWidth = 1f

            barData.barWidth = barWidth

            binding.barChart.description.isEnabled = false
            binding.barChart.xAxis.setCenterAxisLabels(true)
            binding.barChart.setFitBars(true)
            binding.barChart.groupBars(0f, groupSpace, barSpace)
            binding.barChart.animateY(700)

            binding.barChart.invalidate()

            if (options.size != 0)
                setBarLegends(options, percentage, material_color_list)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showPieChart() {

        isPieChart = true

        binding.pieChart.visibility = View.VISIBLE
        binding.barChart.visibility = View.GONE

        //MPChart
        pieDataInformation = ArrayList()
        for (index in percentage.indices) {
            try {
                pieDataInformation.add(
                    PieEntry(
                        percentage[index].replace("%", "").replace("\"", "").toFloat(),
                        options[index]
                    )
                )
                pieDataSet = PieDataSet(pieDataInformation, options[index])
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        pieDataSet.colors = material_color_list.asList()
        pieDataSet.valueTextSize = 8f

        val data = PieData(pieDataSet)

        binding.pieChart.data = data
        binding.pieChart.description.isEnabled = false
        binding.pieChart.isDrawHoleEnabled = false
        binding.pieChart.setDrawEntryLabels(false)
        binding.pieChart.legend.orientation = Legend.LegendOrientation.VERTICAL
        binding.pieChart.legend.isWordWrapEnabled = true
        binding.pieChart.legend.isEnabled = false

        // Center text inside slices
        pieDataSet.xValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE
        pieDataSet.yValuePosition = PieDataSet.ValuePosition.INSIDE_SLICE

        // Custom value formatter to format the value
        pieDataSet.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value == 0f) "" else ("" + value.toInt() + "%")
            }
        }

        binding.pieChart.invalidate()

        if (options.size != 0) {
            val params = binding.rvBarLegend.layoutParams as ConstraintLayout.LayoutParams
            params.topToBottom = binding.pieChart.id
            setBarLegends(options, percentage, material_color_list)
        }

    }

    private fun setBarLegends(
        options: ArrayList<String>,
        percentage: ArrayList<String>,
        materialColors: IntArray
    ) {
        optionsColorList = ArrayMap<String, Int>()

        for (i in 0 until options.size) {
            optionsColorList.put(optionsIDList[i], material_color_list[i])
        }

        barLegendsAdapter =
            BarLegendsAdapter(this@SubmitedPollDetailActivity, options, percentage, materialColors)
        binding.rvBarLegend.layoutManager = GridLayoutManager(this, 2)
        binding.rvBarLegend.adapter = barLegendsAdapter
    }

    private fun setPollResultDetails(pollResponse: PollResponse) {

        if (!pollType.equals(
                "Percent share",
                true
            ) && !pollType.equals("Percentage sharing on option", true)
        ) {
            var answerIdList = ArrayList<String>()
            val regex = Regex("^p\\d+")

            for (i in 0 until pollResponse.pollData.pollOptions.size) {
                for (j in 0 until pollResponse.pollData.pollOptions[i].answerIds.size) {

                    val unique_usid =
                        regex.find(pollResponse.pollData.pollOptions[i].answerIds[j])?.value

                    if (unique_usid == user_sort_id) {
                        answerIdList.add(0, pollResponse.pollData.pollOptions[i].answerIds[j])
                    } else {
                        answerIdList.add(pollResponse.pollData.pollOptions[i].answerIds[j])
                    }
                }
            }

            val distinctAnswerIdList = answerIdList.distinct()

            pollResultDetailsUpdateAdapter =
                PollResultUpdatedAdapter(
                    this@SubmitedPollDetailActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse,
                    distinctAnswerIdList,
                    optionsColorList,
                    myUserPollOptionsModel
                )
            binding.rvPollResult.adapter = pollResultDetailsUpdateAdapter
        } else {
            pollResultDetailsAdapter =
                PollResultAdapter(
                    this@SubmitedPollDetailActivity,
                    options,
                    percentage,
                    material_color_list,
                    pollResponse
                )

            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            binding.rvPollResult.layoutManager = layoutManager
            binding.rvPollResult.adapter = pollResultDetailsAdapter
        }
    }

    private fun addyourOwnResearch() {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId",
            mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty("pollId", pollId)
        requestData.addProperty("userAnswer", binding.edtDoOwnResearch.text.toString())
        ApiServiceProvider.getInstance(this@SubmitedPollDetailActivity).sendPostData(
            Constants.UrlPath.ADD_USER_OWN_RESEARCH, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                if (status) {
                    binding.edtDoOwnResearch.setText("")
                    getPollDetailsResearch()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getPollDetailsResearch() {
        val jsonObject = JsonObject()

        jsonObject.addProperty("pollId", pollId)
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())

        ApiServiceProvider.getInstance(this@SubmitedPollDetailActivity).sendPostData(
            Constants.UrlPath.GET_POLL_DETAILS, jsonObject, false
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                if (status) {
                    submittedPollDetailsModel = Gson().fromJson<Any>(
                        responseObj.toString(),
                        object : TypeToken<SubmittedPollDetailsModel>() {}.type
                    ) as SubmittedPollDetailsModel

                    setOwnRearchData()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setOnclickListener() {
        binding.apply {
            imgClose.setOnClickListener {
                finish()
            }
            imgSend.setOnClickListener {
                if (checkValidation()) {
                    addyourOwnResearch()
                }
            }
            tvUserQuestion.setOnClickListener {
                if (tvUserQuestion.tag == "1") {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_expand, 0
                    )
                    tvUserQuestionDescription.visibility = View.VISIBLE
                    tvUserQuestion.tag = "0"
                } else {
                    tvUserQuestion.setCompoundDrawablesWithIntrinsicBounds(
                        0, 0, R.drawable.ic_collapse, 0
                    )
                    tvUserQuestionDescription.visibility = View.GONE
                    tvUserQuestion.tag = "1"
                }
            }
        }
    }

    private fun checkValidation(): Boolean {
        if (binding.edtDoOwnResearch.text.toString().isEmpty()) {
            binding.edtDoOwnResearch.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_user_message),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }

    private fun setOwnRearchData() {
        myActivityOwnResearchAdapter = MyActivityOwnResearchAdapter(
            this@SubmitedPollDetailActivity,
            submittedPollDetailsModel!!.pollData.researchData
        )
        binding.rvDoOwnResearch.adapter = myActivityOwnResearchAdapter
    }
}