package com.poll.print.editprofile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.poll.print.R
import com.poll.print.Utils.Constants
import com.poll.print.databinding.BottomSheetAddProfilePicBinding

class AddProfilePicFragment : BottomSheetDialogFragment() {

    var mListener: Listener? = null
    private var _binding: BottomSheetAddProfilePicBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomSheetAddProfilePicBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpClickListener()
    }

    private fun setUpClickListener() {
        binding.apply {
            chooseCamera.setOnClickListener {
                mListener!!.returnData(Constants.RequestPermission.REQUEST_CAMERA)
                dismiss()
            }

            chooseGallery.setOnClickListener {
                mListener!!.returnData(Constants.RequestPermission.REQUEST_GALLERY)
                dismiss()
            }
            imgClose.setOnClickListener {
                dismiss()
            }
        }
    }

    /**
     * Method is used to set the listener...
     * @param listener
     */
    fun setListener(listener: Listener) {
        mListener = listener
    }


    /**
     * INTERFACE is used to get the listener when dialog is closed...
     */
    interface Listener {
        fun returnData(selectedType: Int?)
    }
}