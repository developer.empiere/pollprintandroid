package com.poll.print.editprofile

import android.Manifest.permission.CAMERA
import android.Manifest.permission.READ_MEDIA_IMAGES
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.gngc.utils.Permissons
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.ImagePickerActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.country.CountryAdapter
import com.poll.print.country.CountryModel
import com.poll.print.databinding.ActivityEditProfileBinding
import com.poll.print.prouser.ProUserActivity
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import com.poll.print.state.StateAdapter
import com.poll.print.state.StateModel
import com.yalantis.ucrop.UCrop
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.File
import java.util.Calendar

class EditProfileActivity : AppCompatActivity() {

    lateinit var binding: ActivityEditProfileBinding
    private lateinit var profileImage: File
    var mPicturePath: String = ""
    lateinit var mSessionManager: SessionManager
    var locationImageUrl: String = ""

    private var countryModel: MutableList<CountryModel>? = mutableListOf()
    private var stateModel: MutableList<StateModel>? = mutableListOf()

    var countryId: String = ""
    var country: String = ""
    var state: String = ""
    val imageStoreId: String = System.currentTimeMillis().toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mSessionManager = SessionManager(this)

        getCountryList()
        setOnCLickListener()
    }

    private fun setHomeData() {
        if (mSessionManager.getBooleanData(SessionManager.IS_USER_PREMIUM)) {
            if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) != Constants.PRO_USER_STATUS.ACCEPTED) {
                if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.SUBMITTED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                } else if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.NOTAPLLIED) {
                    binding.btnProUser.text = "Application Submitted"
                    binding.btnProUser.isEnabled = false
                } else if (mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS) == Constants.PRO_USER_STATUS.REJECTED) {
                    binding.btnProUser.text = "Become a Pro User"
                    binding.btnProUser.isEnabled = true
                }
            } else {
                binding.btnProUser.text = "Pro User"
                binding.btnProUser.isEnabled = false
            }
        } else {
            binding.btnProUser.text = "Become a Pro User"
            binding.btnProUser.isEnabled = true
        }

    }

    private fun getUserProfile() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", mSessionManager.getData(SessionManager.USER_ID).toString())
        ApiServiceProvider.getInstance(this@EditProfileActivity).sendPostData(
            Constants.UrlPath.GET_USER_PROFILE, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                val data = responseObj.getJSONObject("data")
                var country_id = ""
                if (status) {
                    val name = data.getString("username").toString()
                    val email = data.getString("email").toString()
                    val mobile = data.getString("mobile").toString()
                    val birthDate = data.getString("birthDate").toString()
                    val imageURL = data.getString("image").toString()
                    val premiumUserStatus = data.getString("premiumUserStatus")
                    val isPremiumUser = data.getBoolean("isPremiumUser")
                    country = data.getString("country").toString()
                    countryId = data.getString("countryId").toString()
                    state = data.getString("state").toString()

                    if (data.has("country_id")) {
                        country_id = data.getString("country_id").toString()
                        mSessionManager.setData(SessionManager.USER_COUNTRY_ID, country_id)
                    }

                    mSessionManager.setData(SessionManager.USER_COUNTRY, country)

                    if (imageURL.equals("")) {
                        Glide.with(this@EditProfileActivity)
                            .load(R.drawable.ic_no_profile)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    } else {
                        Glide.with(this@EditProfileActivity)
                            .load(imageURL)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    }

                    mSessionManager.setData(SessionManager.USER_NAME, name)
                    mSessionManager.setData(SessionManager.USER_PROFILE, imageURL)
                    mSessionManager.setData(SessionManager.USER_EMAIL, email)
                    mSessionManager.setData(SessionManager.USER_BIRTH_DATE, birthDate)
                    mSessionManager.setData(SessionManager.USER_MOBILE, mobile)
                    mSessionManager.setData(SessionManager.IS_USER_PREMIUM, isPremiumUser)

                    if (premiumUserStatus.equals("Submitted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.SUBMITTED
                        )

                    } else if (premiumUserStatus.equals("Rejected")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.REJECTED
                        )

                    } else if (premiumUserStatus.equals("Accepted")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.ACCEPTED
                        )

                    } else if (premiumUserStatus.equals("NotApplied")) {
                        mSessionManager.setData(
                            SessionManager.USER_PREMIUM_STATUS,
                            Constants.PRO_USER_STATUS.NOTAPLLIED
                        )

                    }

                    binding.edtName.setText(name)
                    binding.edtMobile.setText(mobile)
                    binding.edtEmailAddress.setText(email)
                    binding.edtBirthdate.setText(birthDate)

                    for (i in 0 until countryModel!!.size) {
                        if (country == countryModel!![i].countryName) {
                            binding.spCountry.setSelection(i)
                            countryId = countryModel!![i]._id
                            break
                        }
                    }

                    if (!countryId.equals(""))
                        getStateListData(countryId)

                    setHomeData()

                } else {
                    Toast.makeText(this@EditProfileActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setOnCLickListener() {
        binding.apply {
            imgClose.setOnClickListener {
                finish()
            }
            btnProUser.setOnClickListener {
                val i = Intent(this@EditProfileActivity, ProUserActivity::class.java)
                i.putExtra("userCountry", country)
                startActivity(i)
            }
            edtBirthdate.setOnClickListener {
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(
                    this@EditProfileActivity, { view, year, monthOfYear, dayOfMonth ->
                        edtBirthdate.setText(
                            (dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                        )
                    }, year, month, day
                )
                datePickerDialog.show()
            }
            imageProfileEdit.setOnClickListener {
                showImagePickerChooser()
            }
            btnSave.setOnClickListener {
                if (checkValidation()) {
                    editProfile()
                }
            }
            binding.spCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {

                    countryId = (binding.spCountry.selectedItem as CountryModel)._id
                    country = (binding.spCountry.selectedItem as CountryModel).countryName

                    if (!countryId.equals("")) {
                        getStateListData(countryId)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }

            binding.spState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {
                    state = stateModel!!.get(position).stateName
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }
    }

    private fun getCountryList() {
        val jsonObject = JsonObject()
        ApiServiceProvider.getInstance(this@EditProfileActivity).sendPostData(
            Constants.UrlPath.GET_COUNTRY,
            jsonObject,
            true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val countries = responseObj.getJSONArray("countries")
                        countryModel = Gson().fromJson<Any>(
                            countries.toString(),
                            object : TypeToken<MutableList<CountryModel>?>() {}.type
                        ) as MutableList<CountryModel>
                        val cm = CountryModel()
                        cm.countryName = "Select Country"
                        countryModel!!.add(0, cm)
                        setCountryAdapter(countryModel!!)

                        for (i in 0 until countryModel!!.size) {
                            if (country.equals(countryModel!!.get(i).countryName)) {
                                binding.spCountry.setSelection(i)
                                break
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setCountryAdapter(countryModel: MutableList<CountryModel>) {
        binding.spCountry.adapter =
            this@EditProfileActivity.let { CountryAdapter(it, countryModel) }
    }

    private fun getStateListData(storeCountryId: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("countryId", storeCountryId)
        ApiServiceProvider.getInstance(this@EditProfileActivity).sendPostData(
            Constants.UrlPath.GET_STATE,
            jsonObject,
            true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val states = responseObj.getJSONArray("states")
                        stateModel = Gson().fromJson<Any>(
                            states.toString(),
                            object : TypeToken<MutableList<StateModel>?>() {}.type
                        ) as MutableList<StateModel>

                        val sm = StateModel()
                        sm.stateName = "Select State"
                        stateModel!!.add(0, sm)
                        setStateAdapter(stateModel!!)

                        for (i in 0 until stateModel!!.size) {
                            if (state.equals(stateModel!!.get(i).stateName)) {
                                binding.spState.setSelection(i)
                                break
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setStateAdapter(stateModel: MutableList<StateModel>) {
        binding.spState.adapter =
            this@EditProfileActivity.let { StateAdapter(it, stateModel) }
    }

    private fun editProfile() {
        val requestData = JsonObject()
        requestData.addProperty(
            "userId", mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        requestData.addProperty("name", binding.edtName.text.toString())
        requestData.addProperty("imageURL", locationImageUrl)
        requestData.addProperty("mobile", binding.edtMobile.text.toString())
        requestData.addProperty("birthDate", binding.edtBirthdate.text.toString())
        requestData.addProperty("country", country)
        requestData.addProperty("countryId", countryId)
        requestData.addProperty("state", state)
        requestData.addProperty("email", binding.edtEmailAddress.text.toString())

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.UPDATE_USER_PROFILE,
            requestData,
            true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                        getUserProfile()
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun checkValidation(): Boolean {
        if (binding.edtName.text.toString().isEmpty()) {
            binding.edtName.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_name),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtEmailAddress.text.toString().isEmpty()) {
            binding.edtEmailAddress.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_email),
                getString(R.string.txt_ok)
            )
            return false
        } else if (!AppUtils.isValidEmail(binding.edtEmailAddress)) {
            binding.edtEmailAddress.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_valid_email),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }

    private fun showImagePickerChooser() {
        showIntentChooser()
    }

    private fun showIntentChooser() {
        val addProfilePicFragment = AddProfilePicFragment()
        addProfilePicFragment.setListener(listener = object : AddProfilePicFragment.Listener {
            override fun returnData(selectedType: Int?) {
                if (selectedType == Constants.RequestPermission.REQUEST_GALLERY) {
                    askForGalleryPermission()

                } else if (selectedType == Constants.RequestPermission.REQUEST_CAMERA) {
                    askForCameraPermission()
                }
            }
        })
        addProfilePicFragment.show(supportFragmentManager, "ADD_PROFILE_PIC")
    }

    /**
     * Ask for camera permission or open camera if permission already granted..
     */
    private fun askForCameraPermission() {
        if (!Permissons.Check_CAMERA(this)) {
            requestPermissions(
                arrayOf(
                    CAMERA, WRITE_EXTERNAL_STORAGE
                ), Permissons.CAMERA
            )
        } else openCamera()
    }

    /**
     * Ask for gallery permission or open gallery if permission already granted..
     */
    private fun askForGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU && !Permissons.Check_MEDIA_IMAGES(
                this
            )
        ) {
            requestPermissions(
                arrayOf(READ_MEDIA_IMAGES), Permissons.WRITE_EXTERNAL_STORAGE
            )
        } else openGallery()
    }

    private fun openCamera() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, Constants.RequestPermission.REQUEST_GALLERY)

    }

    /**
     * Used to open gallery using intent..
     */
    private fun openGallery() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, Constants.RequestPermission.REQUEST_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestPermission.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                val fileName = "front"
                val destination = Uri.fromFile(File(cacheDir, "cropped_$fileName"))
                UCrop.of(Uri.fromFile(File(mPicturePath)), destination).withAspectRatio(1F, 1F)
                    .start(this)
            }

        } else if (requestCode == Constants.RequestPermission.REQUEST_GALLERY) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                profileImage = File(uri!!.path.toString())
                binding.imgProfile.setImageURI(uri)
                uploadImages()
            }

        } else if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == RESULT_OK) {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(this.contentResolver, UCrop.getOutput(data!!))
                Glide.with(this@EditProfileActivity).load(bitmap).into(binding.imgProfile)
                profileImage = File(PathUtil.getPath(this, UCrop.getOutput(data)))

            } else if (resultCode == UCrop.RESULT_ERROR) {
            }
        }
    }

    private fun uploadImages() {
        val imageType: RequestBody =
            "OptionImage".toRequestBody("text/plain".toMediaTypeOrNull())

        val imageStoreId: RequestBody =
            imageStoreId.toString()
                .toRequestBody("text/plain".toMediaTypeOrNull())

        val uploadImage: RequestBody =
            profileImage.asRequestBody("multipart/form- *".toMediaTypeOrNull())
        val uploadImagePart: MultipartBody.Part =
            MultipartBody.Part.createFormData("userImage", profileImage.name, uploadImage)

        ApiServiceProvider.getInstance(this@EditProfileActivity).uploadImage(
            Constants.UrlPath.UPLOAD_IMAGE, imageType, imageStoreId, uploadImagePart, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val success = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                locationImageUrl = responseObj.getString("location")

                if (success) {
                    Toast.makeText(this@EditProfileActivity, "" + message, Toast.LENGTH_SHORT)
                        .show()

                    if (locationImageUrl.equals("")) {
                        Glide.with(this@EditProfileActivity)
                            .load(R.drawable.ic_no_profile)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    } else {
                        Glide.with(this@EditProfileActivity)
                            .load(locationImageUrl)
                            .placeholder(R.drawable.ic_no_profile)
                            .into(binding.imgProfile)
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Permissons.WRITE_EXTERNAL_STORAGE -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery()
                }
            } else askForGalleryPermission()

            Permissons.CAMERA -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                }
            } else askForCameraPermission()
        }
    }

    override fun onResume() {
        super.onResume()
        getUserProfile()
    }
}