package com.gngc.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

object Permissons {
    const val WRITE_EXTERNAL_STORAGE = 100
    const val CAMERA = 101

    fun Check_MEDIA_IMAGES(context: Context?): Boolean {
        val result =
            ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_MEDIA_IMAGES)
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun Check_CAMERA(context: Context?): Boolean {
        val result = ContextCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA)
        return result == PackageManager.PERMISSION_GRANTED
    }

}