package com.poll.print.createpoll

import android.Manifest.permission.CAMERA
import android.Manifest.permission.READ_MEDIA_IMAGES
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.gngc.utils.Permissons
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.poll.print.ImagePickerActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.AppUtils.setGone
import com.poll.print.Utils.AppUtils.setInVisible
import com.poll.print.Utils.AppUtils.setVisible
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.country.CountryAdapter
import com.poll.print.country.CountryModel
import com.poll.print.databinding.ActivityCreatePollBinding
import com.poll.print.editprofile.AddProfilePicFragment
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

class CreatePollActivity : AppCompatActivity(), AddPollAdapter.AddOptionImageClickListener, AddPollAdapter.EnlargeOptionImageListener {

    lateinit var binding: ActivityCreatePollBinding

    private lateinit var profileImage: File
    var mPicturePath: String = ""
    var categoryId: String = ""
    var isAllow: Boolean = false
    var countryId: String = ""
    var imagePathFirst: String = ""
    var imagePathSecond: String = ""
    var pollType: String = "Yes/No"

    var selectedPollOptionPos = 0

    lateinit var mSessionManager: SessionManager

    private var categoryModel: MutableList<CategoryModel>? = mutableListOf()

    val uniqueId: String = System.currentTimeMillis().toString()

    private var addPollModel = mutableListOf<AddPollModel>()
    private lateinit var addPollAdapter: AddPollAdapter

    var pollOptionsArray = JsonArray()
    val pollImagesArray = JsonArray()

    var isPollOptionImage = false

    private var countryModel: MutableList<CountryModel>? = mutableListOf()

    private var startDay = ""
    private var startMonth = ""
    private var startYear = ""
    private var userCountry = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCreatePollBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        binding.headerCreatePoll.tvTitle.text = "Create A Poll"

        setOnClickListener()
        setPollTypeOptionAdd()
        setAnimation()

        getPollCategory()
        //getCountryList()

        userCountry = mSessionManager.getData(SessionManager.USER_COUNTRY).toString()
        countryId = mSessionManager.getData(SessionManager.USER_COUNTRY_ID).toString()

        binding.edtUserCountry.setText(userCountry)

        binding.switchAllowVoters.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            isAllow = isChecked
        })
    }

    /*private fun getCountryList() {
        val jsonObject = JsonObject()
        ApiServiceProvider.getInstance(this@CreatePollActivity).sendPostData(
            Constants.UrlPath.GET_COUNTRY,
            jsonObject,
            true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val countries = responseObj.getJSONArray("countries")
                        countryModel = Gson().fromJson<Any>(
                            countries.toString(),
                            object : TypeToken<MutableList<CountryModel>?>() {}.type
                        ) as MutableList<CountryModel>

                        val cm = CountryModel()
                        cm.countryName = "Select Country"
                        countryModel!!.add(0, cm)

                        setCountryAdapter(countryModel!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }*/

    private fun uploadImages() {
        val imageType: RequestBody = "OptionImage".toRequestBody("text/plain".toMediaTypeOrNull())
        val imageStoreId: RequestBody = uniqueId.toRequestBody("text/plain".toMediaTypeOrNull())

        val uploadImage: RequestBody = profileImage.asRequestBody("multipart/form-data*".toMediaTypeOrNull())
        val uploadImagePart: MultipartBody.Part = MultipartBody.Part.createFormData("userImage", profileImage.name, uploadImage)

        ApiServiceProvider.getInstance(this@CreatePollActivity).uploadImage(
            Constants.UrlPath.UPLOAD_IMAGE, imageType, imageStoreId, uploadImagePart, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val success = responseObj.getBoolean("status")
                val location = responseObj.getString("location")
                if (success) {
                    if (!isPollOptionImage) {
                        if (imagePathFirst == "") {
                            binding.rlPollImage1.setVisible()
                            imagePathFirst = location
                            Glide.with(this@CreatePollActivity).load(location).into(binding.image)
                            pollImagesArray.add(imagePathFirst)
                        } else if (imagePathSecond == "") {
                            binding.rlPollImage2.setVisible()
                            imagePathSecond = location
                            Glide.with(this@CreatePollActivity).load(location).into(binding.image2)
                            pollImagesArray.add(imagePathSecond)
                        }
                    } else {
                        if (addPollModel[selectedPollOptionPos].firstImageUrl == "") {
                            addPollModel[selectedPollOptionPos].firstImageUrl = location
                        } else if (addPollModel[selectedPollOptionPos].secondImageUrl == "") {
                            addPollModel[selectedPollOptionPos].secondImageUrl = location
                        } else if (addPollModel[selectedPollOptionPos].thirdImageUrl == "") {
                            addPollModel[selectedPollOptionPos].thirdImageUrl = location
                        }

                        addPollAdapter = AddPollAdapter(
                            this@CreatePollActivity, addPollModel, pollType, this, this
                        )
                        binding.rvAddPoll.adapter = addPollAdapter
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setAnimation() {

    }

    private fun setPollTypeOptionAdd() {
        if (pollType == "Yes/No") {
            addPollModel.add(AddPollModel(pollName = "Yes", "", "", ""))
            addPollModel.add(AddPollModel(pollName = "No", "", "", ""))
            addPollAdapter = AddPollAdapter(this@CreatePollActivity, addPollModel, pollType, this, this)
            binding.rvAddPoll.adapter = addPollAdapter
        } else {
            addPollModel.clear()
            addPollModel.add(AddPollModel(pollName = "", "", "", ""))
            addPollAdapter = AddPollAdapter(this@CreatePollActivity, addPollModel, pollType, this, this)
            binding.rvAddPoll.adapter = addPollAdapter
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setOnClickListener() {
        binding.apply {
            headerCreatePoll.imgBack.setOnClickListener {
                finish()
            }
            imageAdd.setOnClickListener {
                if (imagePathFirst != "" && imagePathSecond != "") {
                    Toast.makeText(
                        this@CreatePollActivity, "You can't selected more then 2 images", Toast.LENGTH_SHORT
                    ).show()
                } else {
                    showImagePickerChooser()
                }
            }

            image.setOnClickListener {
                rlEnlargeImage.setVisible()
                Glide.with(applicationContext).load(image.drawable).into(ivEnlargeImage)
            }

            image2.setOnClickListener {
                rlEnlargeImage.setVisible()
                Glide.with(applicationContext).load(image2.drawable).into(ivEnlargeImage)
            }

            ivCloseImage.setOnClickListener {
                rlEnlargeImage.setGone()
            }

            imgClose.setOnClickListener {
                imagePathFirst = ""
                pollImagesArray.remove(0)
                rlPollImage1.setGone()
            }

            imgClose2.setOnClickListener {
                imagePathSecond = ""
                if (imagePathFirst != "") pollImagesArray.remove(1)
                else pollImagesArray.remove(0)
                rlPollImage2.setGone()
            }

            btnSubmit.setOnClickListener {
                pollOptionsArray = JsonArray()
                createPoll()
            }

            edtStartDate.setOnClickListener {
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog = DatePickerDialog(
                    this@CreatePollActivity, { view, year, monthOfYear, dayOfMonth ->
                        edtStartDate.setText(
                            (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                        )
                        edtEndDate.setText("")
                        startDay = dayOfMonth.toString()
                        startMonth = monthOfYear.toString()
                        startYear = year.toString()
                    }, year, month, day
                )
                datePickerDialog.datePicker.minDate = c.getTimeInMillis()
                datePickerDialog.show()
            }

            edtEndDate.setOnClickListener {
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)

                if (startDay != "" && startMonth != "" && startYear != "") {
                    val datePickerDialog = DatePickerDialog(
                        this@CreatePollActivity, { view, year, monthOfYear, dayOfMonth ->
                            edtEndDate.setText(
                                (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                            )
                        }, startYear.toInt(), startMonth.toInt(), startDay.toInt()
                    )
                    c.set(startYear.toInt(), startMonth.toInt(), startDay.toInt())
                    datePickerDialog.datePicker.minDate = c.getTimeInMillis()
                    datePickerDialog.show()
                } else {
                    val datePickerDialog = DatePickerDialog(
                        this@CreatePollActivity, { view, year, monthOfYear, dayOfMonth ->
                            edtEndDate.setText(
                                (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                            )
                        }, year, month, day
                    )
                    datePickerDialog.datePicker.minDate = c.getTimeInMillis()
                    datePickerDialog.show()
                }

            }
            binding.spPollCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {
                    if (position > 0) {
                        categoryId = categoryModel!![position]._id.toString()
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }

            /*binding.spCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>, view: View, position: Int, id: Long
                ) {
                    if (position > 0) {
                        countryId = countryModel!!.get(position)._id
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }*/

            tvAddMoreOption.setOnClickListener {
                if (pollType == "Preference choice") {
                    if (addPollModel.size < 5) {
                        addPollModel.add(AddPollModel(pollName = "", "", "", ""))
                        addPollAdapter.notifyItemInserted(addPollModel.size - 1)
                    } else {
                        Toast.makeText(
                            this@CreatePollActivity, "Maximum limit reached", Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    if (addPollModel.size < 10) {
                        addPollModel.add(AddPollModel(pollName = "", "", "", ""))
                        addPollAdapter.notifyItemInserted(addPollModel.size - 1)
                    } else {
                        Toast.makeText(
                            this@CreatePollActivity, "Maximum limit reached", Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            radioGroupPollType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radioButtonID: Int = radioGroupPollType.checkedRadioButtonId
                val radioButton: View = radioGroupPollType.findViewById(radioButtonID)
                val idx: Int = radioGroupPollType.indexOfChild(radioButton)
                when (idx) {
                    0 -> {
                        pollType = "Yes/No"
                        binding.edtMoney.setGone()
                        binding.tvAddMoreOption.setInVisible()
                        addPollModel.clear()
                        addPollModel.add(AddPollModel(pollName = "Yes", "", "", ""))
                        addPollAdapter.notifyItemInserted(addPollModel.size - 1)
                        addPollModel.add(AddPollModel(pollName = "No", "", "", ""))
                        addPollAdapter.notifyItemInserted(addPollModel.size - 1)
                        addPollAdapter.notifyDataSetChanged()
                    }

                    1 -> {
                        pollType = "Single selection from multiple choice"
                        binding.edtMoney.setGone()
                        binding.tvAddMoreOption.setVisible()
                    }

                    2 -> {
                        pollType = "Multiple choice"
                        binding.edtMoney.setGone()
                        binding.tvAddMoreOption.setVisible()
                    }

                    3 -> {
                        pollType = "Preference choice"
                        binding.edtMoney.setGone()
                        binding.tvAddMoreOption.setVisible()
                    }

                    4 -> {
                        pollType = "Percentage sharing on option"
                        binding.edtMoney.setVisible()
                        binding.tvAddMoreOption.setVisible()
                    }
                }
                setPollTypeOptionAdd()
            })
        }
    }

    private fun getPollCategory() {
        val requestData = JsonObject()
        ApiServiceProvider.getInstance(this@CreatePollActivity).sendPostData(
            Constants.UrlPath.GET_POLL_CATEGORY_LIST, requestData, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")

                if (status) {
                    val categories = responseObj.getJSONArray("categories")

                    categoryModel = Gson().fromJson<Any>(
                        categories.toString(), object : TypeToken<MutableList<CategoryModel>?>() {}.type
                    ) as MutableList<CategoryModel>

                    val cm = CategoryModel()
                    cm.categoryName = "Select Category"
                    categoryModel!!.add(0, cm)
                    setCategoryAdapter(categoryModel!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setCategoryAdapter(categoryModel: MutableList<CategoryModel>) {
        binding.spPollCategory.adapter = this@CreatePollActivity.let { CategoryAdapter(it, categoryModel) }
    }

    /*private fun setCountryAdapter(countryModel: MutableList<CountryModel>) {
        binding.spCountry.adapter =
            this@CreatePollActivity.let { CountryAdapter(it, countryModel) }
    }*/

    private fun createPoll() {

        addPollModel.forEachIndexed { index, i ->
            var pollOptionsImagesArray = JsonArray()

            if (addPollModel[index].firstImageUrl != "") pollOptionsImagesArray.add(addPollModel[index].firstImageUrl)

            if (addPollModel[index].secondImageUrl != "") pollOptionsImagesArray.add(addPollModel[index].secondImageUrl)

            if (addPollModel[index].thirdImageUrl != "") pollOptionsImagesArray.add(addPollModel[index].thirdImageUrl)

            val layout = binding.rvAddPoll.layoutManager!!.findViewByPosition(index) as ConstraintLayout
            val editText = layout.findViewById<EditText>(R.id.edt_poll_title)
            val jsonObject = JsonObject()
            jsonObject.addProperty("option", editText.text.toString())
            jsonObject.add("optionUrl", pollOptionsImagesArray)
            pollOptionsArray.add(jsonObject)
            Log.d("TAG", "setOnClickListener: $pollOptionsArray")
        }
        val countryIdArray = JsonArray()
        countryIdArray.add(countryId)
        val jsonObject = JsonObject()
        jsonObject.addProperty(
            "userId", mSessionManager.getData(SessionManager.USER_ID).toString()
        )
        jsonObject.addProperty("categoryId", categoryId)
        jsonObject.addProperty("imageStoreId", uniqueId)
//        jsonObject.addProperty("countryId", countryId)
        jsonObject.add("countryId", countryIdArray)
        jsonObject.addProperty("allowVotersToChangeAnswer", isAllow)
        jsonObject.addProperty("pollTitle", binding.edtPollTitle.text.toString())
        jsonObject.addProperty("pollSubTitle", binding.edtPollDiscriptionTag.text.toString())
        jsonObject.add("pollImages", pollImagesArray)
        jsonObject.addProperty("startDate", binding.edtStartDate.text.toString())
        jsonObject.addProperty("endDate", binding.edtEndDate.text.toString())
        jsonObject.addProperty("pollType", pollType)
        jsonObject.add("pollOptions", pollOptionsArray)
        jsonObject.addProperty("money", binding.edtMoney.text.toString())
        if (pollType == "Percentage sharing on option") {
            jsonObject.addProperty("pollValue", binding.edtMoney.text.toString())
        }

        println("josn object : ===== $jsonObject")

        ApiServiceProvider.getInstance(this@CreatePollActivity).sendPostData(
            Constants.UrlPath.CREATE_POLL, jsonObject, true
        ) { response ->
            try {
                val responseObj = JSONObject(response.body().toString())
                val status = responseObj.getBoolean("status")
                val message = responseObj.getString("message")
                if (status) {
                    finish()
                    Toast.makeText(this@CreatePollActivity, "" + message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showImagePickerChooser() {
        isPollOptionImage = false
        showIntentChooser()
    }

    private fun showIntentChooser() {
        val addProfilePicFragment = AddProfilePicFragment()
        addProfilePicFragment.setListener(listener = object : AddProfilePicFragment.Listener {
            override fun returnData(selectedType: Int?) {
                if (selectedType == Constants.RequestPermission.REQUEST_GALLERY) {
                    askForGalleryPermission()
                } else if (selectedType == Constants.RequestPermission.REQUEST_CAMERA) {
                    askForCameraPermission()
                }
            }
        })
        addProfilePicFragment.show(supportFragmentManager, "ADD_PROFILE_PIC")
    }

    private fun askForCameraPermission() {
        if (!Permissons.Check_CAMERA(this)) {
            requestPermissions(
                arrayOf(CAMERA, WRITE_EXTERNAL_STORAGE), Permissons.CAMERA
            )
        } else openCamera()
    }

    private fun openCamera() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, Constants.RequestPermission.REQUEST_GALLERY)
    }

    private fun createFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir).apply {
            mPicturePath = absolutePath
        }
    }

    private fun askForGalleryPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU && !Permissons.Check_MEDIA_IMAGES(
                this
            )
        ) {
            requestPermissions(
                arrayOf(READ_MEDIA_IMAGES), Permissons.WRITE_EXTERNAL_STORAGE
            )
        } else openGallery()
    }

    private fun openGallery() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, Constants.RequestPermission.REQUEST_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.RequestPermission.REQUEST_GALLERY) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                profileImage = File(uri!!.path.toString())
                uploadImages()
            }
        } else if (requestCode == Constants.RequestPermission.REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                profileImage = File(uri!!.path.toString())
                println("Camera path:-$profileImage")
                uploadImages()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Permissons.WRITE_EXTERNAL_STORAGE -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery()
                }
            } else askForGalleryPermission()

            Permissons.CAMERA -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                }
            } else askForCameraPermission()
        }
    }

    override fun onAddOptionImageClickListener(position: Int) {

        if (addPollModel[position].firstImageUrl != "" && addPollModel[position].secondImageUrl != "" && addPollModel[position].thirdImageUrl != "") {
            Toast.makeText(
                this@CreatePollActivity, "You can't selected more then 3 images", Toast.LENGTH_SHORT
            ).show()
        } else {
            selectedPollOptionPos = position
            isPollOptionImage = true
            showIntentChooser()
        }

    }

    override fun onEnlargeOptionImageListener(imageUrl: String) {
        binding.rlEnlargeImage.setVisible()
        Glide.with(this).load(imageUrl).into(binding.ivEnlargeImage)
    }
}