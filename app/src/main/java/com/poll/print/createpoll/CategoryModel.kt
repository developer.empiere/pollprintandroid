package com.poll.print.createpoll

data class CategoryModel(
    var _id: String? = null,
    var categoryName: String? = null,
)