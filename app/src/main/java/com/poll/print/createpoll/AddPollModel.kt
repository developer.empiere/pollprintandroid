package com.poll.print.createpoll

data class AddPollModel(
    var pollName: String = "",
    var firstImageUrl: String = "",
    var secondImageUrl: String = "",
    var thirdImageUrl: String = "",
)