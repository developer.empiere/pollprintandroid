package com.poll.print.createpoll

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.poll.print.R
import com.poll.print.Utils.AppUtils.setGone
import com.poll.print.Utils.AppUtils.setVisible

class AddPollAdapter(
    private val context: Context?,
    var addPollModel: MutableList<AddPollModel>?,
    var pollType: String,
    private val addOptionImageClickListener: AddOptionImageClickListener,
    private val enlargeOptionImageListener: EnlargeOptionImageListener
) : RecyclerView.Adapter<AddPollAdapter.ViewHolder>() {

    var deleteItemCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_add_poll, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val model = addPollModel!![position]

        if (model.pollName != "")
            holder.edt_poll_title.setText(model.pollName)

        if (pollType == "Yes/No") {
            holder.edt_poll_title.isEnabled = false
        }

        if (model.firstImageUrl != "") {
            holder.cl_first_image.setVisible()
            Glide.with(context!!).load(model.firstImageUrl).into(holder.img_first)
        }

        if (model.secondImageUrl != "") {
            holder.cl_second_image.setVisible()
            Glide.with(context!!).load(model.secondImageUrl).into(holder.img_second)
        }

        if (model.thirdImageUrl != "") {
            holder.cl_third_image.setVisible()
            Glide.with(context!!).load(model.thirdImageUrl).into(holder.img_third)
        }

        holder.img_select.setOnClickListener {
            addOptionImageClickListener.onAddOptionImageClickListener(position)
        }

        holder.img_first_close.setOnClickListener {
            model.firstImageUrl = ""
            holder.cl_first_image.setGone()
            notifyDataSetChanged()
        }

        holder.img_second_close.setOnClickListener {
            model.secondImageUrl = ""
            holder.cl_second_image.setGone()
            notifyDataSetChanged()
        }

        holder.img_third_close.setOnClickListener {
            model.thirdImageUrl = ""
            holder.cl_third_image.setGone()
            notifyDataSetChanged()
        }

        holder.img_first.setOnClickListener {
            enlargeOptionImageListener.onEnlargeOptionImageListener(model.firstImageUrl)
        }

        holder.img_second.setOnClickListener {
            enlargeOptionImageListener.onEnlargeOptionImageListener(model.secondImageUrl)
        }

        holder.img_third.setOnClickListener {
            enlargeOptionImageListener.onEnlargeOptionImageListener(model.thirdImageUrl)
        }

        holder.tv_delete.setOnClickListener {
            if (pollType == "Yes/No") {
                Toast.makeText(
                    context,
                    "You need to add at least two options for Yes/No poll",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                try {
                    if (addPollModel!!.size > 0)
                        if (position >= deleteItemCount)
                            addPollModel!!.removeAt(position - deleteItemCount)
                        else
                            addPollModel!!.removeAt(position)
                    else
                        addPollModel!!.removeAt(position)

                    notifyItemRemoved(position)

                    if (addPollModel!!.size > 0)
                        deleteItemCount++
                    else
                        deleteItemCount = 0

                    Log.e("aa", "deleteCount:: ${addPollModel!!.size}")
                } catch (e: Exception) {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return if (pollType == "Yes/No") {
            2
        } else {
            addPollModel!!.size
        }
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val img_select: ImageView = itemView.findViewById(R.id.img_select)
        val edt_poll_title: EditText = itemView.findViewById(R.id.edt_poll_title)
        val tv_delete: TextView = itemView.findViewById(R.id.tv_delete)
        val cl_first_image: ConstraintLayout = itemView.findViewById(R.id.cl_first_image)
        val cl_second_image: ConstraintLayout = itemView.findViewById(R.id.cl_second_image)
        val cl_third_image: ConstraintLayout = itemView.findViewById(R.id.cl_third_image)
        val img_first: RoundedImageView = itemView.findViewById(R.id.img_first)
        val img_second: RoundedImageView = itemView.findViewById(R.id.img_second)
        val img_third: RoundedImageView = itemView.findViewById(R.id.img_third)
        val img_first_close: ImageView = itemView.findViewById(R.id.img_first_close)
        val img_second_close: ImageView = itemView.findViewById(R.id.img_second_close)
        val img_third_close: ImageView = itemView.findViewById(R.id.img_third_close)
    }

    interface AddOptionImageClickListener {
        fun onAddOptionImageClickListener(position: Int)
    }

    interface EnlargeOptionImageListener {
        fun onEnlargeOptionImageListener(imageUrl: String)
    }
}