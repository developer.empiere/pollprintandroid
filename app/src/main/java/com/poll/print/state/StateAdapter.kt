package com.poll.print.state

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.poll.print.R

class StateAdapter(val context: Context, val stateModel: List<StateModel>) :
    BaseAdapter() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val view = inflater.inflate(R.layout.row_state_city_filter, p2, false)

        val txt_type = view.findViewById(R.id.txt_type) as TextView
        txt_type.text = stateModel[position].stateName

        return view
    }

    override fun getCount(): Int {
        return stateModel.size
    }

    override fun getItem(p0: Int): Any {
        return stateModel[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }

    override fun getDropDownView(
        position: Int, convertView: View?,
        parent: ViewGroup?
    ): View {
        val view = super.getDropDownView(position, convertView, parent)
        val tv = view as TextView
        if (position == 0) {
            tv.setTextColor(Color.GRAY)
        } else {
            tv.setTextColor(Color.BLACK)
            tv.setPadding(15, 20, 15, 20)
        }
        return view
    }

}
