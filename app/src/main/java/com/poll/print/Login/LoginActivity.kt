package com.poll.print.Login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import com.poll.print.DashBoard.DashBoardActivity
import com.poll.print.ForgotPassword.ForgotPasswordActivity
import com.poll.print.R
import com.poll.print.Registration.RegistrationActivity
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityLoginBinding
import com.poll.print.retrofit.ApiServiceProvider
import com.poll.print.retrofit.RetrofitListener
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    lateinit var mSessionManager: SessionManager

    private var fcmToken = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)

        setOnClickListener()
        registerFCMToken()
    }

    private fun setOnClickListener() {
        binding.apply {
            tvForgotPassword.setOnClickListener {
                val i = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                startActivity(i)
            }
            btnLogin.setOnClickListener {
                if (checkValidation()) {
                    doLogin()
                }
            }
            tvRegisterNow.setOnClickListener {
                val i = Intent(this@LoginActivity, RegistrationActivity::class.java)
                startActivity(i)
            }
        }
    }

    private fun registerFCMToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener<String?> { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            fcmToken = task.result

            Log.e("aaa", "FCM Token :: $fcmToken")
        })
    }

    private fun doLogin() {
        val requestData = JsonObject()
        requestData.addProperty("mobile", binding.edtMobile.text.toString())
        requestData.addProperty("password", binding.edtPassword.text.toString())
        requestData.addProperty("device_token", fcmToken)

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        val token = responseObj.getString("token")
                        val userData = responseObj.getJSONObject("userData")
                        val userId = userData.getString("userId")
                        val userName = userData.getString("userName")
                        val userEmail = userData.getString("userEmail")
                        val userProfile = userData.getString("userProfileImage")
                        val sort_id = userData.getString("sort_id")
                        val premiumUserStatus = userData.getString("premiumUserStatus")
                        val isPremiumUser = userData.getBoolean("isPremiumUser")
                        val deviceToken = userData.getString("device_token")

                        if (status) {
                            mSessionManager.setData(SessionManager.USER_ID, userId)
                            mSessionManager.setData(SessionManager.USER_NAME, userName)
                            mSessionManager.setData(
                                SessionManager.PHONE_NUMBER,
                                binding.edtMobile.text.toString()
                            )
                            mSessionManager.setData(
                                SessionManager.USER_MOBILE,
                                binding.edtMobile.text.toString()
                            )
                            mSessionManager.setData(SessionManager.USER_EMAIL, userEmail)
                            mSessionManager.setData(SessionManager.IS_USER_PREMIUM, isPremiumUser)
                            mSessionManager.setData(SessionManager.SORT_ID, sort_id)
                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager.setData(SessionManager.USER_PROFILE_IMAGE, userProfile)
                            mSessionManager.setData(SessionManager.FCM_TOKEN, deviceToken)
                            mSessionManager.setUserLogin(true)

                            if (premiumUserStatus.equals("Submitted")) {
                                mSessionManager.setData(
                                    SessionManager.USER_PREMIUM_STATUS,
                                    Constants.PRO_USER_STATUS.SUBMITTED
                                )

                            } else if (premiumUserStatus.equals("Rejected")) {
                                mSessionManager.setData(
                                    SessionManager.USER_PREMIUM_STATUS,
                                    Constants.PRO_USER_STATUS.REJECTED
                                )

                            } else if (premiumUserStatus.equals("Accepted")) {
                                mSessionManager.setData(
                                    SessionManager.USER_PREMIUM_STATUS,
                                    Constants.PRO_USER_STATUS.ACCEPTED
                                )

                            } else if (premiumUserStatus.equals("NotApplied")) {
                                mSessionManager.setData(
                                    SessionManager.USER_PREMIUM_STATUS,
                                    Constants.PRO_USER_STATUS.NOTAPLLIED
                                )

                            }
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                            val i = Intent(this@LoginActivity, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT)
                                .show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun checkValidation(): Boolean {
        if (binding.edtMobile.text.toString().isEmpty()) {
            binding.edtMobile.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_enter_mobile),
                getString(R.string.txt_ok)
            )
            return false
        } else if (binding.edtMobile.length() < 10) {
            binding.edtMobile.requestFocus()
            AppUtils.showAlertDialog(
                this,
                getString(R.string.txt_alert),
                getString(R.string.alert_invalid_mobile),
                getString(R.string.txt_ok)
            )
            return false
        }
        return true
    }
}