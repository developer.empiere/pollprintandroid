package com.poll.print.profilelist

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.poll.print.ChangePassword.ChangePasswordActivity
import com.poll.print.R
import com.poll.print.Utils.AppUtils
import com.poll.print.Utils.Constants
import com.poll.print.Utils.SessionManager
import com.poll.print.databinding.ActivityProfileListBinding
import com.poll.print.logout.DialogLogout
import com.poll.print.profiledetails.ProfileDetailsActivity
import com.poll.print.prouser.ProUserActivity

class ProfileListActivity : AppCompatActivity() {

    lateinit var binding: ActivityProfileListBinding

    lateinit var mSessionManager: SessionManager
    var userProfile: String = ""
    var userName: String = ""
    var proUserStatus: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProfileListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppUtils.setStatusBar(window, this)

        mSessionManager = SessionManager(this)
        proUserStatus = mSessionManager.getIntData(SessionManager.USER_PREMIUM_STATUS).toString()

        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()
        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()

        if (userName != null && userName != "null")
            binding.tvName.text = userName
        else
            binding.tvName.text = ""

        Glide.with(this@ProfileListActivity).load(userProfile).placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        setOnClickListener()
        binding.headerSingle.tvTitle.text = "Profile"

    }

    private fun setOnClickListener() {
        binding.apply {
            tvChangePassword.setOnClickListener {
                val i = Intent(this@ProfileListActivity, ChangePasswordActivity::class.java)
                startActivity(i)
            }

            if (proUserStatus.equals(Constants.PRO_USER_STATUS.NOTAPLLIED) || proUserStatus.equals(
                    Constants.PRO_USER_STATUS.REJECTED
                )
            ) {
                tvPremiumUser.setOnClickListener {
                    val i = Intent(this@ProfileListActivity, ProUserActivity::class.java)
                    startActivity(i)
                }
            } else {
                tvPremiumUser.visibility = View.GONE
            }

            tvTermsCondition.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW)
                browserIntent.data = Uri.parse(Constants.UrlPath.APP_TERMS_AND_CONDITION_LINK)
                startActivity(browserIntent)
            }
            tvAboutUs.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW)
                browserIntent.data = Uri.parse(Constants.UrlPath.APP_ABOUT_US_LINK)
                startActivity(browserIntent)
            }
            tvPrivacyPolicy.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW)
                browserIntent.data = Uri.parse(Constants.UrlPath.APP_PRIVACY_POLICY_LINK)
                startActivity(browserIntent)
            }
            clProfile.setOnClickListener {
                val i = Intent(this@ProfileListActivity, ProfileDetailsActivity::class.java)
                startActivity(i)
            }
            tvMyActivity.setOnClickListener {
                val i = Intent(this@ProfileListActivity, ProfileDetailsActivity::class.java)
                startActivity(i)
            }
            headerSingle.imgBack.setOnClickListener {
                finish()
            }
            tvLogout.setOnClickListener {
                val logoutDialog = DialogLogout()
                logoutDialog.show(supportFragmentManager, DialogLogout::class.java.name)
            }
            switchPushNotification.setOnClickListener {
                // This is only necessary for API level >= 33 (TIRAMISU)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    if (ContextCompat.checkSelfPermission(
                            this@ProfileListActivity,
                            Manifest.permission.POST_NOTIFICATIONS
                        ) !=
                        PackageManager.PERMISSION_GRANTED
                    ) {
                        requestNotificationPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                    } else {
                        val settingsIntent: Intent =
                            Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                        startActivity(settingsIntent)
                    }
                }
            }
        }
    }

    private val requestNotificationPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (!isGranted) {
            Snackbar.make(
                binding.clUserProMain,
                String.format(
                    String.format(
                        "By disabling notification permission, you'll not get any future updates notifications",
                        getString(R.string.app_name)
                    )
                ),
                Snackbar.LENGTH_SHORT
            ).setAction("Settings") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    val settingsIntent: Intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
                    startActivity(settingsIntent)
                }
            }.setActionTextColor(resources.getColor(R.color.colorPrimaryLight)).show()
        }
    }

    override fun onResume() {
        super.onResume()
        userProfile = mSessionManager.getData(SessionManager.USER_PROFILE).toString()
        userName = mSessionManager.getData(SessionManager.USER_NAME).toString()

        if (userName != null && userName != "null")
            binding.tvName.text = userName
        else
            binding.tvName.text = ""

        Glide.with(this@ProfileListActivity).load(userProfile).placeholder(R.drawable.ic_no_profile)
            .into(binding.imgProfile)

        if (NotificationManagerCompat.from(this).areNotificationsEnabled())
            binding.switchPushNotification.isChecked = true
        else
            binding.switchPushNotification.isChecked = false
    }
}